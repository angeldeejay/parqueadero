/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.util;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import parqueadero.dao.UserDAO;
import parqueadero.model.Entity;
import parqueadero.model.Permission;
import parqueadero.model.Role;

/**
 * Session class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class Session {

    private Entity entity;
    private Timestamp loggedIn;
    private UserDAO dao = new UserDAO();
    private Collection<Permission> permissions;
    private Collection<Role> roles;

    public Session(Entity entity, Timestamp loggedIn) {
        this.entity = entity;
        this.loggedIn = loggedIn;
        this.permissions = new ArrayList();
        this.roles = new ArrayList();
        loadACL();
    }

    public Entity getEntity() {
        return entity;
    }

    public Timestamp getLoggedIn() {
        return loggedIn;
    }

    public boolean hasAllPermissions(String[] permissions) {
        if (isValid()) {
            return Arrays.stream(permissions).allMatch(p -> hasPermission(p));
        }
        return false;
    }

    public boolean hasAnyPermission(String[] permissions) {
        if (isValid()) {
            return Arrays.stream(permissions).anyMatch(p -> hasPermission(p));
        }
        return false;
    }

    public boolean hasNotAnyPermission(String[] permissions) {
        if (isValid()) {
            return Arrays.stream(permissions).noneMatch(p -> hasPermission(p));
        }
        return false;
    }

    public boolean hasPermission(String permission) {
        return permissions.stream().anyMatch(p -> p.getCode().equalsIgnoreCase(permission));
    }

    public boolean hasAllRoles(String[] roles) {
        if (isValid()) {
            return Arrays.stream(roles).allMatch(r -> hasRole(r));
        }
        return false;
    }

    public boolean hasAnyRole(String[] roles) {
        if (isValid()) {
            return Arrays.stream(roles).anyMatch(r -> hasRole(r));
        }
        return false;
    }

    public boolean hasNotAnyRole(String[] roles) {
        if (isValid()) {
            return Arrays.stream(roles).noneMatch(r -> hasRole(r));
        }
        return false;
    }

    public boolean hasRole(String role) {
        return roles.stream().anyMatch(r -> r.getName().equalsIgnoreCase(role));
    }

    public boolean isValid() {
        return loggedIn != null && entity != null && entity.getId() != null;
    }

    @Override
    public String toString() {
        return "parqueadero.util.Session{" + "entity=" + entity.toString() + ", loggedIn=" + loggedIn + ", valid=" + this.isValid() + ", permissions=" + this.permissions.toString() + '}';
    }

    public void loadACL() {
        if (this.isValid()) {
            this.roles.addAll(this.entity.getUser().getRoles());
            for (Role p : this.roles) {
                this.permissions.addAll(p.getPermissions());
            }
        }
    }
}
