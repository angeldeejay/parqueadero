/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.util;

import java.security.MessageDigest;
import static java.security.MessageDigest.getInstance;
import static parqueadero.util.Logger.getLogger;

/**
 * Security class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class Security {

    public static String hashPassword(String passwordToHash) {
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = getInstance("MD5");
            //Add password bytes to digest
            md.update(passwordToHash.getBytes());
            //Get the hash's bytes
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            return sb.toString();
        } catch (Exception ex) {
            getLogger(Security.class.getName()).error(ex.getMessage(), ex);
        }
        return null;
    }
}
