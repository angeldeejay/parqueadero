/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.util;

/**
 * Logger class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class Logger {

    public static org.apache.log4j.Logger getLogger(Object o) {
        return org.apache.log4j.Logger.getLogger(o.getClass());
    }

    public static org.apache.log4j.Logger getLogger(Class c) {
        return org.apache.log4j.Logger.getLogger(c);
    }
}

