/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.util;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;
import static parqueadero.util.Logger.getLogger;

/**
 * Config class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class Config {

    public static HashMap<String, Properties> getSettings() {
        HashMap<String, Properties> settings = new HashMap<>();
        try ( InputStream input = Config.class.getResource("/config.properties").openStream()) {
            // load a properties file
            Properties props = new Properties();
            props.load(input);
            // get the property value and print it out
            props.forEach((k, v) -> {
                String[] keys = ((String) k).split("\\.", 2);
                Properties acc = settings.get(keys[0]);
                if (acc == null) {
                    acc = new Properties();
                }
                acc.setProperty(keys[1], ((String) v));
                settings.put(keys[0], acc);
            });
            return settings;
        } catch (Exception ex) {
            getLogger(Config.class).error(ex.getMessage(), ex);
        }
        return null;
    }

    public static Properties getSettings(String type) {
        return getSettings().get(type);
    }
}











