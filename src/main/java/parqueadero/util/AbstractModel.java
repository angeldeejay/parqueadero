/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.util;

import java.io.Serializable;
import static java.lang.String.join;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import oracle.sql.Datum;
import parqueadero.model.User;
import static parqueadero.util.DataSource.getSecuenceNextValue;
import static parqueadero.util.Inflector.pascalize;
import static parqueadero.util.Logger.getLogger;

/**
 * AbstractModel class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public abstract class AbstractModel implements Serializable {

    /**
     * Table identifier (Primary key)
     */
    protected BigDecimal id;

    /**
     * Default constructor
     */
    public AbstractModel() {
    }

    /**
     * Bean factory constructor for models.This method build a model from a
     * key/value map retrieved from database.
     *
     * @param data
     */
    public AbstractModel(Map<String, Object> data) {
        this.setId((BigDecimal) data.get("id"));
        for (Field attribute : this.getClass().getDeclaredFields()) {
            int mod = attribute.getModifiers();
            if (Modifier.isAbstract(mod) || Modifier.isFinal(mod) | Modifier.isStatic(mod)) {
                continue;
            }
            boolean accesible = attribute.isAccessible();
            try {
                attribute.setAccessible(true);
                String k = attribute.getName(), setterName = ("set" + pascalize(k));
                Object v = data.get(k);
                if (v != null) {
                    Method setterMethod;
                    switch (k) {
                        case "id":
                            // superclass proxy
                            setterMethod = this.getClass().getSuperclass().getDeclaredMethod(setterName, new Class[]{BigDecimal.class});
                            setterMethod.invoke(this, v);
                            break;
                        case "empty":
                        case "enabled":
                        case "handicapped":
                        case "inmutable":
                            // Change setter signature to boolean
                            setterMethod = this.getClass().getDeclaredMethod(setterName, new Class[]{boolean.class});
                            setterMethod.invoke(this, v.equals("1"));
                            break;
                        default:
                            Class type = attribute.getType();
                            setterMethod = this.getClass().getDeclaredMethod(setterName, new Class[]{type});
                            switch (type.getName()) {
                                case "java.sql.Timestamp":
                                    setterMethod.invoke(this, v == null ? v : ((Datum) v).timestampValue());
                                    break;
                                default:
                                    setterMethod.invoke(this, v);
                            }
                    }
                }
            } catch (SecurityException
                    | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            } catch (Exception ex) {
                getLogger(AbstractModel.class).error(ex.getMessage(), ex);
            }
            attribute.setAccessible(accesible);
        }
    }

    @Override
    public boolean equals(Object object) {
        return object == null ? false : toString().equals(object.toString());
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    /**
     * ID getter
     *
     * @return id
     */
    public BigDecimal getId() {
        return this.id;
    }

    /**
     * ID setter
     *
     * @param id
     */
    public void setId(BigDecimal id) {
        this.id = id;
    }

    /**
     * Calculate next sequence value for this model
     *
     * @return BigDecimal Next id
     */
    public BigDecimal getNextId() {
        return getSecuenceNextValue(this.getClass());
    }

    /**
     * Hook to set data to model when is deleted
     *
     * @param u Who's performing model delete
     */
    public void performDelete(User u) {
        for (String f : new String[]{"deletedBy", "deletedAt", "enabled"}) {
            try {
                Field attribute = this.getClass().getDeclaredField(f);
                boolean accesible = attribute.isAccessible();
                attribute.setAccessible(true);
                switch (f) {
                    case "deletedBy":
                        if (u == null || u.getId() == null) {
                            throw new RuntimeException("this action should be performed by a valid user!");
                        }
                        attribute.set(this, u.getId());
                        break;
                    case "deletedAt":
                        Date now = new Date();
                        attribute.set(this, new Timestamp(now.getTime()));
                        break;
                    case "enabled":
                        attribute.set(this, "0");
                        break;
                }
                attribute.setAccessible(accesible);
            } catch (NoSuchFieldException _ex) {
                // Skip exception if field is not defined in this context
            } catch (IllegalAccessException | RuntimeException ex) {
                getLogger(AbstractModel.class).error(ex.getMessage(), ex);
            }
        }
    }

    @Override
    public String toString() {
        ArrayList<String> fieldsData = new ArrayList<>();
        BigDecimal contextId = this.getId();
        fieldsData.add("id=" + (contextId == null ? null : contextId.intValueExact()));
        Class klazz = this.getClass();
        for (Field attribute : klazz.getDeclaredFields()) {
            int mod = attribute.getModifiers();
            if (Modifier.isAbstract(mod) || Modifier.isFinal(mod) | Modifier.isStatic(mod)) {
                continue;
            }
            try {
                boolean accesible = attribute.isAccessible();
                attribute.setAccessible(true);
                String prefix = "get";
                Method getter;
                switch (attribute.getName()) {
                    case "id":
                        getter = klazz
                                .getSuperclass()
                                .getDeclaredMethod(prefix + pascalize(attribute.getName()),
                                        new Class[]{});
                        break;
                    case "empty":
                    case "enabled":
                    case "handicapped":
                    case "inmutable":
                        prefix = "is";
                        getter = klazz
                                .getDeclaredMethod(prefix + pascalize(attribute.getName()),
                                        new Class[]{});
                        break;
                    default:
                        getter = klazz
                                .getDeclaredMethod(prefix + pascalize(attribute.getName()),
                                        new Class[]{});
                }
                fieldsData.add(attribute.getName() + "=" + getter.invoke(this));
                attribute.setAccessible(accesible);
            } catch (SecurityException | IllegalAccessException
                    | IllegalArgumentException | InvocationTargetException ex) {
                getLogger(AbstractModel.class).warn(ex.getMessage(), ex);
            } catch (Exception ex) {
                getLogger(AbstractModel.class).error(ex.getMessage(), ex);
            }
        }
        return this.getClass().getName() + "{" + join(", ", fieldsData.toArray(new String[fieldsData.size()])) + "}";
    }
}
