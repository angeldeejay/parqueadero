/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.util;

import java.math.BigDecimal;
import parqueadero.model.User;

/**
 * InterfaceDAO class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 * @param <T>
 */
public interface InterfaceDAO<T extends AbstractModel> {

    /**
     * Get a record from database by id
     *
     * @param id model id
     * @return T
     */
    public T getById(Integer id);

    /**
     * Adds a record to database
     * 
     * @param model the raw model data
     * @return model id
     */
    public BigDecimal add(T model);

    /**
     * Update a record into database
     *
     * @param model the raw model data
     * @return success status
     */
    public boolean update(T model);

    /**
     * Delete a record from database
     *
     * @param model the raw model data
     * @return success status
     */
    public boolean delete(T model, User performer);
}
