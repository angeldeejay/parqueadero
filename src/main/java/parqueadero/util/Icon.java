/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.util;

import java.awt.Color;
import static java.awt.Color.DARK_GRAY;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import static java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment;
import java.awt.Image;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;
import jiconfont.icons.font_awesome.FontAwesome;
import static jiconfont.icons.font_awesome.FontAwesome.getIconFont;
import static jiconfont.swing.IconFontSwing.buildIcon;
import static jiconfont.swing.IconFontSwing.register;

/**
 * Icon class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class Icon {

    public static javax.swing.Icon getIcon(FontAwesome icon, int size, Color color) {
        register(getIconFont());
        return buildIcon(icon, size, color);
    }

    public static javax.swing.Icon getIcon(FontAwesome icon, Color color) {
        return getIcon(icon, 16, color);
    }

    public static javax.swing.Icon getIcon(FontAwesome icon, int size) {
        return getIcon(icon, size, DARK_GRAY);
    }

    public static javax.swing.Icon getIcon(FontAwesome icon) {
        return getIcon(icon, 16);
    }

    public static Image getImage(FontAwesome icon, int size, Color color) {
        javax.swing.Icon rawIcon = getIcon(icon, size, color);
        if (rawIcon instanceof ImageIcon) {
            return ((ImageIcon) rawIcon).getImage();
        } else {
            int w = rawIcon.getIconWidth();
            int h = rawIcon.getIconHeight();
            GraphicsEnvironment ge
                = getLocalGraphicsEnvironment();
            GraphicsDevice gd = ge.getDefaultScreenDevice();
            GraphicsConfiguration gc = gd.getDefaultConfiguration();
            BufferedImage image = gc.createCompatibleImage(w, h);
            Graphics2D g = image.createGraphics();
            rawIcon.paintIcon(null, g, 0, 0);
            g.dispose();
            return image;
        }
    }

    public static Image getImage(FontAwesome icon, Color color) {
        return getImage(icon, 16, color);
    }

    public static Image getImage(FontAwesome icon, int size) {
        return getImage(icon, size, DARK_GRAY);
    }

    public static Image getImage(FontAwesome icon) {
        return getImage(icon, 16);
    }
}
