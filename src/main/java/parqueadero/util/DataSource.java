/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.util;

import static java.lang.Class.forName;
import static java.lang.Integer.parseInt;
import static java.lang.String.format;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import static java.sql.ResultSet.CONCUR_READ_ONLY;
import static java.sql.ResultSet.TYPE_SCROLL_INSENSITIVE;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Properties;
import static parqueadero.util.Config.getSettings;
import static parqueadero.util.Inflector.camelize;
import static parqueadero.util.Inflector.pluralize;
import static parqueadero.util.Logger.getLogger;

/**
 * DataSource class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class DataSource {

    private static final String CONNECTION_STRING = "jdbc:oracle:thin:@//%s:%d/%s";
    private static final String QUERY_IS_CONNECTED = "SELECT SYSDATE FROM DUAL";
    private static final String QUERY_SEQUENCE_NEXTVAL = "SELECT %s_id_seq.NEXTVAL AS id FROM DUAL";

    private static Connection create() throws SQLException {
        Properties config = getSettings("db");
        try {
            forName("oracle.jdbc.driver.OracleDriver");
            return DriverManager.getConnection(format(CONNECTION_STRING,
                    config.getProperty("hostname"),
                    parseInt(config.getProperty("port")),
                    config.getProperty("SID")), config);
        } catch (NumberFormatException ex) {
            getLogger(DataSource.class).error("Configuration error: Port should be a valid number", ex);
        } catch (ClassNotFoundException ex) {
            getLogger(DataSource.class).error("Could not load database driver", ex);
        }
        return null;
    }

    public static boolean isValidConfig() {
        try {
            return executeQuery(QUERY_IS_CONNECTED, false).size() == 1;
        } catch (Exception ex) {
            getLogger(DataSource.class).error(ex.getMessage(), ex);
        }
        return false;
    }

    public static void killConnections(PreparedStatement statement, Connection conn) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException ex) {
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
            }
        }
    }

    public static ArrayList<HashMap<String, Object>> executeQuery(String query, Object[] params, boolean logQuery) {
        Connection conn = null;
        PreparedStatement statement = null;
        ArrayList<HashMap<String, Object>> rows = new ArrayList<>();
        if (params == null) {
            params = new Object[]{};
        }
        if (logQuery) {
            getLogger(DataSource.class).trace("\n - Query: " + query.replaceAll("\n", " ")
                    + "\n - Parameters: " + Arrays.toString(params));
        }
        try {
            conn = create();
            conn.setAutoCommit(false);
            statement = conn.prepareStatement(query, TYPE_SCROLL_INSENSITIVE, CONCUR_READ_ONLY);
            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }
            ResultSet rs = statement.executeQuery();
            conn.commit();
            // Row processing
            ResultSetMetaData rsmd = rs.getMetaData();
            int cols = rsmd.getColumnCount();
            while (rs.next()) {
                HashMap<String, Object> row = new HashMap<>();
                for (int i = 1; i <= cols; i++) {
                    row.put(camelize(rsmd.getColumnName(i)), rs.getObject(i));
                }
                rows.add(row);
            }
        } catch (SQLException ex) {
            getLogger(DataSource.class).error("Error running query", ex);
        } catch (Exception ex) {
            getLogger(DataSource.class).error("Runtime error", new RuntimeException(ex));
        } finally {
            killConnections(statement, conn);
        }
        return rows;
    }

    public static ArrayList<HashMap<String, Object>> executeQuery(String query, Object[] params) {
        return executeQuery(query, params, true);
    }

    public static ArrayList<HashMap<String, Object>> executeQuery(String query, boolean logQuery) {
        return executeQuery(query, new Object[]{}, logQuery);
    }

    public static ArrayList<HashMap<String, Object>> executeQuery(String query) {
        return executeQuery(query, new Object[]{}, true);
    }

    public static ArrayList<HashMap<String, Object>> executeUpdate(String query, Object[] params, boolean logQuery) throws SQLException {
        Connection conn = null;
        PreparedStatement statement = null;
        ArrayList<HashMap<String, Object>> rows = new ArrayList<>();
        if (params == null) {
            params = new Object[]{};
        }
        if (logQuery) {
            getLogger(DataSource.class).trace("\n - Query: " + query.replaceAll("\n", " ")
                    + "\n - Parameters: " + Arrays.toString(params));
        }
        try {
            conn = create();
            conn.setAutoCommit(false);
            statement = conn.prepareStatement(query, TYPE_SCROLL_INSENSITIVE, CONCUR_READ_ONLY);
            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }
            int rowCount = statement.executeUpdate();
            conn.commit();
            if (rowCount > 0) {
                try {
                    ResultSet rs = statement.executeQuery();
                    // Row processing
                    ResultSetMetaData rsmd = rs.getMetaData();
                    int cols = rsmd.getColumnCount();
                    while (rs.next()) {
                        HashMap<String, Object> row = new HashMap<>();
                        for (int i = 1; i <= cols; i++) {
                            row.put(camelize(rsmd.getColumnName(i)), rs.getObject(i));
                        }
                        rows.add(row);
                    }
                } catch (Exception _ex) {
                }
            }
        } catch (SQLException ex) {
            throw ex;
        } catch (Exception ex) {
            getLogger(DataSource.class).error("Runtime error", new RuntimeException(ex));
        } finally {
            killConnections(statement, conn);
        }
        return rows;
    }

    public static ArrayList<HashMap<String, Object>> executeUpdate(String query, Object[] params) throws SQLException {
        return executeUpdate(query, params, true);
    }

    public static ArrayList<HashMap<String, Object>> executeUpdate(String query, boolean logQuery) throws SQLException {
        return executeUpdate(query, new Object[]{}, logQuery);
    }

    public static ArrayList<HashMap<String, Object>> executeUpdate(String query) throws SQLException {
        return executeUpdate(query, new Object[]{}, true);
    }

    static BigDecimal getSecuenceNextValue(Class<? extends AbstractModel> modelClass) {
        BigDecimal nextId = null;
        try {
            String tablename = pluralize(modelClass.getSimpleName()).toLowerCase();
            HashMap<String, Object> sequenceData = executeUpdate(format(QUERY_SEQUENCE_NEXTVAL, tablename)).get(0);
            nextId = (BigDecimal) sequenceData.get("id");
        } catch (Exception ex) {
            getLogger(DataSource.class).error(ex.getMessage(), ex);
        }
        return nextId;
    }
}
