/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parqueadero.dao;

import java.math.BigDecimal;
import java.util.HashMap;
import org.apache.ibatis.jdbc.SQL;
import parqueadero.model.Bike;
import parqueadero.model.User;
import static parqueadero.util.DataSource.executeQuery;
import static parqueadero.util.DataSource.executeUpdate;
import parqueadero.util.InterfaceDAO;
import static parqueadero.util.Logger.getLogger;

/**
 *
 * @author avanegas
 */
public class BikeDAO implements InterfaceDAO<Bike> {

    @Override
    public Bike getById(Integer id) {
        String query = new SQL() {
            {
                SELECT("b.id, b.type, b.color, b.observations");
                FROM("bikes b");
                WHERE("b.id = ?");
                WHERE("ROWNUM = 1");
            }
        }.toString();
        for (HashMap data : executeQuery(query, new Integer[]{id})) {
            return new Bike(data);
        }
        return null;
    }

    /**
     *
     * @param model the value of model
     * @return the java.math.BigDecimal
     */
    @Override
    public BigDecimal add(Bike model) {
        String query = new SQL() {
            {
                INSERT_INTO("bikes");
                VALUES("id, type, color, observations", "?, ?, ?, ?");
            }
        }.toString();
        try {
            executeUpdate(query, new Object[]{
                model.getId().intValueExact(),
                model.getType(),
                model.getColor(),
                model.getObservations()
            });
            return model.getId();
        } catch (Exception ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
        return null;
    }

    @Override
    public boolean update(Bike model) {
        String query = new SQL() {
            {
                UPDATE("bikes");
                SET("type = ?");
                SET("color = ?");
                SET("observations = ?");
                WHERE("id = ?");
            }
        }.toString();
        try {
            executeUpdate(query, new Object[]{
                model.getType(),
                model.getColor(),
                model.getObservations(),
                model.getId().intValueExact()
            });
            return true;
        } catch (Exception ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
        return false;
    }

    @Override
    public boolean delete(Bike model, User __) {
        String query = new SQL() {
            {
                DELETE_FROM("bikes");
                WHERE("id = ?");
            }
        }.toString();
        try {
            executeUpdate(query, new Object[]{
                model.getId().intValueExact()
            });
            return true;
        } catch (Exception ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
        return false;
    }

}
