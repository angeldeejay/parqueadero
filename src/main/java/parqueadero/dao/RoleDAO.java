/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import org.apache.ibatis.jdbc.SQL;
import parqueadero.model.Permission;
import parqueadero.model.Role;
import parqueadero.model.User;
import static parqueadero.util.DataSource.executeQuery;
import static parqueadero.util.DataSource.executeUpdate;
import parqueadero.util.InterfaceDAO;
import static parqueadero.util.Logger.getLogger;

/**
 * RoleDAO class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class RoleDAO implements InterfaceDAO<Role> {

    /**
     *
     * @param model the value of model
     * @return the java.math.BigDecimal
     */
    @Override
    public BigDecimal add(Role model) {
        String query = new SQL() {
            {
                INSERT_INTO("roles");
                VALUES("id, name, enabled, inmutable",
                        "?, ?, ?, ?");
            }
        }.toString();
        try {
            BigDecimal newId = model.getNextId();
            model.setId(newId);
            executeUpdate(query, new Object[]{
                model.getId().intValueExact(),
                model.getName(),
                model.isEnabled() ? "1" : "0",
                model.isInmutable() ? "1" : "0"
            });
            executeQuery(new SQL() {
                {
                    DELETE_FROM("role_permissions");
                    WHERE("role_id = ?");
                }
            }.toString(), new BigDecimal[]{newId});
            for (Permission p : model.getPermissions()) {
                if (p.getId() != null) {
                    executeQuery(new SQL() {
                        {
                            INSERT_INTO("role_permissions");
                            VALUES("role_id, permission_id",
                                    "?, ?");
                        }
                    }.toString(), new BigDecimal[]{newId, p.getId()});
                }
            }
            return model.getId();
        } catch (Exception ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
        return null;
    }

    @Override
    public boolean delete(Role model, User performer) {
        model.performDelete(performer);
        return update(model);
    }

    @Override
    public Role getById(Integer id) {
        String query = new SQL() {
            {
                SELECT("e.*");
                FROM("roles e");
                WHERE("e.id = ?");
                WHERE("ROWNUM = 1");
            }
        }.toString();
        for (HashMap data : executeQuery(query, new Integer[]{id})) {
            return new Role(data);
        }
        return null;
    }

    public Role getByName(String name) {
        String query = new SQL() {
            {
                SELECT("e.*");
                FROM("roles e");
                WHERE("UPPER(e.name) = ?");
                WHERE("ROWNUM = 1");
            }
        }.toString();
        for (HashMap data : executeQuery(query, new String[]{name})) {
            return new Role(data);
        }
        return null;
    }

    public Collection<Role> getByUserId(Integer id, boolean allowDisabled) {
        Collection<Role> results = new ArrayList();
        String query = new SQL() {
            {
                SELECT("r.*");
                FROM("roles r");
                INNER_JOIN("user_roles ur ON ur.role_id = r.id AND ur.user_id = ?");
                WHERE("r.enabled IN (1, ?)");
            }
        }.toString();
        for (HashMap data : executeQuery(query, new Object[]{
            id,
            allowDisabled ? "0" : "1"
        })) {
            results.add(new Role(data));
        }
        return results;
    }

    @Override
    public boolean update(Role model) {
        String query = new SQL() {
            {
                UPDATE("roles");
                SET("name = UPPER(?)");
                WHERE("id = ?");
            }
        }.toString();
        try {
            executeUpdate(query, new Object[]{
                model.getName().trim().toUpperCase(),
                model.getId().intValueExact()
            });
            executeQuery(new SQL() {
                {
                    DELETE_FROM("role_permissions");
                    WHERE("role_id = ?");
                }
            }.toString(), new BigDecimal[]{model.getId()});
            for (Permission p : model.getPermissions()) {
                if (p.getId() != null) {
                    executeQuery(new SQL() {
                        {
                            INSERT_INTO("role_permissions");
                            VALUES("role_id, permission_id",
                                    "?, ?");
                        }
                    }.toString(), new BigDecimal[]{model.getId(), p.getId()});
                }
            }
            return true;
        } catch (Exception ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
        return false;
    }
}
