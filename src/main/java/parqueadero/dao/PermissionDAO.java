/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import org.apache.ibatis.jdbc.SQL;
import parqueadero.model.Permission;
import parqueadero.model.User;
import static parqueadero.util.DataSource.executeQuery;
import static parqueadero.util.DataSource.executeUpdate;
import parqueadero.util.InterfaceDAO;
import static parqueadero.util.Logger.getLogger;

/**
 * PermissionDAO class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class PermissionDAO implements InterfaceDAO<Permission> {

    /**
     *
     * @param model the value of model
     * @return the java.math.BigDecimal
     */
    @Override
    public BigDecimal add(Permission model) {
        String query = new SQL() {
            {
                INSERT_INTO("permissions");
                VALUES("id, name, code, enabled",
                        "?, ?, ?, ?");
            }
        }.toString();
        try {
            model.setId(model.getNextId());
            executeUpdate(query, new Object[]{
                model.getId().intValueExact(),
                model.getName(),
                model.getCode(),
                model.isEnabled() ? "1" : "0"
            });
            return model.getId();
        } catch (Exception ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
        return null;
    }

    @Override
    public boolean delete(Permission model, User performer) {
        String query = new SQL() {
            {
                DELETE_FROM("permission");
                WHERE("id = ?");
            }
        }.toString();
        try {
            executeUpdate(query, new Object[]{
                model.getId().intValueExact()
            });
            return true;
        } catch (Exception ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
        return false;
    }

    @Override
    public Permission getById(Integer id) {
        String query = new SQL() {
            {
                SELECT("p.*");
                FROM("permissions p");
                WHERE("p.id = ?");
                WHERE("ROWNUM = 1");
            }
        }.toString();
        for (HashMap data : executeQuery(query, new Integer[]{id})) {
            return new Permission(data);
        }
        return null;
    }

    public Collection<Permission> getByRoleId(Integer id, boolean allowDisabled) {
        Collection<Permission> results = new ArrayList();
        String query = new SQL() {
            {
                SELECT("p.*");
                FROM("permissions p");
                INNER_JOIN("role_permissions rp ON rp.permission_id = p.id AND rp.role_id = ?");
                WHERE("p.enabled IN (1, ?)");
            }
        }.toString();
        for (HashMap data : executeQuery(query, new Object[]{
            id,
            allowDisabled ? "0" : "1"
        })) {
            results.add(new Permission(data));
        }
        return results;
    }

    @Override
    public boolean update(Permission model) {
        String query = new SQL() {
            {
                UPDATE("permissions");
                SET("name = ?");
                SET("code = ?");
                SET("enabled = ?");
                WHERE("id = ?");
            }
        }.toString();
        try {
            executeUpdate(query, new Object[]{
                model.getName(),
                model.getCode(),
                model.isEnabled() ? "1" : "0",
                model.getId().intValueExact()
            });
            return true;
        } catch (Exception ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
        return false;
    }
}
