/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.dao;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.ibatis.jdbc.SQL;
import parqueadero.model.Lot;
import parqueadero.model.User;
import static parqueadero.util.DataSource.executeQuery;
import static parqueadero.util.DataSource.executeUpdate;
import parqueadero.util.InterfaceDAO;
import static parqueadero.util.Logger.getLogger;

/**
 * LotDAO class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class LotDAO implements InterfaceDAO<Lot> {

    @Override
    public Lot getById(Integer id) {
        String query = new SQL() {
            {
                SELECT("l.id, l.code, l.empty, l.zone_id, l.allowed_type, "
                        + "l.handicapped, l.enabled, l.deleted_by, l.deleted_at");
                FROM("lots l");
                WHERE("l.id = ?");
                WHERE("ROWNUM = 1");
            }
        }.toString();
        for (HashMap data : executeQuery(query, new Integer[]{id})) {
            return new Lot(data);
        }
        return null;
    }

    @Override
    public BigDecimal add(Lot model) {
        String query = new SQL() {
            {
                INSERT_INTO("lots");
                VALUES(
                        "id, code, empty, zone_id, allowed_type, handicapped, enabled",
                        "?, ?, ?, ?, ?, ?, ?");
            }
        }.toString();
        try {
            model.setId(model.getNextId());
            executeUpdate(query, new Object[]{
                model.getId().intValueExact(),
                model.getCode(),
                model.isEmpty() ? "1" : "0",
                model.getZone().getId(),
                model.getAllowedType(),
                model.isHandicapped() ? "1" : "0",
                model.isEnabled() ? "1" : "0"
            });
            return model.getId();
        } catch (SQLException ex) {
            Logger.getLogger(LotDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public boolean update(Lot model) {
        String query = new SQL() {
            {
                UPDATE("lots");
                SET("code = ?");
                SET("empty = ?");
                SET("zone_id = ?");
                SET("allowed_type = ?");
                SET("handicapped = ?");
                SET("enabled = ?");
                WHERE("id = ?");
            }
        }.toString();
        try {
            executeUpdate(query, new Object[]{
                model.getCode(),
                model.isEmpty() ? "1" : "0",
                model.getZone().getId(),
                model.getAllowedType(),
                model.isHandicapped() ? "1" : "0",
                model.isEnabled() ? "1" : "0",
                model.getId()
            });
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ZoneDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean delete(Lot model, User performer) {
        model.performDelete(performer);
        try {
            boolean result = this.update(model);
            try {
                if (model.isEnabled()) {
                    LotDAO dao = new LotDAO();
                    result = result && dao.delete(model, performer);
                }
            } catch (Exception _ex) {
                getLogger(this.getClass()).error(_ex.getMessage(), _ex);
            }
            return result;
        } catch (Exception ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
        return false;
    }

}
