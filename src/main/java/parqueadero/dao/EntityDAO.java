/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.dao;

import java.math.BigDecimal;
import java.util.HashMap;
import org.apache.ibatis.jdbc.SQL;
import parqueadero.model.Entity;
import parqueadero.model.Owner;
import parqueadero.model.User;
import static parqueadero.util.DataSource.executeQuery;
import static parqueadero.util.DataSource.executeUpdate;
import parqueadero.util.InterfaceDAO;
import static parqueadero.util.Logger.getLogger;

/**
 * EntityDAO class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class EntityDAO implements InterfaceDAO<Entity> {

    public Entity getByIdent(String identType, String identValue) {
        String query = new SQL() {
            {
                SELECT("e.*");
                SELECT("u.username, u.password, u.enabled, u.deleted_by, u.deleted_at");
                FROM("entities e");
                LEFT_OUTER_JOIN("users u ON u.id = e.id AND u.enabled = '1'");
                LEFT_OUTER_JOIN("owners o ON o.id = e.id AND o.enabled = '1'");
                WHERE("e.ident_type = ?");
                WHERE("e.ident_value = ?");
                WHERE("ROWNUM = 1");
            }
        }.toString();
        for (HashMap data : executeQuery(query, new String[]{identType, identValue})) {
            return new Entity(data);
        }
        return null;
    }

    @Override
    public Entity getById(Integer id) {
        String query = new SQL() {
            {
                SELECT("e.*");
                FROM("entities e");
                WHERE("e.id = ?");
                WHERE("ROWNUM = 1");
            }
        }.toString();
        for (HashMap data : executeQuery(query, new Integer[]{id})) {
            return new Entity(data);
        }
        return null;
    }

    /**
     *
     * @param model the value of model
     * @return the java.math.BigDecimal
     */
    @Override
    public BigDecimal add(Entity model) {
        String query = new SQL() {
            {
                INSERT_INTO("entities");
                VALUES("id, first_name, last_name, ident_type, ident_value",
                        "?, ?, ?, ?, ?");
            }
        }.toString();
        try {
            model.setId(model.getNextId());
            executeUpdate(query, new Object[]{
                model.getId().intValueExact(),
                model.getFirstName(),
                model.getLastName(),
                model.getIdentType(),
                model.getIdentValue()
            });
            try {
                User u = model.getUser();
                if (u != null) {
                    u.setId(model.getId());
                    UserDAO dao = new UserDAO();
                    dao.add(u);
                }
            } catch (Exception _ex) {
                getLogger(this.getClass()).error(_ex.getMessage(), _ex);
            }
            try {
                Owner o = model.getOwner();
                if (o != null) {
                    o.setId(model.getId());
                    OwnerDAO dao = new OwnerDAO();
                    dao.add(o);
                }
            } catch (Exception _ex) {
                getLogger(this.getClass()).error(_ex.getMessage(), _ex);
            }
            return model.getId();
        } catch (Exception ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
        return null;
    }

    @Override
    public boolean update(Entity model) {
        String query = new SQL() {
            {
                UPDATE("entities");
                SET("first_name = ?");
                SET("last_name = ?");
                SET("ident_type = ?");
                SET("ident_value = ?");
                WHERE("id = ?");
            }
        }.toString();
        try {
            executeUpdate(query, new Object[]{
                model.getFirstName(),
                model.getLastName(),
                model.getIdentType(),
                model.getIdentValue(),
                model.getId().intValueExact()
            });
            boolean result = true;
            try {
                User u = model.getUser();
                if (u != null) {
                    u.setId(model.getId());
                    UserDAO dao = new UserDAO();
                    result = result && dao.update(u);
                }
            } catch (Exception _ex) {
                getLogger(this.getClass()).error(_ex.getMessage(), _ex);
            }
            try {
                Owner o = model.getOwner();
                if (o != null) {
                    o.setId(model.getId());
                    OwnerDAO dao = new OwnerDAO();
                    if (dao.getById(o.getId().intValueExact()) == null) {
                        result = result && dao.add(o) != null;
                    } else {
                        result = result && dao.update(o);
                    }
                }
            } catch (Exception _ex) {
                getLogger(this.getClass()).error(_ex.getMessage(), _ex);
            }
            return result;
        } catch (Exception ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
        return false;
    }

    @Override
    public boolean delete(Entity model, User performer) {
        model.performDelete(performer);
        try {
            boolean result = this.update(model);
            try {
                User u = model.getUser();
                if (u != null && u.isEnabled()) {
                    UserDAO dao = new UserDAO();
                    result = result && dao.delete(u, performer);
                }
            } catch (Exception _ex) {
                getLogger(this.getClass()).error(_ex.getMessage(), _ex);
            }
            try {
                Owner o = model.getOwner();
                if (o != null && o.isEnabled()) {
                    OwnerDAO dao = new OwnerDAO();
                    result = result && dao.delete(o, performer);
                }
            } catch (Exception _ex) {
                getLogger(this.getClass()).error(_ex.getMessage(), _ex);
            }
            return result;
        } catch (Exception ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
        return false;
    }
}
