/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parqueadero.dao;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.ibatis.jdbc.SQL;
import parqueadero.model.User;
import parqueadero.model.Zone;
import static parqueadero.util.DataSource.executeQuery;
import static parqueadero.util.DataSource.executeUpdate;
import parqueadero.util.InterfaceDAO;

/**
 * ZoneDAO class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class ZoneDAO implements InterfaceDAO<Zone> {

    @Override
    public Zone getById(Integer id) {
        String query = new SQL() {
            {
                SELECT("z.*");
                FROM("zones z");
                WHERE("z.id = ?");
                WHERE("ROWNUM = 1");
            }
        }.toString();
        for (HashMap data : executeQuery(query, new Integer[]{id})) {
            return new Zone(data);
        }
        return null;
    }

    @Override
    public BigDecimal add(Zone model) {
        String query = new SQL() {
            {
                INSERT_INTO("zones");
                VALUES("id, code, enabled", "?, ?, ?");
            }
        }.toString();
        try {
            model.setId(model.getNextId());
            executeUpdate(query, new Object[]{
                model.getId().intValueExact(),
                model.getCode(),
                model.isEnabled()});
            return model.getId();
        } catch (SQLException ex) {
            Logger.getLogger(ZoneDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public boolean update(Zone model) {
        String query = new SQL() {
            {
                UPDATE("zones");
                SET("code = ?");
                SET("enabled= ?");
                WHERE("id = ?");
            }
        }.toString();
        try {
            executeUpdate(query, new Object[]{
                model.getCode(),
                model.isEnabled(),
                model.getId()
            });
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ZoneDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean delete(Zone model, User performer) {
        model.performDelete(performer);
        return update(model);
    }

}
