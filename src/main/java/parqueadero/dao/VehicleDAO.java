/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.dao;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.HashMap;
import org.apache.ibatis.jdbc.SQL;
import parqueadero.model.Bike;
import parqueadero.model.Car;
import parqueadero.model.Motorcycle;
import parqueadero.model.User;
import parqueadero.model.Vehicle;
import static parqueadero.util.DataSource.executeQuery;
import static parqueadero.util.DataSource.executeUpdate;
import parqueadero.util.InterfaceDAO;
import static parqueadero.util.Logger.getLogger;

/**
 * VehicleDAO class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class VehicleDAO implements InterfaceDAO<Vehicle> {

    private final BikeDAO bdao = new BikeDAO();
    private final CarDAO cdao = new CarDAO();
    private final MotorcycleDAO mdao = new MotorcycleDAO();

    public Vehicle getByIdent(String vehicleType, Object[] params) {
        SQL query = new SQL() {
            {
                SELECT("v.*");
                FROM("Vehicles v");
                WHERE("ROWNUM = 1");
                LEFT_OUTER_JOIN("bikes b ON v.vehicle_type = 'B' "
                        + "AND b.id = v.id");
                LEFT_OUTER_JOIN("cars c ON v.vehicle_type = 'C' "
                        + "AND c.id = v.id");
                LEFT_OUTER_JOIN("motorcycles m ON v.vehicle_type = 'M' "
                        + "AND m.id = v.id");
                switch (vehicleType) {
                    case Vehicle.BIKE:
                        WHERE("v.vehicle_type = 'B' "
                                + "AND b.type = ?"
                                + "AND b.color = ?");
                        break;
                    case Vehicle.CAR:
                        WHERE("v.vehicle_type = 'C' "
                                + "AND c.plate = ?");
                        break;
                    case Vehicle.MOTORCYCLE:
                        WHERE("v.vehicle_type = 'M' "
                                + "AND m.plate = ?");
                        break;
                }
            }
        };
        for (HashMap data : executeQuery(query.toString(), params)) {
            return new Vehicle(data);
        }
        return null;
    }

    @Override
    public Vehicle getById(Integer id) {
        String query = new SQL() {
            {
                SELECT("v.*");
                SELECT("b.color, b.type, b.observations");
                SELECT("c.plate, c.model, c.city, c.serial, c.color");
                SELECT("m.plate, m.model, m.city, m.serial, m.color");
                FROM("vehicles v");
                WHERE("v.id = ?");
                WHERE("ROWNUM = 1");
                LEFT_OUTER_JOIN("bikes b ON v.vehicle_type = 'B' "
                        + "AND b.id = v.id");
                LEFT_OUTER_JOIN("cars c ON v.vehicle_type = 'C' "
                        + "AND c.id = v.id");
                LEFT_OUTER_JOIN("motorcycles m ON v.vehicle_type = 'M' "
                        + "AND m.id = v.id");
            }
        }.toString();
        for (HashMap data : executeQuery(query, new Integer[]{id})) {
            return new Vehicle(data);
        }
        return null;
    }

    /**
     *
     * @param model the value of model
     * @return the java.math.BigDecimal
     */
    @Override
    public BigDecimal add(Vehicle model) {
        String query = new SQL() {
            {
                INSERT_INTO("vehicles");
                VALUES("id, vehicle_type", "?, ?");
            }
        }.toString();
        try {
            model.setId(model.getNextId());
            executeUpdate(query, new Object[]{
                model.getId().intValueExact(),
                model.getVehicleType()
            });
            generateTypesData(model);
            return model.getId();
        } catch (Exception ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
        return null;
    }

    @Override
    public boolean update(Vehicle model) {
        String query = new SQL() {
            {
                UPDATE("vehicles");
                SET("vehicle_type = ?");
                WHERE("id = ?");
            }
        }.toString();
        try {
            boolean result = true;
            cleanTypesData(model);
            executeUpdate(query, new Object[]{
                model.getVehicleType(),
                model.getId().intValueExact()
            });
            return result && generateTypesData(model);
        } catch (Exception ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
        return false;
    }

    @Override
    public boolean delete(Vehicle model, User performer) {
        model.performDelete(performer);
        try {
            boolean result = true;
            try {
                cleanTypesData(model);
                String query = new SQL() {
                    {
                        DELETE_FROM("vehicles");
                        WHERE("id = ?");
                    }
                }.toString();
                executeUpdate(query, new Object[]{
                    model.getId().intValueExact()
                });
                return result;
            } catch (Exception _ex) {
                getLogger(this.getClass()).error(_ex.getMessage(), _ex);
            }
            return result;
        } catch (Exception ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
        return false;
    }

    private void cleanTypesData(Vehicle model) throws SQLException {
        for (String table : new String[]{"bikes", "cars", "motorcycles"}) {
            String cleanQuery = new SQL() {
                {
                    DELETE_FROM(table);
                    WHERE("id = ?");
                }
            }.toString();
            executeUpdate(cleanQuery, new Object[]{
                model.getId().intValueExact()
            });
        }
    }

    private boolean generateTypesData(Vehicle model) {
        switch (model.getVehicleType()) {
            case Vehicle.BIKE:
                try {
                    Bike b = model.getBike();
                    if (b != null) {
                        b.setId(model.getId());
                        return bdao.add(b) != null;
                    }
                } catch (Exception _ex) {
                    getLogger(this.getClass()).error(_ex.getMessage(), _ex);
                }
                break;
            case Vehicle.CAR:
                try {
                    Car c = model.getCar();
                    if (c != null) {
                        c.setId(model.getId());
                        return cdao.add(c) != null;
                    }
                } catch (Exception _ex) {
                    getLogger(this.getClass()).error(_ex.getMessage(), _ex);
                }
                break;
            case Vehicle.MOTORCYCLE:
                try {
                    Motorcycle m = model.getMotorcycle();
                    if (m != null) {
                        m.setId(model.getId());
                        return mdao.add(m) != null;
                    }
                } catch (Exception _ex) {
                    getLogger(this.getClass()).error(_ex.getMessage(), _ex);
                }
                break;
        }
        return false;
    }
}
