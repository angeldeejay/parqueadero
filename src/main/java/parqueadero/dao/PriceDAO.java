/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.dao;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.ibatis.jdbc.SQL;
import parqueadero.model.Price;
import parqueadero.model.User;
import static parqueadero.util.DataSource.executeQuery;
import static parqueadero.util.DataSource.executeUpdate;
import parqueadero.util.InterfaceDAO;
import static parqueadero.util.Logger.getLogger;

/**
 * PriceDAO class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class PriceDAO implements InterfaceDAO<Price> {

    public Price getByIdent(String vehicleType, BigDecimal ownerId, BigDecimal vehicleId) {
        ArrayList<Object> params = new ArrayList<>();
        String query = new SQL() {
            {
                SELECT("p.*");
                FROM("prices p");
                WHERE("p.vehicle_type " + (vehicleType != null ? "= ?" : "IS NULL"));
                WHERE("p.owner_id " + (ownerId != null ? "= ?" : "IS NULL"));
                WHERE("p.vehicle_id " + (vehicleId != null ? "= ?" : "IS NULL"));
                WHERE("ROWNUM = 1");
            }
        }.toString();
        if (vehicleType != null) {
            params.add(vehicleType);
        }
        if (ownerId != null) {
            params.add(ownerId);
        }
        if (vehicleId != null) {
            params.add(vehicleId);
        }
        for (HashMap data : executeQuery(query, params.toArray(new Object[]{}))) {
            return new Price(data);
        }
        return null;
    }

    @Override
    public Price getById(Integer id) {
        String query = new SQL() {
            {
                SELECT("p.id, p.value, p.vehicle_type, p.owner_id, p.vehicle_id");
                FROM("prices p");
                WHERE("p.id = ?");
                WHERE("ROWNUM = 1");
            }
        }.toString();
        for (HashMap data : executeQuery(query, new Integer[]{id})) {
            return new Price(data);
        }
        return null;
    }

    @Override
    public BigDecimal add(Price model) {
        String query = new SQL() {
            {
                INSERT_INTO("prices");
                VALUES(
                        "id, value, vehicle_type, owner_id, vehicle_id",
                        "?, ?, ?, ?, ?");
            }
        }.toString();
        try {
            model.setId(model.getNextId());
            executeUpdate(query, new Object[]{
                model.getId().intValueExact(),
                model.getValue(),
                model.getVehicleType(),
                model.getOwnerId(),
                model.getVehicleId()
            });
            return model.getId();
        } catch (SQLException ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
        return null;
    }

    @Override
    public boolean update(Price model) {
        String query = new SQL() {
            {
                UPDATE("prices");
                SET("value = ?");
                SET("vehicle_type = ?");
                SET("owner_id = ?");
                SET("vehicle_id = ?");
                WHERE("id = ?");
            }
        }.toString();
        try {
            executeUpdate(query, new Object[]{
                model.getValue(),
                model.getVehicleType(),
                model.getOwnerId(),
                model.getVehicleId(),
                model.getId()
            });
            return true;
        } catch (SQLException ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
        return false;
    }

    @Override
    public boolean delete(Price model, User performer) {
        String query = new SQL() {
            {
                DELETE_FROM("prices");
                WHERE("id = ?");
            }
        }.toString();
        try {
            executeUpdate(query, new Object[]{
                model.getId().intValueExact()
            });
            return true;
        } catch (Exception ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
        return false;
    }
}
