/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.dao;

import java.math.BigDecimal;
import java.util.HashMap;
import org.apache.ibatis.jdbc.SQL;
import parqueadero.model.Owner;
import parqueadero.model.User;
import static parqueadero.util.DataSource.executeQuery;
import static parqueadero.util.DataSource.executeUpdate;
import parqueadero.util.InterfaceDAO;
import static parqueadero.util.Logger.getLogger;

/**
 * OwnerDAO class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class OwnerDAO implements InterfaceDAO<Owner> {

    @Override
    public Owner getById(Integer id) {
        String query = new SQL() {
            {
                SELECT("*");
                FROM("owners o");
                WHERE("o.id = ?");
                WHERE("ROWNUM = 1");
            }
        }.toString();
        for (HashMap data : executeQuery(query, new Integer[]{id})) {
            return new Owner(data);
        }
        return null;
    }

    /**
     *
     * @param model the value of model
     * @return the java.math.BigDecimal
     */
    @Override
    public BigDecimal add(Owner model) {
        String query = new SQL() {
            {
                INSERT_INTO("owners");
                VALUES("id, handicapped", "?, ?");
            }
        }.toString();
        try {
            executeUpdate(query, new Object[]{
                model.getId().intValueExact(),
                model.isHandicapped() ? "1" : "0"
            });
            return model.getId();
        } catch (Exception ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
        return null;
    }

    @Override
    public boolean update(Owner model) {
        String query = new SQL() {
            {
                UPDATE("owners");
                SET("handicapped = ?");
                SET("enabled = ?");
                SET("deleted_by = ?");
                SET("deleted_at = ?");
                WHERE("id = ?");
            }
        }.toString();
        try {
            executeUpdate(query, new Object[]{
                model.isHandicapped() ? "1" : "0",
                model.isEnabled() ? "1" : "0",
                model.getDeletedBy(),
                model.getDeletedAt(),
                model.getId().intValueExact()
            });
            return true;
        } catch (Exception ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
        return false;
    }

    @Override
    public boolean delete(Owner model, User performer) {
        model.performDelete(performer);
        return update(model);
    }
}
