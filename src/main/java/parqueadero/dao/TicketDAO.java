/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.dao;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.ibatis.jdbc.SQL;
import parqueadero.model.Ticket;
import parqueadero.model.User;
import static parqueadero.util.DataSource.executeQuery;
import static parqueadero.util.DataSource.executeUpdate;
import parqueadero.util.InterfaceDAO;

/**
 * PricesDAO class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class TicketDAO implements InterfaceDAO<Ticket> {

    @Override
    public Ticket getById(Integer id) {
        String query = new SQL() {
            {
                SELECT("t.id, t.checkin, t.checkout, t.end_price, t.observations, "
                        + "t.owner_id, t.vehicle_id, t.lot_id, t.user_id");
                SELECT("o.handicapped, o.enabled, o.deleted_by, o.deleted_at");
                SELECT("u.username, u.password, u.enabled, u.deleted_by, u.deleted_at");
                SELECT("l.code, l.empty, l.allowed_type, l.handicapped, l.deleted_by, "
                        + "l.deleted_at, l.enabled, l.zone_id");
                SELECT("v.vehicle_type");
                FROM("tickets t");
                INNER_JOIN("users u ON u.id = t.user_id");
                INNER_JOIN("lots l ON l.id = t.lot_id");
                INNER_JOIN("owners o ON o.id = t.owner_id");
                INNER_JOIN("vehicles v ON v.id = t.vehicle_id");
                WHERE("t.id = ?");
                WHERE("ROWNUM = 1");
            }
        }.toString();
        for (HashMap data : executeQuery(query, new Integer[]{id})) {
            return new Ticket(data);
        }
        return null;
    }

    @Override
    public BigDecimal add(Ticket model) {
        String query = new SQL() {
            {
                INSERT_INTO("tickets");
                VALUES("id, checkin, checkout, end_price, observations, owner_id, "
                        + "vehicle_id, lot_id, user_id",
                        "?, ?, ?, ?, ?, ?, ?, ?, ?");
            }
        }.toString();
        try {
            model.setId(model.getNextId());
            executeUpdate(query, new Object[]{
                model.getId().intValueExact(),
                model.getCheckin(),
                model.getCheckout(),
                model.getEndPrice(),
                model.getObservations(),
                model.getOwnerId(),
                model.getVehicleId(),
                model.getLotId(),
                model.getUserId()
            });
            return model.getId();
        } catch (SQLException ex) {
            Logger.getLogger(LotDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public boolean update(Ticket model) {
        String query = new SQL() {
            {
                UPDATE("tickets");
                SET("checkin = ?");
                SET("checkout = ?");
                SET("end_price = ?");
                SET("observations = ?");
                SET("owner_id = ?");
                SET("vehicle_id = ?");
                SET("lot_id = ?");
                SET("user_id = ?");
                WHERE("id = ?");
            }
        }.toString();
        try {
            executeUpdate(query, new Object[]{
                model.getCheckin(),
                model.getCheckout(),
                model.getEndPrice(),
                model.getObservations(),
                model.getOwnerId(),
                model.getVehicleId(),
                model.getLotId(),
                model.getUserId(),
                model.getId().intValueExact()
            });
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ZoneDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean delete(Ticket model, User performer) {
        model.performDelete(performer);
        return update(model);
    }

}
