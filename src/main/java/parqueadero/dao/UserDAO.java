/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.dao;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import org.apache.ibatis.jdbc.SQL;
import parqueadero.model.Role;
import parqueadero.model.User;
import static parqueadero.util.DataSource.executeQuery;
import static parqueadero.util.DataSource.executeUpdate;
import parqueadero.util.InterfaceDAO;
import static parqueadero.util.Logger.getLogger;

/**
 * UserDAO class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class UserDAO implements InterfaceDAO<User> {

    /**
     *
     * @param model the value of model
     * @return the java.math.BigDecimal
     */
    @Override
    public BigDecimal add(User model) {
        String query = new SQL() {
            {
                INSERT_INTO("users");
                VALUES("id, username, password", "?, ?, ?");
            }
        }.toString();
        try {
            BigDecimal newId = model.getNextId();
            model.setId(newId);
            executeUpdate(query, new Object[]{
                model.getId().intValueExact(),
                model.getUsername(),
                model.getPassword()
            });
            executeQuery(new SQL() {
                {
                    DELETE_FROM("user_roles");
                    WHERE("user_id = ?");
                }
            }.toString(), new BigDecimal[]{newId});
            for (Role r : model.getRoles()) {
                if (r.getId() != null) {
                    executeQuery(new SQL() {
                        {
                            INSERT_INTO("user_roles");
                            VALUES("user_id, role_id",
                                    "?, ?");
                        }
                    }.toString(), new BigDecimal[]{newId, r.getId()});
                }
            }
            return model.getId();
        } catch (Exception ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
        return null;
    }

    @Override
    public boolean delete(User model, User performer) {
        model.performDelete(performer);
        return update(model);
    }

    @Override
    public User getById(Integer id) {
        String query = new SQL() {
            {
                SELECT("u.id, u.username, u.password, u.enabled, u.deleted_by, u.deleted_at");
                FROM("users u");
                WHERE("u.id = ?");
                WHERE("ROWNUM = 1");
            }
        }.toString();
        for (HashMap data : executeQuery(query, new Integer[]{id})) {
            return new User(data);
        }
        return null;
    }

    public boolean hasPermissions(BigDecimal userId, String[] permissionCodes, boolean hasAll, boolean negate) {
        return hasPermissions(userId.intValue(), permissionCodes, hasAll, negate);
    }

    public boolean hasPermissions(Integer userId, String[] permissionCodes, boolean hasAll, boolean negate) {
        String query = new SQL() {
            {
                SELECT("p.id");
                FROM("permissions p");
                INNER_JOIN("role_permissions rp ON rp.permission_id = p.id");
                INNER_JOIN("roles r ON r.id = rp.role_id");
                INNER_JOIN("user_roles ur ON ur.role_id = r.id AND ur.user_id = ?");
                WHERE("1 = 0");
                for (String code : permissionCodes) {
                    OR();
                    WHERE("p.code = '" + code + "'");
                }
            }
        }.toString();
        Collection resultset = executeQuery(query, new Object[]{userId});
        boolean hasResult = (hasAll ? (resultset.size() == permissionCodes.length) : resultset.size() > 0);
        return negate ? !hasResult : hasResult;
    }

    public boolean hasRoles(BigDecimal userId, String[] roleNames, boolean hasAll, boolean negate) {
        return hasRoles(userId.intValue(), roleNames, hasAll, negate);
    }

    public boolean hasRoles(Integer userId, String[] roleNames, boolean hasAll, boolean negate) {
        String query = new SQL() {
            {
                SELECT("r.id");
                FROM("roles r");
                INNER_JOIN("user_roles ur ON ur.role_id = r.id AND ur.user_id = ?");
                WHERE("1 = 0");
                for (String name : roleNames) {
                    OR();
                    WHERE("p.code = '" + name + "'");
                }
            }
        }.toString();
        Collection resultset = executeQuery(query, new Object[]{userId});
        boolean hasResult = (hasAll ? (resultset.size() == roleNames.length) : resultset.size() > 0);
        return negate ? !hasResult : hasResult;
    }

    @Override
    public boolean update(User model) {
        String query = new SQL() {
            {
                UPDATE("users");
                SET("username = ?");
                SET("password = ?");
                SET("enabled = ?");
                SET("deleted_by = ?");
                SET("deleted_at = ?");
                WHERE("id = ?");
            }
        }.toString();
        try {
            executeUpdate(query, new Object[]{
                model.getUsername(),
                model.getPassword(),
                model.isEnabled() ? "1" : "0",
                model.getDeletedBy(),
                model.getDeletedAt(),
                model.getId().intValueExact()
            });
            executeUpdate(new SQL() {
                {
                    DELETE_FROM("user_roles");
                    WHERE("user_id = ?");
                }
            }.toString(), new BigDecimal[]{model.getId()});
            for (Role r : model.getRoles()) {
                if (r.getId() != null) {
                    executeUpdate(new SQL() {
                        {
                            INSERT_INTO("user_roles");
                            VALUES("user_id, role_id",
                                    "?, ?");
                        }
                    }.toString(), new BigDecimal[]{model.getId(), r.getId()});
                }
            }
            return true;
        } catch (Exception ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
        return false;
    }
}
