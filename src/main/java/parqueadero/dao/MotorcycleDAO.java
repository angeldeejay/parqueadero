/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parqueadero.dao;

import java.math.BigDecimal;
import java.util.HashMap;
import org.apache.ibatis.jdbc.SQL;
import parqueadero.model.Motorcycle;
import parqueadero.model.User;
import static parqueadero.util.DataSource.executeQuery;
import static parqueadero.util.DataSource.executeUpdate;
import parqueadero.util.InterfaceDAO;
import static parqueadero.util.Logger.getLogger;

/**
 *
 * @author avanegas
 */
public class MotorcycleDAO implements InterfaceDAO<Motorcycle> {

    @Override
    public Motorcycle getById(Integer id) {
        String query = new SQL() {
            {
                SELECT("m.id, m.plate, m.model, m.city, m.serial, m.color");
                FROM("motorcycles m");
                WHERE("m.id = ?");
                WHERE("ROWNUM = 1");
            }
        }.toString();
        for (HashMap data : executeQuery(query, new Integer[]{id})) {
            return new Motorcycle(data);
        }
        return null;
    }

    /**
     *
     * @param model the value of model
     * @return the java.math.BigDecimal
     */
    @Override
    public BigDecimal add(Motorcycle model) {
        String query = new SQL() {
            {
                INSERT_INTO("motorcycles");
                VALUES("id, plate, model, city, serial, color", "?, ?, ?, ?, ?, ?");
            }
        }.toString();
        try {
            executeUpdate(query, new Object[]{
                model.getId().intValueExact(),
                model.getPlate(),
                model.getCity(),
                model.getModel(),
                model.getSerial(),
                model.getColor()
            });
            return model.getId();
        } catch (Exception ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
        return null;
    }

    @Override
    public boolean update(Motorcycle model) {
        String query = new SQL() {
            {
                UPDATE("motorcycles");
                SET("plate  = ?");
                SET("city = ?");
                SET("model = ?");
                SET("serial = ?");
                SET("color = ?");
                WHERE("id = ?");
            }
        }.toString();
        try {
            executeUpdate(query, new Object[]{
                model.getPlate(),
                model.getCity(),
                model.getModel(),
                model.getSerial(),
                model.getColor(),
                model.getId().intValueExact()
            });
            return true;
        } catch (Exception ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
        return false;
    }

    @Override
    public boolean delete(Motorcycle model, User __) {
        String query = new SQL() {
            {
                DELETE_FROM("motorcycles");
                WHERE("id = ?");
            }
        }.toString();
        try {
            executeUpdate(query, new Object[]{
                model.getId().intValueExact()
            });
            return true;
        } catch (Exception ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
        return false;
    }

}
