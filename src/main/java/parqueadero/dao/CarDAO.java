/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parqueadero.dao;

import java.math.BigDecimal;
import java.util.HashMap;
import org.apache.ibatis.jdbc.SQL;
import parqueadero.model.Car;
import parqueadero.model.User;
import static parqueadero.util.DataSource.executeQuery;
import static parqueadero.util.DataSource.executeUpdate;
import parqueadero.util.InterfaceDAO;
import static parqueadero.util.Logger.getLogger;

/**
 *
 * @author avanegas
 */
public class CarDAO implements InterfaceDAO<Car> {

    @Override
    public Car getById(Integer id) {
        String query = new SQL() {
            {
                SELECT("c.id, c.plate, c.model, c.city, c.serial, c.color");
                FROM("cars c");
                WHERE("c.id = ?");
                WHERE("ROWNUM = 1");
            }
        }.toString();
        for (HashMap data : executeQuery(query, new Integer[]{id})) {
            return new Car(data);
        }
        return null;
    }

    /**
     *
     * @param model the value of model
     * @return the java.math.BigDecimal
     */
    @Override
    public BigDecimal add(Car model) {
        String query = new SQL() {
            {
                INSERT_INTO("cars");
                VALUES("id, plate, model, city, serial, color", "?, ?, ?, ?, ?, ?");
            }
        }.toString();
        try {
            executeUpdate(query, new Object[]{
                model.getId().intValueExact(),
                model.getPlate(),
                model.getCity(),
                model.getModel(),
                model.getSerial(),
                model.getColor()
            });
            return model.getId();
        } catch (Exception ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
        return null;
    }

    @Override
    public boolean update(Car model) {
        String query = new SQL() {
            {
                UPDATE("cars");
                SET("plate  = ?");
                SET("city = ?");
                SET("model = ?");
                SET("serial = ?");
                SET("color = ?");
                WHERE("id = ?");
            }
        }.toString();
        try {
            executeUpdate(query, new Object[]{
                model.getPlate(),
                model.getCity(),
                model.getModel(),
                model.getSerial(),
                model.getColor(),
                model.getId().intValueExact()
            });
            return true;
        } catch (Exception ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
        return false;
    }

    @Override
    public boolean delete(Car model, User __) {
        String query = new SQL() {
            {
                DELETE_FROM("cars");
                WHERE("id = ?");
            }
        }.toString();
        try {
            executeUpdate(query, new Object[]{
                model.getId().intValueExact()
            });
            return true;
        } catch (Exception ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
        return false;
    }

}
