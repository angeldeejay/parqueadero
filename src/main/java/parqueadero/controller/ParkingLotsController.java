/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.controller;

import java.util.ArrayList;
import java.util.Collection;
import org.apache.ibatis.jdbc.SQL;
import parqueadero.dao.LotDAO;
import parqueadero.dao.ZoneDAO;
import parqueadero.model.Lot;
import parqueadero.model.User;
import parqueadero.model.Zone;
import static parqueadero.util.DataSource.executeQuery;

/**
 * ParkingLotsController class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class ParkingLotsController {

    private static final ZoneDAO zonesDAO = new ZoneDAO();
    private static final LotDAO lotDAO = new LotDAO();

    private static final String ZONES_QUERY = new SQL() {
        {
            SELECT("z.*");
            FROM("zones z");
            WHERE("z.enabled = '1'");
        }
    }.toString();

    public static Collection<Lot> getLots(String where) {
        return getLots(where, false);
    }

    public static Collection<Lot> getLots(String where, boolean allowDisabled) {
        String[] clauses = where != null && where.length() > 0 ? new String[]{
            "(UPPER(l.code) LIKE UPPER(?) OR"
            + "UPPER(z.code) LIKE UPPER(?))"
        } : new String[]{};
        Object[] params = where != null && where.length() > 0 ? new String[]{
            "%" + where + "%",
            "%" + where + "%"
        } : new Object[]{};
        SQL query = new SQL() {
            {
                SELECT("l.id, l.code, l.empty, l.zone_id, l.allowed_type, "
                        + "l.handicapped, l.enabled, l.deleted_by, l.deleted_at");
                FROM("lots l");
                INNER_JOIN("zones z ON "
                        + "z.id = l.zone_id AND "
                        + "z.enabled IN ('" + (allowDisabled ? "0', '1" : "1") + "')");
                WHERE("l.enabled IN ('" + (allowDisabled ? "0', '1" : "1") + "')");
                if (where != null) {
                    for (String w : clauses) {
                        WHERE(w);
                    }
                }
            }
        };

        Collection results = new ArrayList<Lot>();
        executeQuery(query.toString())
                .forEach((row) -> {
                    results.add(new Lot(row));
                });
        return results;
    }

    public static Collection<Zone> getZones() {
        Collection results = new ArrayList<Zone>();
        executeQuery(ZONES_QUERY)
                .forEach((row) -> {
                    results.add(new Zone(row));
                });
        return results;
    }

    public static Boolean createZone(final Zone newZone) {
        zonesDAO.add(newZone);
        return false;
    }

    public static Boolean updateZone(final Zone zone) {
        return zonesDAO.update(zone);
    }

    public static Boolean deleteZone(final Zone zone, final User performer) {
        return zonesDAO.delete(zone, performer);
    }

    public static Boolean createLot(final Lot lot) {
        return lotDAO.add(lot) != null;
    }

    public static Boolean updateLot(final Lot lot) {
        return lotDAO.update(lot);
    }

    public static Boolean deleteLot(final Lot lot, final User performer) {
        return lotDAO.delete(lot, performer);
    }
}
