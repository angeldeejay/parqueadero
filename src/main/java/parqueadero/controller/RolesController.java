/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import org.apache.ibatis.jdbc.SQL;
import parqueadero.dao.PermissionDAO;
import parqueadero.dao.RoleDAO;
import parqueadero.dao.RoleDAO;
import parqueadero.model.Permission;
import parqueadero.model.Role;
import parqueadero.model.Role;
import parqueadero.model.User;
import static parqueadero.util.DataSource.executeQuery;

/**
 * AuthController class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class RolesController {

    private static final RoleDAO roleDAO = new RoleDAO();
    private static final PermissionDAO permissionDAO = new PermissionDAO();

    public static Role getByName(String name) {
        return roleDAO.getByName(name);
    }

    public static Collection<Role> getRoles(String where) {
        return getRoles(where, false);
    }

    public static Collection<Role> getRoles(String where, boolean allowDisabled) {
        String[] clauses = where != null && where.length() > 0 ? new String[]{
            "(UPPER(e.name) LIKE UPPER(?)",
            "e.enabled IN ('" + (allowDisabled ? "0', '1" : "1") + "')"
        } : new String[]{};
        Object[] params = where != null && where.length() > 0 ? new String[]{
            "%" + where + "%"
        } : new Object[]{};
        SQL query = new SQL() {
            {
                SELECT("e.*");
                FROM("roles e");
                if (where != null) {
                    for (String w : clauses) {
                        WHERE(w);
                    }
                }
            }
        };
        Collection results = new ArrayList<Role>();
        executeQuery(query.toString(), params)
                .forEach((row) -> {
                    Role role = new Role(row);
                    role.setPermissions(getPermissionsByRole(role, allowDisabled));
                    results.add(role);
                });
        return results;
    }

    public static boolean updateRole(Role formData) {
        return roleDAO.update(formData);
    }

    public static boolean deleteRole(Role formData, User perfomer) {
        return roleDAO.delete(formData, perfomer);
    }

    public static boolean createRole(Role formData) {
        return roleDAO.add(formData) != null;
    }

    public static Collection<Permission> getPermissionsByRole(Role formData, boolean allowDisabled) {
        Collection results = new ArrayList<Permission>();
        if (formData.getId() != null) {
            return permissionDAO.getByRoleId(formData.getId().intValue(), allowDisabled);
        }
        return results;
    }

    public static Collection<Permission> getAllPermissions(boolean allowDisabled) {
        Collection results = new ArrayList<Permission>();
        executeQuery(new SQL() {
            {
                SELECT("p.*");
                FROM("permissions p");
            }
        }.toString()).forEach((row) -> {
            results.add(new Permission(row));
        });
        return results;
    }
}
