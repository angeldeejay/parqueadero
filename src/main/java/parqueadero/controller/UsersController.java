/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.controller;

import java.util.ArrayList;
import java.util.Collection;
import org.apache.ibatis.jdbc.SQL;
import parqueadero.dao.EntityDAO;
import parqueadero.dao.UserDAO;
import parqueadero.model.Entity;
import parqueadero.model.User;
import static parqueadero.util.DataSource.executeQuery;

/**
 * AuthController class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class UsersController {

    private static final EntityDAO entityDAO = new EntityDAO();
    private static final UserDAO userDAO = new UserDAO();

    public static Entity getByIdent(String identType, String identValue) {
        return entityDAO.getByIdent(identType, identValue);
    }

    public static Collection<Entity> getUsers(String where) {
        return getUsers(where, false);
    }

    public static Collection<Entity> getUsers(String where, boolean allowDisabled) {
        String[] clauses = where != null && where.length() > 0 ? new String[]{
            "(UPPER(e.first_name) LIKE UPPER(?) OR"
            + "UPPER(e.last_name) LIKE UPPER(?) OR"
            + "UPPER(e.ident_value) LIKE UPPER(?) OR"
            + "UPPER(u.username) LIKE UPPER(?))"
        } : new String[]{};
        Object[] params = where != null && where.length() > 0 ? new String[]{
            "%" + where + "%",
            "%" + where + "%",
            "%" + where + "%",
            "%" + where + "%"
        } : new Object[]{};
        SQL query = new SQL() {
            {
                SELECT("*");
                FROM("entities e");
                INNER_JOIN("users u ON "
                        + "u.id = e.id AND "
                        + "u.enabled IN ('1', '" + (allowDisabled ? "0" : "1") + "')");
                if (where != null) {
                    for (String w : clauses) {
                        WHERE(w);
                    }
                }
            }
        };
        ArrayList<Entity> results = new ArrayList<>();
        executeQuery(query.toString(), params)
                .forEach((row) -> {
                    results.add(new Entity(row));
                });
        return results;
    }

    public static boolean editUser(Entity formData) {
        return entityDAO.update(formData);
    }

    public static boolean deleteUser(Entity formData, User perfomer) {
        return userDAO.delete(formData.getUser(), perfomer);
    }

    public static boolean createUser(Entity formData) {
        return entityDAO.add(formData) != null;
    }
}
