/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parqueadero.controller;

import java.util.ArrayList;
import java.util.Collection;
import org.apache.ibatis.jdbc.SQL;
import parqueadero.dao.VehicleDAO;
import parqueadero.model.Vehicle;
import static parqueadero.util.DataSource.executeQuery;

/**
 *
 * @author avanegas
 */
public class VehiclesController {

    private static final VehicleDAO vehicleDAO = new VehicleDAO();

    public static Vehicle getByIdent(String vehicleType, Object[] params) {
        return vehicleDAO.getByIdent(vehicleType, params);
    }

    public static Collection<Vehicle> getVehicles(String where) {
        String[] clauses = where != null && where.length() > 0 ? new String[]{
            "(UPPER(b.color) LIKE UPPER(?) OR"
            + "UPPER(m.color) LIKE UPPER(?) OR "
            + "UPPER(m.plate) LIKE UPPER(?) OR "
            + "UPPER(m.city) LIKE UPPER(?) OR "
            + "UPPER(m.model) LIKE UPPER(?) OR"
            + "UPPER(m.serial) LIKE UPPER(?) OR"
            + "UPPER(c.color) LIKE UPPER(?) OR "
            + "UPPER(c.plate) LIKE UPPER(?) OR "
            + "UPPER(c.city) LIKE UPPER(?) OR "
            + "UPPER(c.model) LIKE UPPER(?) OR"
            + "UPPER(c.serial) LIKE UPPER(?))"
        } : new String[]{};
        Object[] params = where != null && where.length() > 0 ? new String[]{
            "%" + where + "%",
            "%" + where + "%",
            "%" + where + "%",
            "%" + where + "%",
            "%" + where + "%",
            "%" + where + "%",
            "%" + where + "%",
            "%" + where + "%",
            "%" + where + "%",
            "%" + where + "%",
            "%" + where + "%"
        } : new Object[]{};
        SQL query = new SQL() {
            {
                SELECT("v.*");
                SELECT("b.color, b.type, b.observations");
                SELECT("c.plate, c.model, c.city, c.serial, c.color");
                SELECT("m.plate, m.model, m.city, m.serial, m.color");
                FROM("vehicles v");
                LEFT_OUTER_JOIN("bikes b ON v.vehicle_type = 'B' "
                        + "AND b.id = v.id");
                LEFT_OUTER_JOIN("motorcycles m ON v.vehicle_type = 'M' "
                        + "AND m.id = v.id");
                LEFT_OUTER_JOIN("cars c ON v.vehicle_type = 'C' "
                        + "AND c.id = v.id");
                WHERE("COALESCE(b.id, m.id, c.id) IS NOT NULL");
                if (where != null) {
                    for (String w : clauses) {
                        WHERE(w);
                    }
                }
                ORDER_BY("v.id, c.plate, m.plate");
            }
        };
        Collection results = new ArrayList<Vehicle>();
        executeQuery(query.toString(), params)
                .forEach((row) -> {
                    results.add(new Vehicle(row));
                });
        return results;
    }

    public static void resetVehicleTypeData(Vehicle formData) {
    }

    public static boolean updateVehicle(Vehicle formData) {
        return vehicleDAO.update(formData);
    }

    public static boolean deleteVehicle(Vehicle formData) {
        return vehicleDAO.delete(formData, null);
    }

    public static boolean createVehicle(Vehicle formData) {
        return vehicleDAO.add(formData) != null;
    }
}
