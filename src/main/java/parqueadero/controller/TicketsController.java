/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import org.apache.ibatis.jdbc.SQL;
import parqueadero.dao.TicketDAO;
import parqueadero.model.Ticket;
import parqueadero.model.Vehicle;
import static parqueadero.util.DataSource.executeQuery;

/**
 * TicketsController class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class TicketsController {

    private final static TicketDAO TICKETS_DAO = new TicketDAO();

    private final static String TICKETS_QUERY = new SQL() {
        {
            SELECT("t.id, t.checkin, t.checkout, t.end_price, t.observations, "
                    + "t.owner_id, t.vehicle_id, t.lot_id, t.user_id");
            SELECT("u.username, u.password, u.enabled, u.deleted_by, u.deleted_at");
            SELECT("l.code, l.empty, l.allowed_type, l.handicapped, l.deleted_by, "
                    + "l.deleted_at, l.enabled, l.zone_id");
            SELECT("v.vehicle_type");
            FROM("tickets t");
            INNER_JOIN("users u ON u.id = t.user_id");
            INNER_JOIN("lots l ON l.id = t.lot_id");
            INNER_JOIN("owners o ON o.id = t.owner_id");
            INNER_JOIN("vehicles v ON v.id = t.vehicle_id");
            ORDER_BY("t.checkin DESC");
            ORDER_BY("t.checkout DESC NULLS FIRST");
        }
    }.toString();

    private final static String ACTIVE_TICKETS_QUERY = new SQL() {
        {
            SELECT("t.*");
            FROM("tickets t");
            WHERE("t.vehicle_id = ?");
            WHERE("t.checkout IS NULL");
            WHERE("ROWNUM = 1");
        }
    }.toString();

    public static Ticket getActiveTicket(Vehicle v) {
        Ticket t = null;
        for (HashMap row : executeQuery(ACTIVE_TICKETS_QUERY, new Object[]{v.getId()})) {
            t = new Ticket(row);
            break;
        }
        return t;
    }

    public static Collection<Ticket> getTickets(String where) {
        Collection results = new ArrayList<Ticket>();
        executeQuery(TICKETS_QUERY)
                .forEach((row) -> {
                    results.add(new Ticket(row));
                });
        return results;
    }

    public static boolean createTicket(final Ticket ticket) {
        return TICKETS_DAO.add(ticket) != null;
    }

    public static boolean updateTicket(final Ticket ticket) {
        return TICKETS_DAO.update(ticket);
    }
}
