package parqueadero.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import org.apache.ibatis.jdbc.SQL;
import parqueadero.dao.PriceDAO;
import parqueadero.model.Price;
import parqueadero.model.Ticket;
import static parqueadero.util.DataSource.executeQuery;

/**
 * PricesController class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class PricesController {

    private final static PriceDAO PRICES_DAO = new PriceDAO();

    private final static String PRICES_QUERY = new SQL() {
        {
            SELECT("*");
            FROM("prices p");
        }
    }.toString();

    public static Integer getEndPrice(Ticket t) {
        String query = new SQL() {
            {
                SELECT("p.*");
                FROM("prices p");
                WHERE("p.vehicle_type = ? OR p.vehicle_type IS NULL");
                WHERE("p.owner_id = ? OR p.owner_id IS NULL");
                WHERE("p.vehicle_id = ? OR p.vehicle_id IS NULL");
                ORDER_BY("p.vehicle_type NULLS LAST");
                ORDER_BY("p.owner_id NULLS LAST");
                ORDER_BY("p.vehicle_id NULLS LAST");
            }
        }.toString();
        Price p = new Price();
        p.setValue(new BigDecimal(0));
        for (HashMap row : executeQuery(query, new Object[]{
            t.getVehicle().getVehicleType(),
            t.getOwnerId(),
            t.getVehicleId()
        })) {
            p = new Price(row);
            break;
        }
        return p.getValue().intValueExact();
    }

    public static Price getByIdent(String vehicleType, BigDecimal ownerId, BigDecimal vehicleId) {
        return PRICES_DAO.getByIdent(vehicleType, ownerId, vehicleId);
    }

    public static Collection<Price> getPrices(String where) {
        Collection results = new ArrayList<Price>();
        executeQuery(PRICES_QUERY)
                .forEach((row) -> {
                    results.add(new Price(row));
                });
        return results;
    }

    public static boolean createPrice(final Price price) {
        return PRICES_DAO.add(price) != null;
    }

    public static boolean deletePrice(final Price price) {
        return PRICES_DAO.delete(price, null);
    }

    public static boolean updatePrice(final Price price) {
        return PRICES_DAO.update(price);
    }
}
