/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.controller;

import java.util.ArrayList;
import java.util.Collection;
import org.apache.ibatis.jdbc.SQL;
import parqueadero.dao.EntityDAO;
import parqueadero.dao.OwnerDAO;
import parqueadero.model.Entity;
import parqueadero.model.User;
import static parqueadero.util.DataSource.executeQuery;

/**
 * OwnersController class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class OwnersController {

    private static final EntityDAO entityDAO = new EntityDAO();
    private static final OwnerDAO ownerDAO = new OwnerDAO();

    public static Entity getByIdent(String identType, String identValue) {
        return entityDAO.getByIdent(identType, identValue);
    }

    public static Collection<Entity> getOwners(String where) {
        return getOwners(where, false);
    }

    public static Collection<Entity> getOwners(String where, boolean allowDisabled) {
        String[] clauses = where != null && where.length() > 0 ? new String[]{
            "(UPPER(e.first_name) LIKE UPPER(?) OR"
            + "UPPER(e.last_name) LIKE UPPER(?) OR"
            + "UPPER(e.ident_value) LIKE UPPER(?)"
        } : new String[]{};
        Object[] params = where != null && where.length() > 0 ? new String[]{
            "%" + where + "%",
            "%" + where + "%",
            "%" + where + "%"
        } : new Object[]{};
        SQL query = new SQL() {
            {
                SELECT("e.*");
                SELECT("o.handicapped, o.enabled, o.deleted_by, o.deleted_at");
                FROM("entities e");
                INNER_JOIN("owners o ON "
                        + "o.id = e.id AND "
                        + "o.enabled IN ('" + (allowDisabled ? "0', '1" : "1") + "')");
                if (where != null) {
                    for (String w : clauses) {
                        WHERE(w);
                    }
                }
            }
        };
        Collection results = new ArrayList<Entity>();
        executeQuery(query.toString(), params)
                .forEach((row) -> {
                    results.add(new Entity(row));
                });
        return results;
    }

    public static boolean editOwner(Entity formData) {
        return entityDAO.update(formData);
    }

    public static boolean deleteOwner(Entity formData, User perfomer) {
        return ownerDAO.delete(formData.getOwner(), perfomer);
    }

    public static boolean createOwner(Entity formData) {
        return entityDAO.add(formData) != null;
    }
}
