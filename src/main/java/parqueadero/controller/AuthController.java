/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.controller;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import org.apache.ibatis.jdbc.SQL;
import parqueadero.model.Entity;
import parqueadero.model.User;
import static parqueadero.util.DataSource.executeQuery;
import static parqueadero.util.Logger.getLogger;
import static parqueadero.util.Security.hashPassword;
import parqueadero.util.Session;

/**
 * AuthController class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class AuthController {

    private static final String LOGIN_QUERY = new SQL() {
        {
            SELECT("e.*, u.*");
            FROM("entities e");
            INNER_JOIN("users u ON e.id = u.id AND u.username = ? AND u.password = ? AND u.enabled = '1'");
            WHERE("ROWNUM = 1");
        }
    }.toString();

    public static Session logIn(String username, String password) {
        String hashedPassword = hashPassword(password);
        try {
            HashMap entityData = executeQuery(
                    LOGIN_QUERY, new String[]{username, hashedPassword}).get(0);
            Entity e = new Entity(entityData);
            e.setUser(new User(entityData));
            Date now = new Date();
            return new Session(e, new Timestamp(now.getTime()));
        } catch (Exception ex) {
            getLogger(AuthController.class).error(ex.getMessage(), ex);
        }
        return null;
    }
}
