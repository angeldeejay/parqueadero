/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.view.form;

import static java.lang.String.valueOf;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import javax.swing.JButton;
import static jiconfont.icons.font_awesome.FontAwesome.CHECK;
import static jiconfont.icons.font_awesome.FontAwesome.TIMES;
import static parqueadero.controller.RolesController.getRoles;
import static parqueadero.controller.UsersController.createUser;
import static parqueadero.controller.UsersController.editUser;
import static parqueadero.controller.UsersController.getByIdent;
import parqueadero.model.Entity;
import parqueadero.model.Role;
import parqueadero.model.User;
import static parqueadero.util.Icon.getIcon;
import static parqueadero.util.Security.hashPassword;
import parqueadero.view.MainView;
import parqueadero.view.pane.UsersTabPane;

/**
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class UserForm extends parqueadero.view.component.Dialog {

    private Entity formData;
    private boolean isNew = false;

    /**
     * Creates new empty form UserForm
     *
     * @param parent
     */
    public UserForm(java.awt.Frame parent) {
        this(parent, null);
    }

    /**
     * Creates new filled form UserForm
     *
     * @param parent
     * @param e
     */
    public UserForm(java.awt.Frame parent, Entity e) {
        super(parent, true);
        this.formData = e == null ? new Entity() : e;
        this.isNew = e == null;
        initComponents();
        fillComponents();
    }

    private void fillComponents() {
        fillComponents(false);
    }

    private void fillComponents(boolean skipIdent) {
        this.cancelButton.setIcon(getIcon(TIMES, 14));
        this.saveButton.setIcon(getIcon(CHECK, 14));
        User u = formData.getUser();
        this.username.setText(u != null ? u.getUsername() : "");
        this.firstName.setText(formData.getFirstName());
        this.lastName.setText(formData.getLastName());
        if (!skipIdent) {
            this.identType.setSelectedType(formData.getIdentType());
            this.identValue.setText(formData.getIdentValue());
        }
        this.enabledCheckBox.setSelected((u != null && u.isEnabled()) || u == null);

        Collection<Role> allRoles = getRoles(null);
        if (isNew) {
            this.rolesDualPanel.setValues(allRoles);
        } else {
            this.rolesDualPanel.setValues(allRoles, u.getRoles());
        }
    }

    private void saveFormData() {
        this.formData = getByIdent(identType.getSelectedType(), identValue.getText());
        this.isNew = (this.formData == null);
        if (this.formData == null) {
            this.formData = new Entity();
        }
        formData.setFirstName(this.firstName.getText());
        formData.setLastName(this.lastName.getText());
        formData.setIdentType(this.identType.getSelectedType());
        formData.setIdentValue(this.identValue.getText());

        User u = formData.getUser();
        if (u == null) {
            u = new User();
        }
        u.setUsername(this.username.getText());
        u.setEnabled(this.enabledCheckBox.isSelected());
        u.setDeletedAt(this.enabledCheckBox.isSelected() ? null : new Timestamp(new Date().getTime()));
        u.setDeletedBy(this.enabledCheckBox.isSelected() ? null : ((MainView) this.getParent()).getLoggedInEntity().getUser().getId());
        u.setRoles(this.rolesDualPanel.getSelectedValuesList());
        String passwordValue = valueOf(this.password.getPassword()).trim();
        if (passwordValue.length() > 0) {
            u.setPassword(hashPassword(passwordValue));
        }
        formData.setUser(u);
        boolean result = false;
        if (isNew) {
            result = createUser(formData);
        } else {
            result = editUser(formData);
        }
        if (result) {
            try {
                UsersTabPane openedPane = ((MainView) this.getOwner()).getOpenedPane(UsersTabPane.class);
                JButton refreshButton = openedPane.getComponentByName(JButton.class, "refreshButton");
                refreshButton.doClick();
            } catch (Exception _ex) {
                _ex.printStackTrace();
            }
            this.dispose();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        javax.swing.JPanel entityPanel = new javax.swing.JPanel();
        javax.swing.JLabel firstNameLabel = new javax.swing.JLabel();
        javax.swing.JLabel lastNameLabel = new javax.swing.JLabel();
        javax.swing.JLabel identTypeLabel = new javax.swing.JLabel();
        firstName = new javax.swing.JTextField();
        lastName = new javax.swing.JTextField();
        identType = new parqueadero.view.component.IdentTypeComboBox();
        identValue = new parqueadero.view.component.NumberTextField();
        javax.swing.JLabel identValueLabel = new javax.swing.JLabel();
        javax.swing.JPanel userPanel = new javax.swing.JPanel();
        javax.swing.JLabel usernameLabel = new javax.swing.JLabel();
        javax.swing.JLabel passwordLabel = new javax.swing.JLabel();
        username = new javax.swing.JTextField();
        password = new javax.swing.JPasswordField();
        javax.swing.JPanel actionsPanel = new javax.swing.JPanel();
        enabledCheckBox = new javax.swing.JCheckBox();
        saveButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        javax.swing.JPanel rolesPanel = new javax.swing.JPanel();
        rolesDualPanel = new parqueadero.view.component.DualListPanel<Role>((el) -> el.getName() != null ? el.getName() : "");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setModal(true);
        setResizable(false);
        getContentPane().setLayout(new java.awt.GridBagLayout());

        entityPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos generales"));
        entityPanel.setMinimumSize(new java.awt.Dimension(100, 50));
        entityPanel.setPreferredSize(new java.awt.Dimension(325, 178));
        entityPanel.setLayout(new java.awt.GridBagLayout());

        firstNameLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        firstNameLabel.setText("Nombres");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        entityPanel.add(firstNameLabel, gridBagConstraints);

        lastNameLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lastNameLabel.setText("Apellidos");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        entityPanel.add(lastNameLabel, gridBagConstraints);

        identTypeLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        identTypeLabel.setText("Tipo de documento");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        entityPanel.add(identTypeLabel, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        entityPanel.add(firstName, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        entityPanel.add(lastName, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        entityPanel.add(identType, gridBagConstraints);

        identValue.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                identValueKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(6, 6, 6, 6);
        entityPanel.add(identValue, gridBagConstraints);

        identValueLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        identValueLabel.setText("Número");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        entityPanel.add(identValueLabel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.6;
        getContentPane().add(entityPanel, gridBagConstraints);

        userPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos de usuario"));
        userPanel.setMinimumSize(new java.awt.Dimension(400, 100));
        userPanel.setPreferredSize(new java.awt.Dimension(400, 100));
        userPanel.setLayout(new java.awt.GridBagLayout());

        usernameLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        usernameLabel.setText("Usuario");
        usernameLabel.setPreferredSize(new java.awt.Dimension(95, 15));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        userPanel.add(usernameLabel, gridBagConstraints);

        passwordLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        passwordLabel.setText("Contraseña");
        passwordLabel.setPreferredSize(new java.awt.Dimension(95, 15));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        userPanel.add(passwordLabel, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.9;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        userPanel.add(username, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.9;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        userPanel.add(password, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        getContentPane().add(userPanel, gridBagConstraints);

        actionsPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        enabledCheckBox.setText("Habilitado");
        actionsPanel.add(enabledCheckBox);

        saveButton.setText("Guardar");
        saveButton.setIconTextGap(5);
        saveButton.setMaximumSize(new java.awt.Dimension(100, 23));
        saveButton.setMinimumSize(new java.awt.Dimension(100, 23));
        saveButton.setPreferredSize(new java.awt.Dimension(100, 23));
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        actionsPanel.add(saveButton);

        cancelButton.setText("Cancelar");
        cancelButton.setIconTextGap(5);
        cancelButton.setMaximumSize(new java.awt.Dimension(100, 23));
        cancelButton.setMinimumSize(new java.awt.Dimension(100, 23));
        cancelButton.setPreferredSize(new java.awt.Dimension(100, 23));
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        actionsPanel.add(cancelButton);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        getContentPane().add(actionsPanel, gridBagConstraints);

        rolesPanel.setLayout(new java.awt.BorderLayout());

        rolesDualPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(""), "Perfiles", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        rolesPanel.add(rolesDualPanel, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.4;
        getContentPane().add(rolesPanel, gridBagConstraints);

        setSize(new java.awt.Dimension(374, 516));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        saveFormData();
    }//GEN-LAST:event_saveButtonActionPerformed

    private void identValueKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_identValueKeyReleased
        this.formData = getByIdent(identType.getSelectedType(), identValue.getText());
        this.isNew = (this.formData == null);
        if (this.formData == null) {
            this.formData = new Entity();
        }
        fillComponents(true);
    }//GEN-LAST:event_identValueKeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelButton;
    private javax.swing.JCheckBox enabledCheckBox;
    private javax.swing.JTextField firstName;
    private parqueadero.view.component.IdentTypeComboBox identType;
    private parqueadero.view.component.NumberTextField identValue;
    private javax.swing.JTextField lastName;
    private javax.swing.JPasswordField password;
    private parqueadero.view.component.DualListPanel<Role> rolesDualPanel;
    private javax.swing.JButton saveButton;
    private javax.swing.JTextField username;
    // End of variables declaration//GEN-END:variables

    @Override
    protected void cancelAction() {
        this.cancelButton.doClick();
    }

    @Override
    protected void defaultAction() {
        this.saveButton.doClick();
    }
}
