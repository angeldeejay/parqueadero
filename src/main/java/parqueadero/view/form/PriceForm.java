/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.view.form;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import static java.awt.event.KeyEvent.VK_ENTER;
import static java.awt.event.KeyEvent.VK_ESCAPE;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Enumeration;
import javax.swing.JButton;
import static javax.swing.JComponent.WHEN_IN_FOCUSED_WINDOW;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;
import static javax.swing.KeyStroke.getKeyStroke;
import static jiconfont.icons.font_awesome.FontAwesome.CHECK;
import static jiconfont.icons.font_awesome.FontAwesome.TIMES;
import static parqueadero.controller.OwnersController.getOwners;
import parqueadero.controller.PricesController;
import static parqueadero.controller.PricesController.createPrice;
import static parqueadero.controller.PricesController.updatePrice;
import static parqueadero.controller.VehiclesController.getVehicles;
import parqueadero.model.Entity;
import parqueadero.model.Price;
import parqueadero.model.Vehicle;
import static parqueadero.util.Icon.getIcon;
import parqueadero.view.MainView;
import parqueadero.view.component.ComboBoxRenderer;
import parqueadero.view.pane.PricesTabPane;

/**
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class PriceForm extends parqueadero.view.component.Dialog {

    private Price formData;
    private boolean isNew = false;

    /**
     * Creates new form PriceForm
     *
     * @param parent
     */
    public PriceForm(java.awt.Frame parent) {
        this(parent, new Price());
        this.isNew = true;
    }

    /**
     * Creates new filled form PriceForm
     *
     * @param parent
     * @param e
     */
    public PriceForm(java.awt.Frame parent, Price e) {
        super(parent, true);
        this.isNew = e == null || e.getId() == null;
        this.formData = isNew ? new Price() : e;
        initComponents();
        setUIDefaults();
        fillComponents();
    }

    private void setUIDefaults() {
        this.cancelButton.setIcon(getIcon(TIMES, 14));
        this.saveButton.setIcon(getIcon(CHECK, 14));
        this.owner.setRenderer(new ComboBoxRenderer<>((Entity e)
                -> "[" + e.getIdentType() + ". "
                + e.getIdentValue() + "] "
                + e.getFirstName() + " " + e.getLastName()));
        this.owner.addItems(getOwners(null));
        this.owner.setSelectedIndex(-1);

        this.vehicle.setRenderer(new ComboBoxRenderer<>((Vehicle v) -> {
            String type = "-", vehicleLabel = "";
            for (String k : Collections.list((Enumeration<String>) Vehicle.AVAILABLE_TYPES.keys())) {
                String vt = Vehicle.AVAILABLE_TYPES.get(k);
                if (vt.equalsIgnoreCase(v.getVehicleType())) {
                    type = k;
                    break;
                }
            }
            switch (v.getVehicleType()) {
                case Vehicle.BIKE:
                    if (v.getBike() == null) {
                        break;
                    }
                    String[] models = new String[]{"REGULAR", "BMX", "MONTAÑA", "RUTA", "TOUR", "OTRO TIPO"};
                    vehicleLabel = models[Integer.parseInt(v.getBike().getType()) + 1] + "-"
                            + v.getBike().getColor();
                    break;
                case Vehicle.CAR:
                    if (v.getCar() == null) {
                        break;
                    }
                    vehicleLabel = v.getCar().getPlate() + " - "
                            + v.getCar().getModel() + " - "
                            + v.getCar().getSerial() + " - "
                            + v.getCar().getColor();
                    break;
                case Vehicle.MOTORCYCLE:
                    if (v.getMotorcycle() == null) {
                        break;
                    }
                    vehicleLabel = v.getMotorcycle().getPlate() + " - "
                            + v.getMotorcycle().getModel() + " - "
                            + v.getMotorcycle().getSerial() + " - "
                            + v.getMotorcycle().getColor();
                    break;
            }
            return "[" + type + "] " + vehicleLabel;
        }));
        this.vehicle.addItems(getVehicles(null));
        this.vehicle.setSelectedIndex(-1);
    }

    private void fillComponents() {
        price.setText(formData.getValue() == null ? "0" : formData.getValue().toString());
        vehicleType.setSelectedType(formData.getVehicleType());
        owner.setSelectedItem(formData.getOwner());
        vehicle.setSelectedItem(formData.getVehicle());

    }

    private void saveFormData() {
        this.formData = PricesController.getByIdent(formData.getVehicleType(), formData.getOwnerId(), formData.getVehicleId());
        this.isNew = (this.formData == null);
        if (this.formData == null) {
            this.formData = new Price();
        }
        formData.setValue(new BigDecimal(price.getText().trim()));
        formData.setVehicleType(vehicleType.getSelectedType());
        formData.setOwnerId(owner.getSelectedId());
        formData.setVehicleId(vehicle.getSelectedId());

        boolean result = false;
        if (isNew) {
            result = createPrice(formData);
        } else {
            result = updatePrice(formData);
        }
        if (result) {
            try {
                PricesTabPane openedPane = ((MainView) this.getOwner()).getOpenedPane(PricesTabPane.class);
                JButton refreshButton = openedPane.getComponentByName(JButton.class, "refreshButton");
                refreshButton.doClick();
            } catch (Exception _ex) {
            }
            this.dispose();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        javax.swing.JLabel priceLabel = new javax.swing.JLabel();
        javax.swing.JLabel vehicleTypeLabel = new javax.swing.JLabel();
        javax.swing.JLabel ownerLabel = new javax.swing.JLabel();
        price = new parqueadero.view.component.NumberTextField();
        vehicleType = new parqueadero.view.component.VehicleTypeComboBox();
        owner = new parqueadero.view.component.ComboBox<>();
        javax.swing.JLabel vehicleLabel = new javax.swing.JLabel();
        vehicle = new parqueadero.view.component.ComboBox<>();
        saveButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });
        getContentPane().setLayout(new java.awt.BorderLayout(5, 5));

        formPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos de tarifa"));
        formPanel.setLayout(new java.awt.GridBagLayout());

        priceLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        priceLabel.setText("Tarifa");
        priceLabel.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        priceLabel.setIcon(getIcon(jiconfont.icons.font_awesome.FontAwesome.USD));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(priceLabel, gridBagConstraints);

        vehicleTypeLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        vehicleTypeLabel.setText("Tipo de vehículo");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(vehicleTypeLabel, gridBagConstraints);

        ownerLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        ownerLabel.setText("Conductor");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(ownerLabel, gridBagConstraints);

        price.setFont(price.getFont().deriveFont(price.getFont().getStyle() | java.awt.Font.BOLD, price.getFont().getSize()+5));
        price.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                priceKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(6, 6, 6, 6);
        formPanel.add(price, gridBagConstraints);

        vehicleType.setMinimumSize(new java.awt.Dimension(81, 27));
        vehicleType.setPreferredSize(new java.awt.Dimension(81, 27));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(vehicleType, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(owner, gridBagConstraints);

        vehicleLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        vehicleLabel.setText("Vehículo");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(vehicleLabel, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(vehicle, gridBagConstraints);

        getContentPane().add(formPanel, java.awt.BorderLayout.CENTER);

        actionsPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        saveButton.setText("Guardar");
        saveButton.setIconTextGap(5);
        saveButton.setMaximumSize(new java.awt.Dimension(100, 23));
        saveButton.setMinimumSize(new java.awt.Dimension(100, 23));
        saveButton.setPreferredSize(new java.awt.Dimension(100, 23));
        saveButton.setIcon(getIcon(jiconfont.icons.font_awesome.FontAwesome.CHECK));
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        actionsPanel.add(saveButton);

        cancelButton.setText("Cancelar");
        cancelButton.setIconTextGap(5);
        cancelButton.setMaximumSize(new java.awt.Dimension(100, 23));
        cancelButton.setMinimumSize(new java.awt.Dimension(100, 23));
        cancelButton.setPreferredSize(new java.awt.Dimension(100, 23));
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        actionsPanel.add(cancelButton);

        getContentPane().add(actionsPanel, java.awt.BorderLayout.PAGE_END);

        setSize(new java.awt.Dimension(568, 256));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Closes the dialog
     */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        setVisible(false);
        dispose();
    }//GEN-LAST:event_closeDialog

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        saveFormData();
    }//GEN-LAST:event_saveButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void priceKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_priceKeyReleased

    }//GEN-LAST:event_priceKeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private final javax.swing.JPanel actionsPanel = new javax.swing.JPanel();
    private javax.swing.JButton cancelButton;
    private final javax.swing.JPanel formPanel = new javax.swing.JPanel();
    private parqueadero.view.component.ComboBox<Entity> owner;
    private parqueadero.view.component.NumberTextField price;
    private javax.swing.JButton saveButton;
    private parqueadero.view.component.ComboBox<Vehicle> vehicle;
    private parqueadero.view.component.VehicleTypeComboBox vehicleType;
    // End of variables declaration//GEN-END:variables

    @Override
    protected void cancelAction() {
        this.cancelButton.doClick();
    }

    @Override
    protected void defaultAction() {
        this.saveButton.doClick();
    }

}
