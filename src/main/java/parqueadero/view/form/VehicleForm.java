/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.view.form;

import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.ParseException;
import javax.swing.JButton;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;
import static jiconfont.icons.font_awesome.FontAwesome.CHECK;
import static jiconfont.icons.font_awesome.FontAwesome.TIMES;
import static parqueadero.controller.VehiclesController.createVehicle;
import static parqueadero.controller.VehiclesController.updateVehicle;
import parqueadero.model.Bike;
import parqueadero.model.Car;
import parqueadero.model.Motorcycle;
import parqueadero.model.Vehicle;
import static parqueadero.util.Icon.getIcon;
import static parqueadero.util.Logger.getLogger;
import parqueadero.view.MainView;
import parqueadero.view.pane.VehiclesTabPane;

/**
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class VehicleForm extends  parqueadero.view.component.Dialog {

    private final Vehicle formData;
    private boolean isNew;

    /**
     * Creates new empty form UserForm
     *
     * @param parent
     */
    public VehicleForm(java.awt.Frame parent) {
        this(parent, null);
    }

    /**
     * Creates new filled form UserForm
     *
     * @param parent
     * @param e
     */
    public VehicleForm(java.awt.Frame parent, final Vehicle e) {
        super(parent, true);
        this.isNew = e == null;
        this.formData = this.isNew ? new Vehicle() : e;
        initComponents();
        setUIDefaults();
        fillComponents();
    }

    private void setUIDefaults() {
        this.cancelButton.setIcon(getIcon(TIMES, 14));
        this.saveButton.setIcon(getIcon(CHECK, 14));
        this.vehicleType.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    formData.setVehicleType(vehicleType.getSelectedType());
                    refreshFormBySelectedtype();
                    refreshPlatePreview();
                }
            }
        });
        if (isNew) {
            this.vehicleType.setSelectedType(Vehicle.CAR);
        } else {
            this.vehicleType.setSelectedType(formData.getVehicleType());
        }
    }

    private void refreshFormBySelectedtype() {
        String selectedType = formData.getVehicleType();
        this.platePreview.setVisible(!selectedType.equals(Vehicle.BIKE));
        this.plate.setVisible(!selectedType.equals(Vehicle.BIKE));
        this.plateLabel.setVisible(!selectedType.equals(Vehicle.BIKE));
        this.city.setVisible(!selectedType.equals(Vehicle.BIKE));
        this.cityLabel.setVisible(!selectedType.equals(Vehicle.BIKE));
        this.model.setVisible(!selectedType.equals(Vehicle.BIKE));
        this.modelLabel.setVisible(!selectedType.equals(Vehicle.BIKE));
        this.serial.setVisible(!selectedType.equals(Vehicle.BIKE));
        this.serialLabel.setVisible(!selectedType.equals(Vehicle.BIKE));
        this.bikeType.setVisible(selectedType.equals(Vehicle.BIKE));
        this.bikeTypeLabel.setVisible(selectedType.equals(Vehicle.BIKE));
        this.observationsScrollPane.setVisible(selectedType.equals(Vehicle.BIKE));
        this.observationsLabel.setVisible(selectedType.equals(Vehicle.BIKE));
        this.bikeTypeLabel.setVisible(selectedType.equals(Vehicle.BIKE));
        this.formPanel.revalidate();
        this.actionsPanel.revalidate();
        if (selectedType.equals(Vehicle.CAR)
                || selectedType.equals(Vehicle.MOTORCYCLE)) {
            try {
                String oldPlate = (String) this.plate.getValue(), pattern = null;
                switch (selectedType) {
                    case Vehicle.CAR:
                        pattern = "UUU###";
                        break;
                    case Vehicle.MOTORCYCLE:
                        pattern = "UUU##U";
                        break;
                }
                this.plate.setFormatterFactory(
                        new DefaultFormatterFactory(
                                new MaskFormatter(pattern)));
                Object newPlate = parseString(oldPlate, pattern);
                this.plate.setValue(oldPlate == null ? oldPlate : newPlate);
            } catch (Exception ex) {
                getLogger(this.getClass()).warn(ex);
            }
        }
        Dimension dim;
        if (selectedType.equals(Vehicle.BIKE)) {
            dim = new Dimension(this.getMinimumSize());
        } else {
            dim = new Dimension(this.getMaximumSize());
        }
        this.setSize(dim);
        this.setPreferredSize(dim);
        this.setLocationRelativeTo(null);
    }

    private Object parseString(Object value, String pattern) {
        MaskFormatter format = new MaskFormatter();
        format.setValueContainsLiteralCharacters(false);
        Object d = null;
        for (int i = 0; i < pattern.length(); i++) {
            try {
                format.setMask(pattern.substring(0, i));
                d = format.valueToString(value);
            } catch (ParseException e) {
                break;
            }
        }
        return d;
    }

    private void fillComponents() {
        String selectedType = isNew ? this.vehicleType.getSelectedType() : formData.getVehicleType(), _color = null;
        this.vehicleType.setSelectedType(selectedType);
        switch (selectedType) {
            case Vehicle.BIKE:
                Bike b = formData.getBike() == null ? new Bike() : formData.getBike();
                this.bikeType.setSelectedIndex(Integer.parseInt(b.getType()) - 1);
                _color = b.getColor();
                this.observations.setText(b.getObservations());
                break;
            case Vehicle.CAR:
                Car c = formData.getCar() == null ? new Car() : formData.getCar();
                this.plate.setValue(c.getPlate());
                this.city.setText(c.getCity());
                this.model.setText(c.getModel());
                this.serial.setText(c.getSerial());
                _color = c.getColor();
                break;
            case Vehicle.MOTORCYCLE:
                Motorcycle m = formData.getMotorcycle() == null ? new Motorcycle() : formData.getMotorcycle();
                this.plate.setValue(m.getPlate());
                this.city.setText(m.getCity());
                this.model.setText(m.getModel());
                this.serial.setText(m.getSerial());
                _color = m.getColor();
                break;
        }
        if (_color != null) {
            this.color.setSelectedItem(_color);
        }
        refreshPlatePreview();
    }

    protected void refreshPlatePreview() {
        plate.setText(plate.getText().toUpperCase());
        city.setText(city.getText().toUpperCase());
        platePreview.setPlate(plate.getText());
        platePreview.setCity(city.getText());
    }

    private void saveFormData() {
        String selectedType = formData.getVehicleType(), color = ((String) this.color.getSelectedItem()).toUpperCase();
        this.vehicleType.setSelectedType(selectedType);
        switch (selectedType) {
            case Vehicle.BIKE:
                Bike b = formData.getBike() == null ? new Bike() : formData.getBike();
                b.setType(String.valueOf(this.bikeType.getSelectedIndex() + 1));
                b.setColor(color);
                b.setObservations(this.observations.getText().toUpperCase());
                formData.setBike(b);
                break;
            case Vehicle.CAR:
                Car c = formData.getCar() == null ? new Car() : formData.getCar();
                c.setPlate((String) this.plate.getValue());
                c.setCity(this.city.getText().toUpperCase());
                c.setModel(this.model.getText().toUpperCase());
                c.setSerial(this.serial.getText().toUpperCase());
                c.setColor(color);
                formData.setCar(c);
                break;
            case Vehicle.MOTORCYCLE:
                Motorcycle m = formData.getMotorcycle() == null ? new Motorcycle() : formData.getMotorcycle();
                m.setPlate((String) this.plate.getValue());
                m.setCity(this.city.getText().toUpperCase());
                m.setModel(this.model.getText().toUpperCase());
                m.setSerial(this.serial.getText().toUpperCase());
                m.setColor(color);
                formData.setMotorcycle(m);
                break;
        }
        boolean result;
        if (isNew) {
            result = createVehicle(formData);
        } else {
            result = updateVehicle(formData);
        }
        if (result) {
            try {
                VehiclesTabPane openedPane = ((MainView) this.getOwner()).getOpenedPane(VehiclesTabPane.class);
                JButton refreshButton = openedPane.getComponentByName(JButton.class, "refreshButton");
                refreshButton.doClick();
            } catch (Exception _ex) {
            }
            this.dispose();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        observations = new javax.swing.JTextArea();
        bikeType = new javax.swing.JComboBox<>();
        color = new parqueadero.view.component.ColorComboBox();
        serial = new javax.swing.JTextField();
        model = new javax.swing.JTextField();
        city = new javax.swing.JTextField();
        vehicleType = new parqueadero.view.component.VehicleTypeComboBox();
        javax.swing.JLabel vehicleTypeLabel = new javax.swing.JLabel();
        plate = new javax.swing.JFormattedTextField();
        observationsLabel = new javax.swing.JLabel();
        platePreview = new parqueadero.view.component.PlatePanel();
        enabledCheckBox = new javax.swing.JCheckBox();
        saveButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(297, 416));
        setMinimumSize(new java.awt.Dimension(297, 282));
        setModal(true);
        setPreferredSize(new java.awt.Dimension(297, 416));
        setSize(new java.awt.Dimension(297, 416));
        getContentPane().setLayout(new java.awt.BorderLayout(5, 5));

        formPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos de vehículo"));
        formPanel.setLayout(new java.awt.GridBagLayout());

        observations.setColumns(20);
        observations.setLineWrap(true);
        observations.setRows(5);
        observationsScrollPane.setViewportView(observations);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(observationsScrollPane, gridBagConstraints);

        bikeType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Regular", "BMX", "Montaña", "Ruta", "Tour", "Otro tipo" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(bikeType, gridBagConstraints);

        color.setMinimumSize(new java.awt.Dimension(81, 27));
        color.setPreferredSize(new java.awt.Dimension(81, 27));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(color, gridBagConstraints);

        serial.setMinimumSize(new java.awt.Dimension(81, 27));
        serial.setPreferredSize(new java.awt.Dimension(81, 27));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(serial, gridBagConstraints);

        model.setMinimumSize(new java.awt.Dimension(81, 27));
        model.setPreferredSize(new java.awt.Dimension(81, 27));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(model, gridBagConstraints);

        city.setMinimumSize(new java.awt.Dimension(81, 27));
        city.setPreferredSize(new java.awt.Dimension(81, 27));
        city.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                cityKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(city, gridBagConstraints);

        vehicleType.setMinimumSize(new java.awt.Dimension(81, 27));
        vehicleType.setPreferredSize(new java.awt.Dimension(81, 27));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(vehicleType, gridBagConstraints);

        bikeTypeLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        bikeTypeLabel.setText("Tipo de bicicleta");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(bikeTypeLabel, gridBagConstraints);

        colorLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        colorLabel.setText("Color");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(colorLabel, gridBagConstraints);

        serialLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        serialLabel.setText("Serie");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(serialLabel, gridBagConstraints);

        modelLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        modelLabel.setText("Modelo");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(modelLabel, gridBagConstraints);

        cityLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        cityLabel.setText("Ciudad");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(cityLabel, gridBagConstraints);

        plateLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        plateLabel.setText("Placa");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(plateLabel, gridBagConstraints);

        vehicleTypeLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        vehicleTypeLabel.setText("Tipo de vehículo");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(vehicleTypeLabel, gridBagConstraints);

        plate.setFormatterFactory(null);
        plate.setMinimumSize(new java.awt.Dimension(81, 27));
        plate.setPreferredSize(new java.awt.Dimension(81, 27));
        plate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                plateActionPerformed(evt);
            }
        });
        plate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                plateKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(plate, gridBagConstraints);

        observationsLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        observationsLabel.setText("Observaciones");
        observationsLabel.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        observationsLabel.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 0, 0, 0));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(observationsLabel, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        formPanel.add(platePreview, gridBagConstraints);

        getContentPane().add(formPanel, java.awt.BorderLayout.PAGE_START);

        actionsPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        enabledCheckBox.setText("Habilitado");
        actionsPanel.add(enabledCheckBox);

        saveButton.setText("Guardar");
        saveButton.setIconTextGap(5);
        saveButton.setMaximumSize(new java.awt.Dimension(100, 23));
        saveButton.setMinimumSize(new java.awt.Dimension(100, 23));
        saveButton.setPreferredSize(new java.awt.Dimension(100, 23));
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        actionsPanel.add(saveButton);

        cancelButton.setText("Cancelar");
        cancelButton.setIconTextGap(5);
        cancelButton.setMaximumSize(new java.awt.Dimension(100, 23));
        cancelButton.setMinimumSize(new java.awt.Dimension(100, 23));
        cancelButton.setPreferredSize(new java.awt.Dimension(100, 23));
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        actionsPanel.add(cancelButton);

        getContentPane().add(actionsPanel, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        saveFormData();
    }//GEN-LAST:event_saveButtonActionPerformed

    private void plateKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_plateKeyReleased
        refreshPlatePreview();
    }//GEN-LAST:event_plateKeyReleased

    private void plateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_plateActionPerformed
    }//GEN-LAST:event_plateActionPerformed

    private void cityKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cityKeyReleased
        refreshPlatePreview();
    }//GEN-LAST:event_cityKeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private final javax.swing.JPanel actionsPanel = new javax.swing.JPanel();
    private javax.swing.JComboBox<String> bikeType;
    private final javax.swing.JLabel bikeTypeLabel = new javax.swing.JLabel();
    private javax.swing.JButton cancelButton;
    private javax.swing.JTextField city;
    private final javax.swing.JLabel cityLabel = new javax.swing.JLabel();
    private parqueadero.view.component.ColorComboBox color;
    private final javax.swing.JLabel colorLabel = new javax.swing.JLabel();
    private javax.swing.JCheckBox enabledCheckBox;
    private final javax.swing.JPanel formPanel = new javax.swing.JPanel();
    private javax.swing.JTextField model;
    private final javax.swing.JLabel modelLabel = new javax.swing.JLabel();
    private javax.swing.JTextArea observations;
    private javax.swing.JLabel observationsLabel;
    private final javax.swing.JScrollPane observationsScrollPane = new javax.swing.JScrollPane();
    private javax.swing.JFormattedTextField plate;
    private final javax.swing.JLabel plateLabel = new javax.swing.JLabel();
    private parqueadero.view.component.PlatePanel platePreview;
    private javax.swing.JButton saveButton;
    private javax.swing.JTextField serial;
    private final javax.swing.JLabel serialLabel = new javax.swing.JLabel();
    private parqueadero.view.component.VehicleTypeComboBox vehicleType;
    // End of variables declaration//GEN-END:variables

    @Override
    protected void cancelAction() {
        this.cancelButton.doClick();
    }

    @Override
    protected void defaultAction() {
        this.saveButton.doClick();
    }
}
