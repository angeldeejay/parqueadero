/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.view.form;

import java.awt.Color;
import java.sql.Timestamp;
import java.util.Date;
import javax.swing.JButton;
import static jiconfont.icons.font_awesome.FontAwesome.CHECK;
import static jiconfont.icons.font_awesome.FontAwesome.TIMES;
import static jiconfont.icons.font_awesome.FontAwesome.WHEELCHAIR;
import static parqueadero.controller.ParkingLotsController.createLot;
import static parqueadero.controller.ParkingLotsController.getZones;
import static parqueadero.controller.ParkingLotsController.updateLot;
import parqueadero.dao.ZoneDAO;
import parqueadero.model.Lot;
import parqueadero.model.Zone;
import static parqueadero.util.Icon.getIcon;
import parqueadero.view.MainView;
import parqueadero.view.component.ComboBoxRenderer;
import parqueadero.view.pane.ParkingLotsTabPane;

/**
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class LotForm extends parqueadero.view.component.Dialog {

    private final Lot formData;
    private final ZoneDAO zDAO = new ZoneDAO();
    private boolean isNew = false;

    /**
     * Creates new empty form UserForm
     *
     * @param parent
     */
    public LotForm(java.awt.Frame parent) {
        this(parent, new Lot());
        this.isNew = true;
    }

    /**
     * Creates new filled form UserForm
     *
     * @param parent
     * @param l
     */
    public LotForm(java.awt.Frame parent, Lot l) {
        super(parent, true);
        this.formData = l;
        initComponents();
        setUIDefaults();
        fillComponents();
    }

    private void setUIDefaults() {
        this.cancelButton.setIcon(getIcon(TIMES, 14));
        this.saveButton.setIcon(getIcon(CHECK, 14));
        this.zone.setRenderer(new ComboBoxRenderer<>((Zone item) -> "Zona " + item.getCode()));
        this.zone.addItems(getZones());
    }

    private void fillComponents() {
        this.code.setText(formData.getCode());
        this.vehicleType.setSelectedItem(formData.getAllowedType() != null ? formData.getAllowedType() : "Cualquiera");
        this.enabledCheckBox.setSelected(formData.isEnabled());
        Zone z = formData.getZone();
        if (z != null) {
            this.zone.setSelectedItem(formData.getZone());
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        javax.swing.JPanel entityPanel = new javax.swing.JPanel();
        javax.swing.JLabel codeLabel = new javax.swing.JLabel();
        javax.swing.JLabel zoneLabel = new javax.swing.JLabel();
        javax.swing.JLabel vehicleTypeLabel = new javax.swing.JLabel();
        javax.swing.JLabel handicappedLabel = new javax.swing.JLabel();
        code = new javax.swing.JTextField();
        vehicleType = new parqueadero.view.component.VehicleTypeComboBox(true);
        zone = new parqueadero.view.component.ComboBox<>();
        handicapped = new javax.swing.JCheckBox();
        javax.swing.JPanel actionsPanel = new javax.swing.JPanel();
        enabledCheckBox = new javax.swing.JCheckBox();
        saveButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setModal(true);
        setResizable(false);
        getContentPane().setLayout(new java.awt.BorderLayout(5, 5));

        entityPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos generales"));
        entityPanel.setMinimumSize(new java.awt.Dimension(100, 50));
        entityPanel.setPreferredSize(new java.awt.Dimension(400, 130));
        entityPanel.setLayout(new java.awt.GridBagLayout());

        codeLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        codeLabel.setText("Código");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        entityPanel.add(codeLabel, gridBagConstraints);

        zoneLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        zoneLabel.setText("Zona");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        entityPanel.add(zoneLabel, gridBagConstraints);

        vehicleTypeLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        vehicleTypeLabel.setText("Tipo de vehículo");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        entityPanel.add(vehicleTypeLabel, gridBagConstraints);

        handicappedLabel.setFont(handicappedLabel.getFont().deriveFont(handicappedLabel.getFont().getStyle() | java.awt.Font.BOLD));
        handicappedLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        handicappedLabel.setIcon(getIcon(WHEELCHAIR, 18, new Color(50, 50, 200)));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        entityPanel.add(handicappedLabel, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        entityPanel.add(code, gridBagConstraints);

        vehicleType.setMinimumSize(new java.awt.Dimension(107, 27));
        vehicleType.setPreferredSize(new java.awt.Dimension(107, 27));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        entityPanel.add(vehicleType, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        entityPanel.add(zone, gridBagConstraints);

        handicapped.setText("Restringido");
        handicapped.setMaximumSize(new java.awt.Dimension(82, 27));
        handicapped.setMinimumSize(new java.awt.Dimension(82, 27));
        handicapped.setPreferredSize(new java.awt.Dimension(82, 27));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        entityPanel.add(handicapped, gridBagConstraints);

        getContentPane().add(entityPanel, java.awt.BorderLayout.CENTER);

        actionsPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        enabledCheckBox.setText("Habilitado");
        actionsPanel.add(enabledCheckBox);

        saveButton.setText("Guardar");
        saveButton.setIconTextGap(5);
        saveButton.setMaximumSize(new java.awt.Dimension(100, 23));
        saveButton.setMinimumSize(new java.awt.Dimension(100, 23));
        saveButton.setPreferredSize(new java.awt.Dimension(100, 23));
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        actionsPanel.add(saveButton);

        cancelButton.setText("Cancelar");
        cancelButton.setIconTextGap(5);
        cancelButton.setMaximumSize(new java.awt.Dimension(100, 23));
        cancelButton.setMinimumSize(new java.awt.Dimension(100, 23));
        cancelButton.setPreferredSize(new java.awt.Dimension(100, 23));
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        actionsPanel.add(cancelButton);

        getContentPane().add(actionsPanel, java.awt.BorderLayout.PAGE_END);

        setSize(new java.awt.Dimension(335, 240));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        formData.setCode(this.code.getText());
        formData.setHandicapped(handicapped.isSelected());
        formData.setEnabled(this.enabledCheckBox.isSelected());
        formData.setDeletedAt(this.enabledCheckBox.isSelected() ? null : new Timestamp(new Date().getTime()));
        formData.setDeletedBy(this.enabledCheckBox.isSelected() ? null : ((MainView) this.getParent()).getLoggedInEntity().getUser().getId());
        formData.setZone(zone.getSelectedEntity());
        boolean result;
        if (isNew) {
            result = createLot(formData);
        } else {
            result = updateLot(formData);
        }
        if (result) {
            try {
                ParkingLotsTabPane openedPane = ((MainView) this.getOwner()).getOpenedPane(ParkingLotsTabPane.class);
                JButton refreshButton = openedPane.getComponentByName(JButton.class, "refreshButton");
                refreshButton.doClick();
            } catch (Exception _ex) {
            }
            this.dispose();
        }
    }//GEN-LAST:event_saveButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelButton;
    private javax.swing.JTextField code;
    private javax.swing.JCheckBox enabledCheckBox;
    private javax.swing.JCheckBox handicapped;
    private javax.swing.JButton saveButton;
    private parqueadero.view.component.VehicleTypeComboBox vehicleType;
    private parqueadero.view.component.ComboBox<Zone> zone;
    // End of variables declaration//GEN-END:variables

    @Override
    protected void cancelAction() {
        this.cancelButton.doClick();
    }

    @Override
    protected void defaultAction() {
        this.saveButton.doClick();
    }
}
