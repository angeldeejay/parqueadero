/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.view.form;

import java.awt.Color;
import java.sql.Timestamp;
import java.util.Date;
import javax.swing.JButton;
import static jiconfont.icons.font_awesome.FontAwesome.CHECK;
import static jiconfont.icons.font_awesome.FontAwesome.TIMES;
import static jiconfont.icons.font_awesome.FontAwesome.WHEELCHAIR;
import static parqueadero.controller.OwnersController.createOwner;
import static parqueadero.controller.OwnersController.editOwner;
import static parqueadero.controller.OwnersController.getByIdent;
import parqueadero.model.Entity;
import parqueadero.model.Owner;
import static parqueadero.util.Icon.getIcon;
import parqueadero.view.MainView;
import parqueadero.view.pane.OwnersTabPane;

/**
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class OwnerForm extends parqueadero.view.component.Dialog {

    private Entity formData;
    private boolean isNew = false;

    /**
     * Creates new empty form OwnerForm
     *
     * @param parent
     */
    public OwnerForm(java.awt.Frame parent) {
        this(parent, new Entity());
        this.isNew = true;
    }

    /**
     * Creates new filled form OwnerForm
     *
     * @param parent
     * @param e
     */
    public OwnerForm(java.awt.Frame parent, Entity e) {
        super(parent, true);
        this.formData = e;
        initComponents();
        this.cancelButton.setIcon(getIcon(TIMES, 14));
        this.saveButton.setIcon(getIcon(CHECK, 14));
        fillComponents();
    }

    private void fillComponents() {
        fillComponents(false);
    }

    private void fillComponents(boolean skipIdent) {
        Owner o = formData.getOwner();
        if (o != null) {
            this.handicapped.setSelected(o.isHandicapped());
        }
        this.firstName.setText(formData.getFirstName());
        this.lastName.setText(formData.getLastName());
        if (!skipIdent) {
            this.identType.setSelectedType(formData.getIdentType());
            this.identValue.setText(formData.getIdentValue());
        }
        this.enabledCheckBox.setSelected((o != null && o.isEnabled()) || o == null);
    }

    private void saveFormData() {
        this.formData = getByIdent(identType.getSelectedType(), identValue.getText());
        this.isNew = (this.formData == null);
        if (this.formData == null) {
            this.formData = new Entity();
        }
        formData.setFirstName(this.firstName.getText());
        formData.setLastName(this.lastName.getText());
        formData.setIdentType(this.identType.getSelectedType());
        formData.setIdentValue(this.identValue.getText());

        Owner o = formData.getOwner();
        if (o == null) {
            o = new Owner();
        }
        o.setHandicapped(this.handicapped.isSelected());
        o.setEnabled(this.enabledCheckBox.isSelected());
        o.setDeletedAt(this.enabledCheckBox.isSelected() ? null : new Timestamp(new Date().getTime()));
        o.setDeletedBy(this.enabledCheckBox.isSelected() ? null : ((MainView) this.getParent()).getLoggedInEntity().getUser().getId());
        formData.setOwner(o);
        boolean result = false;
        if (isNew) {
            result = createOwner(formData);
        } else {
            result = editOwner(formData);
        }
        if (result) {
            try {
                OwnersTabPane openedPane = ((MainView) this.getOwner()).getOpenedPane(OwnersTabPane.class);
                JButton refreshButton = openedPane.getComponentByName(JButton.class, "refreshButton");
                refreshButton.doClick();
            } catch (Exception _ex) {
            }
            this.dispose();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        javax.swing.JPanel userPanel = new javax.swing.JPanel();
        handicapped = new javax.swing.JCheckBox();
        javax.swing.JLabel handicappedLabel = new javax.swing.JLabel();
        javax.swing.JPanel entityPanel = new javax.swing.JPanel();
        javax.swing.JLabel firstNameLabel = new javax.swing.JLabel();
        javax.swing.JLabel lastNameLabel = new javax.swing.JLabel();
        javax.swing.JLabel identTypeLabel = new javax.swing.JLabel();
        javax.swing.JLabel identValueLabel = new javax.swing.JLabel();
        firstName = new javax.swing.JTextField();
        lastName = new javax.swing.JTextField();
        identType = new parqueadero.view.component.IdentTypeComboBox();
        identValue = new parqueadero.view.component.NumberTextField();
        javax.swing.JPanel actionsPanel = new javax.swing.JPanel();
        enabledCheckBox = new javax.swing.JCheckBox();
        saveButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setModal(true);
        setResizable(false);
        getContentPane().setLayout(new java.awt.BorderLayout(5, 5));

        userPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos de propietario"));
        userPanel.setMinimumSize(new java.awt.Dimension(400, 100));
        userPanel.setPreferredSize(new java.awt.Dimension(400, 100));
        userPanel.setLayout(new java.awt.GridBagLayout());

        handicapped.setText("Restringido");
        handicapped.setMaximumSize(new java.awt.Dimension(82, 27));
        handicapped.setMinimumSize(new java.awt.Dimension(82, 27));
        handicapped.setPreferredSize(new java.awt.Dimension(82, 27));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        userPanel.add(handicapped, gridBagConstraints);

        handicappedLabel.setFont(handicappedLabel.getFont().deriveFont(handicappedLabel.getFont().getStyle() | java.awt.Font.BOLD));
        handicappedLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        handicappedLabel.setMaximumSize(new java.awt.Dimension(1000, 0));
        handicappedLabel.setMinimumSize(new java.awt.Dimension(20, 0));
        handicappedLabel.setPreferredSize(new java.awt.Dimension(93, 0));
        handicappedLabel.setIcon(getIcon(WHEELCHAIR, 18, new Color(50, 50, 200)));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        userPanel.add(handicappedLabel, gridBagConstraints);

        getContentPane().add(userPanel, java.awt.BorderLayout.CENTER);

        entityPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos generales"));
        entityPanel.setMinimumSize(new java.awt.Dimension(100, 50));
        entityPanel.setPreferredSize(new java.awt.Dimension(325, 185));
        entityPanel.setLayout(new java.awt.GridBagLayout());

        firstNameLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        firstNameLabel.setText("Nombres");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        entityPanel.add(firstNameLabel, gridBagConstraints);

        lastNameLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lastNameLabel.setText("Apellidos");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        entityPanel.add(lastNameLabel, gridBagConstraints);

        identTypeLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        identTypeLabel.setText("Tipo de documento");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        entityPanel.add(identTypeLabel, gridBagConstraints);

        identValueLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        identValueLabel.setText("Número");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        entityPanel.add(identValueLabel, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        entityPanel.add(firstName, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        entityPanel.add(lastName, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        entityPanel.add(identType, gridBagConstraints);

        identValue.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                identValueKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(6, 6, 6, 6);
        entityPanel.add(identValue, gridBagConstraints);

        getContentPane().add(entityPanel, java.awt.BorderLayout.PAGE_START);

        actionsPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        enabledCheckBox.setText("Habilitado");
        actionsPanel.add(enabledCheckBox);

        saveButton.setText("Guardar");
        saveButton.setIconTextGap(5);
        saveButton.setMaximumSize(new java.awt.Dimension(100, 23));
        saveButton.setMinimumSize(new java.awt.Dimension(100, 23));
        saveButton.setPreferredSize(new java.awt.Dimension(100, 23));
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        actionsPanel.add(saveButton);

        cancelButton.setText("Cancelar");
        cancelButton.setIconTextGap(5);
        cancelButton.setMaximumSize(new java.awt.Dimension(100, 23));
        cancelButton.setMinimumSize(new java.awt.Dimension(100, 23));
        cancelButton.setPreferredSize(new java.awt.Dimension(100, 23));
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        actionsPanel.add(cancelButton);

        getContentPane().add(actionsPanel, java.awt.BorderLayout.PAGE_END);

        setSize(new java.awt.Dimension(335, 320));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        saveFormData();
    }//GEN-LAST:event_saveButtonActionPerformed

    private void identValueKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_identValueKeyReleased
        this.formData = getByIdent(identType.getSelectedType(), identValue.getText());
        this.isNew = (this.formData == null);
        if (this.formData == null) {
            this.formData = new Entity();
        }
        fillComponents(true);
    }//GEN-LAST:event_identValueKeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelButton;
    private javax.swing.JCheckBox enabledCheckBox;
    private javax.swing.JTextField firstName;
    private javax.swing.JCheckBox handicapped;
    private parqueadero.view.component.IdentTypeComboBox identType;
    private parqueadero.view.component.NumberTextField identValue;
    private javax.swing.JTextField lastName;
    private javax.swing.JButton saveButton;
    // End of variables declaration//GEN-END:variables

    @Override
    protected void cancelAction() {
        this.cancelButton.doClick();
    }

    @Override
    protected void defaultAction() {
        this.saveButton.doClick();
    }
}
