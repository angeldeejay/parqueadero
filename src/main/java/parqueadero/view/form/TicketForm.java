/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.view.form;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;
import javax.swing.JButton;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.showMessageDialog;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;
import static jiconfont.icons.font_awesome.FontAwesome.CHECK;
import static jiconfont.icons.font_awesome.FontAwesome.TIMES;
import parqueadero.controller.OwnersController;
import static parqueadero.controller.ParkingLotsController.getLots;
import parqueadero.controller.TicketsController;
import static parqueadero.controller.TicketsController.createTicket;
import parqueadero.controller.VehiclesController;
import parqueadero.dao.EntityDAO;
import parqueadero.dao.LotDAO;
import parqueadero.dao.VehicleDAO;
import parqueadero.model.Bike;
import parqueadero.model.Car;
import parqueadero.model.Entity;
import parqueadero.model.Lot;
import parqueadero.model.Motorcycle;
import parqueadero.model.Owner;
import parqueadero.model.Ticket;
import parqueadero.model.Vehicle;
import static parqueadero.util.Icon.getIcon;
import static parqueadero.util.Logger.getLogger;
import parqueadero.view.MainView;
import parqueadero.view.component.ComboBoxRenderer;
import parqueadero.view.pane.TicketsTabPane;

/**
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class TicketForm extends  parqueadero.view.component.Dialog {

    private Vehicle vehicle;
    private Entity owner;
    private boolean isNewVehicle;
    private boolean isNewEntity;
    private boolean isNewOwner;

    /**
     * Creates new empty form UserForm
     *
     * @param parent
     */
    public TicketForm(java.awt.Frame parent) {
        super(parent, true);
        this.vehicle = new Vehicle();
        this.owner = new Entity();
        isNewVehicle = true;
        isNewOwner = true;
        initComponents();
        setUIDefaults();
        fillVehicleComponents();
        fillOwnerComponents();
    }

    private void setUIDefaults() {
        this.cancelButton.setIcon(getIcon(TIMES, 14));
        this.saveButton.setIcon(getIcon(CHECK, 14));
        this.lot.setRenderer(new ComboBoxRenderer<>((Lot l) -> l.getZone().getCode() + "-" + l.getCode()));
        this.vehicle.setVehicleType(Vehicle.CAR);
        this.vehicleType.addItemListener((ItemEvent e) -> {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                vehicle.setVehicleType(vehicleType.getSelectedType());
                lot.removeAllItems();
                getLots(null).forEach((Lot l) -> {
                    if (l.isEmpty() && (l.getAllowedType() == null
                            || l.getAllowedType().equalsIgnoreCase(vehicleType.getSelectedType()))) {
                        lot.addItem(l);
                    }
                });
                lot.setSelectedIndex(0);
                refreshFormBySelectedtype();
                refreshPlatePreview();
            }
        });
        this.vehicleType.setSelectedType(Vehicle.CAR);
    }

    private void refreshFormBySelectedtype() {
        String selectedType = vehicle.getVehicleType();
        this.platePreview.setVisible(!selectedType.equals(Vehicle.BIKE));
        this.plate.setVisible(!selectedType.equals(Vehicle.BIKE));
        this.plateLabel.setVisible(!selectedType.equals(Vehicle.BIKE));
        this.city.setVisible(!selectedType.equals(Vehicle.BIKE));
        this.cityLabel.setVisible(!selectedType.equals(Vehicle.BIKE));
        this.model.setVisible(!selectedType.equals(Vehicle.BIKE));
        this.modelLabel.setVisible(!selectedType.equals(Vehicle.BIKE));
        this.serial.setVisible(!selectedType.equals(Vehicle.BIKE));
        this.serialLabel.setVisible(!selectedType.equals(Vehicle.BIKE));
        this.bikeType.setVisible(selectedType.equals(Vehicle.BIKE));
        this.bikeTypeLabel.setVisible(selectedType.equals(Vehicle.BIKE));
        this.observationsScrollPane.setVisible(selectedType.equals(Vehicle.BIKE));
        this.observationsLabel.setVisible(selectedType.equals(Vehicle.BIKE));
        this.bikeTypeLabel.setVisible(selectedType.equals(Vehicle.BIKE));
        this.formPanel.revalidate();
        this.actionsPanel.revalidate();
        Dimension d = new Dimension(300, 220);
        if (selectedType.equals(Vehicle.CAR)
                || selectedType.equals(Vehicle.MOTORCYCLE)) {
            try {
                String oldPlate = (String) this.plate.getValue(), pattern = null;
                switch (selectedType) {
                    case Vehicle.CAR:
                        pattern = "UUU###";
                        break;
                    case Vehicle.MOTORCYCLE:
                        pattern = "UUU##U";
                        break;
                }
                this.plate.setFormatterFactory(
                        new DefaultFormatterFactory(
                                new MaskFormatter(pattern)));
                Object newPlate = parseString(oldPlate, pattern);
                this.plate.setValue(oldPlate == null ? oldPlate : newPlate);
            } catch (Exception ex) {
                getLogger(this.getClass()).warn(ex);
            }
            d = new Dimension(300, 354);
        }
        this.formPanel.setSize(d);
        this.formPanel.setMaximumSize(d);
        this.formPanel.setMinimumSize(d);
        this.formPanel.setPreferredSize(d);
        this.pack();
        this.setLocationRelativeTo(null);
    }

    private Object parseString(Object value, String pattern) {
        MaskFormatter format = new MaskFormatter();
        format.setValueContainsLiteralCharacters(false);
        Object d = null;
        for (int i = 0; i < pattern.length(); i++) {
            try {
                format.setMask(pattern.substring(0, i));
                d = format.valueToString(value);
            } catch (ParseException e) {
                break;
            }
        }
        return d;
    }

    private void fillVehicleComponents() {
        fillVehicleComponents(false);
    }

    private void fillVehicleComponents(boolean skipIdent) {
        String selectedType = vehicle.getVehicleType() == null ? vehicleType.getSelectedType() : vehicle.getVehicleType(), _color = null;
        this.vehicleType.setSelectedType(selectedType);
        switch (selectedType) {
            case Vehicle.BIKE:
                Bike b = vehicle.getId() == null ? new Bike() : vehicle.getBike();
                this.bikeType.setSelectedIndex(Integer.parseInt(b.getType()) - 1);
                _color = b.getColor();
                this.observations.setText(b.getObservations());
                break;
            case Vehicle.CAR:
                Car c = vehicle.getId() == null ? new Car() : vehicle.getCar();
                if (!skipIdent) {
                    this.plate.setValue(c.getPlate());
                }
                this.city.setText(c.getCity());
                this.model.setText(c.getModel());
                this.serial.setText(c.getSerial());
                _color = c.getColor();
                break;
            case Vehicle.MOTORCYCLE:
                Motorcycle m = vehicle.getId() == null ? new Motorcycle() : vehicle.getMotorcycle();
                if (!skipIdent) {
                    this.plate.setValue(m.getPlate());
                }
                this.city.setText(m.getCity());
                this.model.setText(m.getModel());
                this.serial.setText(m.getSerial());
                _color = m.getColor();
                break;
        }
        if (_color != null) {
            this.color.setSelectedItem(_color);
        }
        refreshPlatePreview();
    }

    private void fillOwnerComponents() {
        fillOwnerComponents(false);
    }

    private void fillOwnerComponents(boolean skipIdent) {
        this.firstName.setText(owner.getFirstName());
        this.lastName.setText(owner.getLastName());
        if (!skipIdent) {
            this.identType.setSelectedType(owner.getIdentType());
            this.identValue.setText(owner.getIdentValue());
        }
    }

    protected void refreshPlatePreview() {
        plate.setText(plate.getText().toUpperCase());
        city.setText(city.getText().toUpperCase());
        platePreview.setPlate(plate.getText());
        platePreview.setCity(city.getText());
    }

    private void saveFormData() {
        retrieveOwner();
        retrieveVehicle();
        Ticket t = new Ticket();
        t.setCheckin(new Timestamp(((Date) new Date()).getTime()));
        t.setCheckout(null);
        t.setEndPrice(new BigDecimal(0));
        t.setObservations(null);
        EntityDAO eDAO = new EntityDAO();
        if (isNewEntity) {
            BigDecimal id = eDAO.add(owner);
            if (id == null) {
                return;
            }
            owner.setId(id);
            this.isNewOwner = false;
        } else {
            if (isNewOwner) {
                eDAO.update(owner);
            }
        }
        owner = eDAO.getById(owner.getId().intValueExact());
        t.setOwnerId(owner.getId());
        t.setOwner(owner.getOwner());
        VehicleDAO vDAO = new VehicleDAO();
        if (isNewVehicle) {
            BigDecimal id = vDAO.add(vehicle);
            if (id == null) {
                return;
            }
            vehicle = vDAO.getById(id.intValueExact());
        } else {
            vehicle = vDAO.getById(vehicle.getId().intValueExact());
        }
        t.setVehicleId(vehicle.getId());
        t.setVehicle(vehicle);
        t.setLotId(lot.getSelectedId());
        t.setUserId(((MainView) this.getParent()).getLoggedInEntity().getUser().getId());
        boolean result = createTicket(t);
        LotDAO ldao = new LotDAO();
        Lot l = t.getLot();
        l.setEmpty(false);
        ldao.update(l);
        if (result) {
            try {
                TicketsTabPane openedPane = ((MainView) this.getOwner()).getOpenedPane(TicketsTabPane.class);
                JButton refreshButton = openedPane.getComponentByName(JButton.class, "refreshButton");
                refreshButton.doClick();
            } catch (Exception _ex) {
            }
            this.dispose();
        }
    }

    private void retrieveOwner() {
        Entity o = OwnersController.getByIdent(identType.getSelectedType(), identValue.getText());
        if (o != null) {
            this.isNewEntity = false;
            this.owner = o;
        } else {
            this.isNewEntity = true;
            this.owner = new Entity();
            this.owner.setFirstName(firstName.getText());
            this.owner.setLastName(lastName.getText());
            this.owner.setIdentType(identType.getSelectedType());
            this.owner.setIdentValue(identValue.getText());
        }
        if (this.owner.getOwner() != null) {
            this.isNewOwner = false;
        } else {
            this.isNewOwner = true;
            this.owner.setOwner(new Owner());
        }
    }

    private void retrieveVehicle() {
        Object[] params = null;
        String selectedType = vehicle.getVehicleType() == null ? vehicleType.getSelectedType() : vehicle.getVehicleType();
        String colorValue = (String) this.color.getSelectedItem(), plateValue = this.plate.getText();
        switch (selectedType) {
            case Vehicle.BIKE:
                params = new Object[]{this.bikeType.getSelectedIndex() + 1, colorValue};
                break;
            case Vehicle.CAR:
            case Vehicle.MOTORCYCLE:
                params = new Object[]{plateValue};
                break;
        }
        Vehicle v = VehiclesController.getByIdent(selectedType, params);
        Ticket activeTicket = v != null ? TicketsController.getActiveTicket(v) : null;
        if (activeTicket != null) {
            showMessageDialog(this,
                    "El vehículo descrito se encuentra dentro del parqueadero",
                    "Vehículo ingresado",
                    ERROR_MESSAGE,
                    getIcon(TIMES, 48, Color.RED)
            );
            this.plate.setValue(new String(""));
            this.plate.setText(new String(""));
            return;
        }
        if (v != null) {
            this.isNewVehicle = false;
            this.vehicle = v;
        } else {
            this.vehicle = new Vehicle();
            String selectedColor = ((String) this.color.getSelectedItem()).toUpperCase();
            this.vehicle.setVehicleType(selectedType);
            switch (selectedType) {
                case Vehicle.BIKE:
                    Bike b = new Bike();
                    b.setType(String.valueOf(this.bikeType.getSelectedIndex() + 1));
                    b.setColor(selectedColor);
                    b.setObservations(this.observations.getText().toUpperCase());
                    this.vehicle.setBike(b);
                    break;
                case Vehicle.CAR:
                    Car c = new Car();
                    c.setPlate((String) this.plate.getValue());
                    c.setCity(this.city.getText().toUpperCase());
                    c.setModel(this.model.getText().toUpperCase());
                    c.setSerial(this.serial.getText().toUpperCase());
                    c.setColor(selectedColor);
                    this.vehicle.setCar(c);
                    break;
                case Vehicle.MOTORCYCLE:
                    Motorcycle m = new Motorcycle();
                    m.setPlate((String) this.plate.getValue());
                    m.setCity(this.city.getText().toUpperCase());
                    m.setModel(this.model.getText().toUpperCase());
                    m.setSerial(this.serial.getText().toUpperCase());
                    m.setColor(selectedColor);
                    this.vehicle.setMotorcycle(m);
                    break;
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        observations = new javax.swing.JTextArea();
        bikeType = new javax.swing.JComboBox<>();
        color = new parqueadero.view.component.ColorComboBox();
        serial = new javax.swing.JTextField();
        model = new javax.swing.JTextField();
        city = new javax.swing.JTextField();
        vehicleType = new parqueadero.view.component.VehicleTypeComboBox();
        javax.swing.JLabel vehicleTypeLabel = new javax.swing.JLabel();
        platePreview = new parqueadero.view.component.PlatePanel();
        plate = new javax.swing.JFormattedTextField();
        observationsLabel = new javax.swing.JLabel();
        javax.swing.JPanel entityPanel = new javax.swing.JPanel();
        javax.swing.JLabel firstNameLabel = new javax.swing.JLabel();
        javax.swing.JLabel lastNameLabel = new javax.swing.JLabel();
        javax.swing.JLabel identTypeLabel = new javax.swing.JLabel();
        javax.swing.JLabel identValueLabel = new javax.swing.JLabel();
        firstName = new javax.swing.JTextField();
        lastName = new javax.swing.JTextField();
        identType = new parqueadero.view.component.IdentTypeComboBox();
        identValue = new parqueadero.view.component.NumberTextField();
        saveButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        javax.swing.JPanel locationPane = new javax.swing.JPanel();
        lot = new parqueadero.view.component.ComboBox<>();
        javax.swing.JLabel lotLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setModal(true);
        setSize(new java.awt.Dimension(297, 416));

        formPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos de vehículo"));
        formPanel.setMinimumSize(new java.awt.Dimension(300, 360));
        formPanel.setPreferredSize(new java.awt.Dimension(300, 478));
        formPanel.setLayout(new java.awt.GridBagLayout());

        observationsScrollPane.setMinimumSize(new java.awt.Dimension(133, 75));
        observationsScrollPane.setPreferredSize(new java.awt.Dimension(133, 75));

        observations.setColumns(20);
        observations.setLineWrap(true);
        observations.setRows(5);
        observations.setMinimumSize(new java.awt.Dimension(180, 75));
        observationsScrollPane.setViewportView(observations);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.2;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(observationsScrollPane, gridBagConstraints);

        bikeType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Regular", "BMX", "Montaña", "Ruta", "Tour", "Otro tipo" }));
        bikeType.setMaximumSize(new java.awt.Dimension(133, 27));
        bikeType.setMinimumSize(new java.awt.Dimension(133, 27));
        bikeType.setPreferredSize(new java.awt.Dimension(133, 27));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(bikeType, gridBagConstraints);

        color.setMaximumSize(new java.awt.Dimension(133, 27));
        color.setMinimumSize(new java.awt.Dimension(133, 27));
        color.setPreferredSize(new java.awt.Dimension(133, 27));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(color, gridBagConstraints);

        serial.setMaximumSize(new java.awt.Dimension(133, 27));
        serial.setMinimumSize(new java.awt.Dimension(133, 27));
        serial.setPreferredSize(new java.awt.Dimension(133, 27));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(serial, gridBagConstraints);

        model.setMaximumSize(new java.awt.Dimension(133, 27));
        model.setMinimumSize(new java.awt.Dimension(133, 27));
        model.setPreferredSize(new java.awt.Dimension(133, 27));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(model, gridBagConstraints);

        city.setMaximumSize(new java.awt.Dimension(133, 27));
        city.setMinimumSize(new java.awt.Dimension(133, 27));
        city.setPreferredSize(new java.awt.Dimension(133, 27));
        city.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                cityKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(city, gridBagConstraints);

        vehicleType.setMaximumSize(new java.awt.Dimension(133, 27));
        vehicleType.setMinimumSize(new java.awt.Dimension(133, 27));
        vehicleType.setPreferredSize(new java.awt.Dimension(133, 27));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(vehicleType, gridBagConstraints);

        bikeTypeLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        bikeTypeLabel.setText("Tipo de bicicleta");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(bikeTypeLabel, gridBagConstraints);

        colorLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        colorLabel.setText("Color");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(colorLabel, gridBagConstraints);

        serialLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        serialLabel.setText("Serie");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(serialLabel, gridBagConstraints);

        modelLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        modelLabel.setText("Modelo");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(modelLabel, gridBagConstraints);

        cityLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        cityLabel.setText("Ciudad");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(cityLabel, gridBagConstraints);

        plateLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        plateLabel.setText("Placa");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(plateLabel, gridBagConstraints);

        vehicleTypeLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        vehicleTypeLabel.setText("Tipo de vehículo");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(vehicleTypeLabel, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        formPanel.add(platePreview, gridBagConstraints);

        plate.setFormatterFactory(null);
        plate.setMaximumSize(new java.awt.Dimension(133, 27));
        plate.setMinimumSize(new java.awt.Dimension(133, 27));
        plate.setPreferredSize(new java.awt.Dimension(133, 27));
        plate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                plateActionPerformed(evt);
            }
        });
        plate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                plateKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(plate, gridBagConstraints);

        observationsLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        observationsLabel.setText("Observaciones");
        observationsLabel.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        observationsLabel.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 0, 0, 0));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        formPanel.add(observationsLabel, gridBagConstraints);

        entityPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos del conductor"));
        entityPanel.setMinimumSize(new java.awt.Dimension(300, 50));
        entityPanel.setPreferredSize(new java.awt.Dimension(300, 185));
        java.awt.GridBagLayout entityPanelLayout = new java.awt.GridBagLayout();
        entityPanelLayout.columnWidths = new int[] {0, 5, 0, 5, 0};
        entityPanelLayout.rowHeights = new int[] {0, 10, 0, 10, 0, 10, 0};
        entityPanel.setLayout(entityPanelLayout);

        firstNameLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        firstNameLabel.setText("Nombres");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        entityPanel.add(firstNameLabel, gridBagConstraints);

        lastNameLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lastNameLabel.setText("Apellidos");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        entityPanel.add(lastNameLabel, gridBagConstraints);

        identTypeLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        identTypeLabel.setText("Tipo de documento");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        entityPanel.add(identTypeLabel, gridBagConstraints);

        identValueLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        identValueLabel.setText("Número");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        entityPanel.add(identValueLabel, gridBagConstraints);

        firstName.setMaximumSize(new java.awt.Dimension(133, 27));
        firstName.setMinimumSize(new java.awt.Dimension(133, 27));
        firstName.setPreferredSize(new java.awt.Dimension(133, 27));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        entityPanel.add(firstName, gridBagConstraints);

        lastName.setMaximumSize(new java.awt.Dimension(133, 27));
        lastName.setMinimumSize(new java.awt.Dimension(133, 27));
        lastName.setPreferredSize(new java.awt.Dimension(133, 27));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        entityPanel.add(lastName, gridBagConstraints);

        identType.setMaximumSize(new java.awt.Dimension(133, 27));
        identType.setMinimumSize(new java.awt.Dimension(133, 27));
        identType.setPreferredSize(new java.awt.Dimension(133, 27));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        entityPanel.add(identType, gridBagConstraints);

        identValue.setMaximumSize(new java.awt.Dimension(133, 27));
        identValue.setMinimumSize(new java.awt.Dimension(133, 27));
        identValue.setPreferredSize(new java.awt.Dimension(133, 27));
        identValue.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                identValueKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        entityPanel.add(identValue, gridBagConstraints);

        actionsPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        saveButton.setText("Guardar");
        saveButton.setIconTextGap(5);
        saveButton.setMaximumSize(new java.awt.Dimension(100, 23));
        saveButton.setMinimumSize(new java.awt.Dimension(100, 23));
        saveButton.setPreferredSize(new java.awt.Dimension(100, 23));
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        actionsPanel.add(saveButton);

        cancelButton.setText("Cancelar");
        cancelButton.setIconTextGap(5);
        cancelButton.setMaximumSize(new java.awt.Dimension(100, 23));
        cancelButton.setMinimumSize(new java.awt.Dimension(100, 23));
        cancelButton.setPreferredSize(new java.awt.Dimension(100, 23));
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        actionsPanel.add(cancelButton);

        locationPane.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos del ubicación"));
        locationPane.setMinimumSize(new java.awt.Dimension(300, 50));
        locationPane.setPreferredSize(new java.awt.Dimension(300, 185));
        locationPane.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        locationPane.add(lot, gridBagConstraints);

        lotLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lotLabel.setText("Bahía");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        locationPane.add(lotLabel, gridBagConstraints);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(formPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(locationPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(entityPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addComponent(actionsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(entityPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(3, 3, 3)
                        .addComponent(locationPane, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(formPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(0, 0, 0)
                .addComponent(actionsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        saveFormData();
    }//GEN-LAST:event_saveButtonActionPerformed

    private void plateKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_plateKeyReleased
        if (plate.getText().trim().length() == 6) {
            retrieveVehicle();
            fillVehicleComponents(true);
        }
        refreshPlatePreview();
    }//GEN-LAST:event_plateKeyReleased

    private void plateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_plateActionPerformed
    }//GEN-LAST:event_plateActionPerformed

    private void cityKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cityKeyReleased
        refreshPlatePreview();
    }//GEN-LAST:event_cityKeyReleased

    private void identValueKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_identValueKeyReleased
        retrieveOwner();
        fillOwnerComponents(true);
    }//GEN-LAST:event_identValueKeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private final javax.swing.JPanel actionsPanel = new javax.swing.JPanel();
    private javax.swing.JComboBox<String> bikeType;
    private final javax.swing.JLabel bikeTypeLabel = new javax.swing.JLabel();
    private javax.swing.JButton cancelButton;
    private javax.swing.JTextField city;
    private final javax.swing.JLabel cityLabel = new javax.swing.JLabel();
    private parqueadero.view.component.ColorComboBox color;
    private final javax.swing.JLabel colorLabel = new javax.swing.JLabel();
    private javax.swing.JTextField firstName;
    private final javax.swing.JPanel formPanel = new javax.swing.JPanel();
    private parqueadero.view.component.IdentTypeComboBox identType;
    private parqueadero.view.component.NumberTextField identValue;
    private javax.swing.JTextField lastName;
    private parqueadero.view.component.ComboBox<Lot> lot;
    private javax.swing.JTextField model;
    private final javax.swing.JLabel modelLabel = new javax.swing.JLabel();
    private javax.swing.JTextArea observations;
    private javax.swing.JLabel observationsLabel;
    private final javax.swing.JScrollPane observationsScrollPane = new javax.swing.JScrollPane();
    private javax.swing.JFormattedTextField plate;
    private final javax.swing.JLabel plateLabel = new javax.swing.JLabel();
    private parqueadero.view.component.PlatePanel platePreview;
    private javax.swing.JButton saveButton;
    private javax.swing.JTextField serial;
    private final javax.swing.JLabel serialLabel = new javax.swing.JLabel();
    private parqueadero.view.component.VehicleTypeComboBox vehicleType;
    // End of variables declaration//GEN-END:variables

    @Override
    protected void cancelAction() {
        this.cancelButton.doClick();
    }

    @Override
    protected void defaultAction() {
        this.saveButton.doClick();
    }
}
