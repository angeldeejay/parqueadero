/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.view;

import java.awt.Color;
import static java.awt.Color.LIGHT_GRAY;
import static java.awt.Color.WHITE;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JToggleButton;
import javax.swing.event.ChangeEvent;
import jiconfont.icons.font_awesome.FontAwesome;
import static jiconfont.icons.font_awesome.FontAwesome.CIRCLE;
import static jiconfont.icons.font_awesome.FontAwesome.LOCK;
import static jiconfont.icons.font_awesome.FontAwesome.TIMES;
import static jiconfont.icons.font_awesome.FontAwesome.USER_CIRCLE;
import parqueadero.Main;
import parqueadero.model.Entity;
import static parqueadero.util.DataSource.isValidConfig;
import parqueadero.util.Icon;
import static parqueadero.util.Icon.getIcon;
import static parqueadero.util.Logger.getLogger;
import parqueadero.util.Session;
import parqueadero.view.component.TabPane;
import parqueadero.view.dialog.LoginDialog;
import parqueadero.view.pane.OwnersTabPane;
import parqueadero.view.pane.ParkingLotsTabPane;
import parqueadero.view.pane.PricesTabPane;
import parqueadero.view.pane.ReportsTabPane;
import parqueadero.view.pane.RolesTabPane;
import parqueadero.view.pane.TicketsTabPane;
import parqueadero.view.pane.UsersTabPane;
import parqueadero.view.pane.VehiclesTabPane;

/**
 * MainView View class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class MainView extends javax.swing.JFrame {

    private boolean connected = false;
    private Session session;
    private Session originSession;

    /**
     * Creates new form Main
     */
    public MainView() {
        this.session = null;
        this.originSession = null;
        initComponents();
        setFrameDefaults();
        setUIDefaults();
        checkConnectionStatus();
        logoutButton.doClick();
    }

    public Session getSession() {
        return session;
    }

    public Session getOriginSession() {
        return originSession;
    }

    /**
     * Frame defaults on initialize
     */
    private void setFrameDefaults() {
        setVisible(true);
        setExtendedState(MAXIMIZED_BOTH);
    }

    /**
     * Components defaults on initialize
     */
    private <T extends TabPane> void setUIDefaults() {
        handleSessionUI();
        /* Link tabs in tabbed pane with toggle buttons in menu */
        final MainView mainView = this;
        mainTabbedPane.addChangeListener((ChangeEvent changeEvent) -> {
            mainView.toggleMenuOption((T) mainTabbedPane.getSelectedComponent());
        });
    }

    /**
     * User session setter
     *
     * @param session
     */
    public void setSession(Session session) {
        this.session = session;
        this.handleSessionUI();
    }

    /**
     * User session setter
     *
     * @return Entity
     */
    public Entity getLoggedInEntity() {
        return session.getEntity();
    }

    /**
     * Enable/disable main menu
     *
     * @param enable
     */
    public void enableMenu(boolean enable) {
        for (Component c : mainToolbar.getComponents()) {
            if (c.equals(logoutButton) || c.equals(logoutGlue)) {
                continue;
            }
            c.setEnabled(enable);
            c.setVisible(enable);
        }

        if (this.session != null && this.session.isValid()) {
            boolean enableTickets = this.session.hasAnyPermission(new String[]{
                "LISTAR_TICKETS"
            });
            boolean enablePrices = this.session.hasAnyPermission(new String[]{
                "LISTAR_TARIFAS"
            });
            boolean enableVehicles = this.session.hasAnyPermission(new String[]{
                "LISTAR_VEHICULOS"
            });
            boolean enableOwners = this.session.hasAnyPermission(new String[]{
                "LISTAR_CLIENTES"
            });
            boolean enableParklot = this.session.hasAnyPermission(new String[]{
                "LISTAR_LOTES_DE_PARQUEADERO"
            });
            boolean enableUsers = this.session.hasAnyPermission(new String[]{
                "LISTAR_USUARIOS"
            });
            boolean enableRoles = this.session.hasAnyPermission(new String[]{
                "LISTAR_PERFILES_DE_CONTROL_DE_ACCESO"
            });
            boolean enableReports = this.session.hasAnyPermission(new String[]{
                "VER_REPORTE_GENERAL",
                "VER_REPORTE_MAPA_DE_CALOR",
                "VER_REPORTE_BIOMETRICO"
            });
            this.ticketsMenuButton.setVisible(enable && enableTickets);
            this.ticketsMenuButton.setEnabled(enable && enableTickets);
            this.pricesMenuButton.setVisible(enable && enablePrices);
            this.pricesMenuButton.setEnabled(enable && enablePrices);
            this.vehiclesMenuButton.setVisible(enable && enableVehicles);
            this.vehiclesMenuButton.setEnabled(enable && enableVehicles);
            this.ownersMenuButton.setVisible(enable && enableOwners);
            this.ownersMenuButton.setEnabled(enable && enableOwners);
            this.parklotMenuButton.setVisible(enable && enableParklot);
            this.parklotMenuButton.setEnabled(enable && enableParklot);
            this.usersMenuButton.setVisible(enable && enableUsers);
            this.usersMenuButton.setEnabled(enable && enableUsers);
            this.rolesMenuButton.setVisible(enable && enableRoles);
            this.rolesMenuButton.setEnabled(enable && enableRoles);
            this.reportsMenuButton.setVisible(enable && enableReports);
            this.reportsMenuButton.setEnabled(enable && enableReports);
        }
    }

    /**
     * Checks one menu button each time per once
     *
     * @param source
     */
    public void toggleMenuOption(JToggleButton source) {
        JToggleButton[] menuButtons = new JToggleButton[]{
            ownersMenuButton,
            parklotMenuButton,
            pricesMenuButton,
            reportsMenuButton,
            rolesMenuButton,
            ticketsMenuButton,
            usersMenuButton,
            vehiclesMenuButton
        };
        for (JToggleButton b : menuButtons) {
            b.setSelected(source != null ? source.equals(b) : false);
        }
    }

    /**
     * Checks one menu button each time per once
     *
     * @param tab
     */
    public <T extends TabPane> void toggleMenuOption(T tab) {
        toggleMenuOption(tab != null ? tab.getLinkedButton() : null);
    }

    /**
     * Handles session user interface logic
     */
    public void handleSessionUI() {
        if (this.session != null && this.session.isValid()) {
            this.logoutButton.setIcon(getIcon(TIMES, 14, WHITE));
            this.logoutButton.setText("Cerrar sesión");
            this.logoutButton.setBackground(new Color(200, 51, 51));
            this.fullnameLabel.setText(this.session.getEntity().getFirstName() + " "
                    + this.session.getEntity().getLastName());
            this.usernameLabel.setText(this.session.getEntity().getUser().getUsername());
            this.iconLabel.setIcon(getIcon(USER_CIRCLE, 28, LIGHT_GRAY));
        } else {
            this.mainTabbedPane.removeAll();
            this.logoutButton.setIcon(getIcon(LOCK, 14, WHITE));
            this.logoutButton.setText("Iniciar sesión");
            this.logoutButton.setBackground(new Color(0, 153, 0));
            this.fullnameLabel.setText(null);
            this.usernameLabel.setText(null);
            this.iconLabel.setIcon(null);
        }
        connectionLabel.setText(connected ? "Conectado" : "Sin conexión");
        connectionLabel.setIcon(getIcon(CIRCLE, 14,
                connected ? new Color(0, 153, 0) : new Color(200, 51, 51)));
        enableMenu(this.session != null && this.session.isValid() && connected);
        if (!connected) {
            this.mainTabbedPane.removeAll();
        }
    }

    /**
     * Schedule connection status and display to status bar
     */
    public void checkConnectionStatus() {
        TimerTask repeatedTask;
        repeatedTask = new TimerTask() {
            @Override
            public void run() {
                boolean currentStatus = isValidConfig();
                if (currentStatus != connected) {
                    connected = currentStatus;
                    handleSessionUI();
                }
            }
        };
        Timer timer = new Timer("Timer");
        long delay = 0L;
        long period = 1000L;
        timer.scheduleAtFixedRate(repeatedTask, delay, period);
    }

    /**
     * Perform action when a menu button is clicked
     *
     * @param evt
     * @param type
     */
    private <T extends TabPane> void menuActionButtonPerformed(ActionEvent evt, Class<T> type) {
        try {
            JToggleButton source = (JToggleButton) evt.getSource();
            TabPane tab = getOpenedPane(type);
            // Tab bean factory
            if (tab == null) {
                tab = type.getDeclaredConstructor(javax.swing.JFrame.class, JToggleButton.class)
                        .newInstance(this, source);
                this.mainTabbedPane.addTab(source.getText(), source.getIcon(), tab);
            }
            this.mainTabbedPane.setSelectedComponent(tab);
        } catch (Exception ex) {
            getLogger(this.getClass()).error(ex.getMessage(), ex);
        }
    }

    public <T extends TabPane> T getOpenedPane(Class<T> type) {
        for (Component c : this.mainTabbedPane.getComponents()) {
            if (type.isInstance(c)) {
                return (T) c;
            }
        }
        return null;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        mainPane = new javax.swing.JSplitPane();
        mainToolbar = new javax.swing.JToolBar();
        sessionCard = new javax.swing.JPanel();
        iconLabel = new javax.swing.JLabel();
        fullnameLabel = new javax.swing.JLabel();
        usernameLabel = new javax.swing.JLabel();
        ticketsMenuButton = new javax.swing.JToggleButton();
        pricesMenuButton = new javax.swing.JToggleButton();
        vehiclesMenuButton = new javax.swing.JToggleButton();
        ownersMenuButton = new javax.swing.JToggleButton();
        parklotMenuButton = new javax.swing.JToggleButton();
        usersMenuButton = new javax.swing.JToggleButton();
        rolesMenuButton = new javax.swing.JToggleButton();
        reportsMenuButton = new javax.swing.JToggleButton();
        logoutGlue = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 32767));
        logoutButton = new javax.swing.JButton();
        mainTabbedPane = new parqueadero.view.component.ClosableTabbedPane();
        statusbarPanel = new javax.swing.JPanel();
        connectionLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("AVENTI Parking Manager");
        setExtendedState(javax.swing.JFrame.MAXIMIZED_BOTH);
        setIconImage(Main.APP_IMAGE_ICON);
        setSize(new java.awt.Dimension(400, 300));

        mainPane.setDividerLocation(200);
        mainPane.setEnabled(false);

        mainToolbar.setFloatable(false);
        mainToolbar.setOrientation(javax.swing.SwingConstants.VERTICAL);

        sessionCard.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        sessionCard.setEnabled(false);
        sessionCard.setMaximumSize(new java.awt.Dimension(32767, 48));
        sessionCard.setMinimumSize(new java.awt.Dimension(0, 48));
        sessionCard.setPreferredSize(new java.awt.Dimension(200, 48));
        sessionCard.setLayout(new java.awt.GridBagLayout());

        iconLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        iconLabel.setMaximumSize(new java.awt.Dimension(48, 48));
        iconLabel.setMinimumSize(new java.awt.Dimension(48, 48));
        iconLabel.setOpaque(true);
        iconLabel.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        sessionCard.add(iconLabel, gridBagConstraints);

        fullnameLabel.setFont(fullnameLabel.getFont().deriveFont(fullnameLabel.getFont().getStyle() | java.awt.Font.BOLD));
        fullnameLabel.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        fullnameLabel.setOpaque(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        sessionCard.add(fullnameLabel, gridBagConstraints);

        usernameLabel.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        usernameLabel.setOpaque(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        sessionCard.add(usernameLabel, gridBagConstraints);

        mainToolbar.add(sessionCard);

        ticketsMenuButton.setIcon(Icon.getIcon(FontAwesome.ID_CARD_O, 14, ticketsMenuButton.getForeground()));
        ticketsMenuButton.setText("Tickets");
        ticketsMenuButton.setDoubleBuffered(true);
        ticketsMenuButton.setEnabled(false);
        ticketsMenuButton.setFocusable(false);
        ticketsMenuButton.setHorizontalAlignment(javax.swing.SwingConstants.LEADING);
        ticketsMenuButton.setIconTextGap(10);
        ticketsMenuButton.setMargin(new java.awt.Insets(10, 10, 10, 10));
        ticketsMenuButton.setMaximumSize(new java.awt.Dimension(200, 30));
        ticketsMenuButton.setMinimumSize(new java.awt.Dimension(200, 30));
        ticketsMenuButton.setOpaque(true);
        ticketsMenuButton.setPreferredSize(new java.awt.Dimension(200, 30));
        ticketsMenuButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ticketsMenuButtonActionPerformed(evt);
            }
        });
        mainToolbar.add(ticketsMenuButton);

        pricesMenuButton.setIcon(Icon.getIcon(FontAwesome.MONEY, 14, ticketsMenuButton.getForeground()));
        pricesMenuButton.setText("Tarifas");
        pricesMenuButton.setDoubleBuffered(true);
        pricesMenuButton.setEnabled(false);
        pricesMenuButton.setFocusable(false);
        pricesMenuButton.setHorizontalAlignment(javax.swing.SwingConstants.LEADING);
        pricesMenuButton.setIconTextGap(10);
        pricesMenuButton.setMargin(new java.awt.Insets(10, 10, 10, 10));
        pricesMenuButton.setMaximumSize(new java.awt.Dimension(200, 30));
        pricesMenuButton.setMinimumSize(new java.awt.Dimension(200, 30));
        pricesMenuButton.setOpaque(true);
        pricesMenuButton.setPreferredSize(new java.awt.Dimension(200, 30));
        pricesMenuButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        pricesMenuButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pricesMenuButtonActionPerformed(evt);
            }
        });
        mainToolbar.add(pricesMenuButton);

        vehiclesMenuButton.setIcon(Icon.getIcon(FontAwesome.CAR, 14, vehiclesMenuButton.getForeground()));
        vehiclesMenuButton.setText("Vehículos");
        vehiclesMenuButton.setDoubleBuffered(true);
        vehiclesMenuButton.setEnabled(false);
        vehiclesMenuButton.setFocusable(false);
        vehiclesMenuButton.setHorizontalAlignment(javax.swing.SwingConstants.LEADING);
        vehiclesMenuButton.setIconTextGap(10);
        vehiclesMenuButton.setMargin(new java.awt.Insets(10, 10, 10, 10));
        vehiclesMenuButton.setMaximumSize(new java.awt.Dimension(200, 30));
        vehiclesMenuButton.setMinimumSize(new java.awt.Dimension(200, 30));
        vehiclesMenuButton.setOpaque(true);
        vehiclesMenuButton.setPreferredSize(new java.awt.Dimension(200, 30));
        vehiclesMenuButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                vehiclesMenuButtonActionPerformed(evt);
            }
        });
        mainToolbar.add(vehiclesMenuButton);

        ownersMenuButton.setIcon(Icon.getIcon(FontAwesome.USERS, 14, ownersMenuButton.getForeground()));
        ownersMenuButton.setText("Clientes");
        ownersMenuButton.setDoubleBuffered(true);
        ownersMenuButton.setEnabled(false);
        ownersMenuButton.setFocusable(false);
        ownersMenuButton.setHorizontalAlignment(javax.swing.SwingConstants.LEADING);
        ownersMenuButton.setIconTextGap(10);
        ownersMenuButton.setMargin(new java.awt.Insets(10, 10, 10, 10));
        ownersMenuButton.setMaximumSize(new java.awt.Dimension(200, 30));
        ownersMenuButton.setMinimumSize(new java.awt.Dimension(200, 30));
        ownersMenuButton.setOpaque(true);
        ownersMenuButton.setPreferredSize(new java.awt.Dimension(200, 30));
        ownersMenuButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ownersMenuButtonActionPerformed(evt);
            }
        });
        mainToolbar.add(ownersMenuButton);

        parklotMenuButton.setIcon(Icon.getIcon(FontAwesome.SERVER, 14, parklotMenuButton.getForeground()));
        parklotMenuButton.setText("Parqueadero");
        parklotMenuButton.setDoubleBuffered(true);
        parklotMenuButton.setEnabled(false);
        parklotMenuButton.setFocusable(false);
        parklotMenuButton.setHorizontalAlignment(javax.swing.SwingConstants.LEADING);
        parklotMenuButton.setIconTextGap(10);
        parklotMenuButton.setMargin(new java.awt.Insets(10, 10, 10, 10));
        parklotMenuButton.setMaximumSize(new java.awt.Dimension(200, 30));
        parklotMenuButton.setMinimumSize(new java.awt.Dimension(200, 30));
        parklotMenuButton.setOpaque(true);
        parklotMenuButton.setPreferredSize(new java.awt.Dimension(200, 30));
        parklotMenuButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                parklotMenuButtonActionPerformed(evt);
            }
        });
        mainToolbar.add(parklotMenuButton);

        usersMenuButton.setIcon(Icon.getIcon(FontAwesome.USERS, 14, usersMenuButton.getForeground()));
        usersMenuButton.setText("Usuarios");
        usersMenuButton.setDoubleBuffered(true);
        usersMenuButton.setEnabled(false);
        usersMenuButton.setFocusable(false);
        usersMenuButton.setHorizontalAlignment(javax.swing.SwingConstants.LEADING);
        usersMenuButton.setIconTextGap(10);
        usersMenuButton.setMargin(new java.awt.Insets(10, 10, 10, 10));
        usersMenuButton.setMaximumSize(new java.awt.Dimension(200, 30));
        usersMenuButton.setMinimumSize(new java.awt.Dimension(200, 30));
        usersMenuButton.setOpaque(true);
        usersMenuButton.setPreferredSize(new java.awt.Dimension(200, 30));
        usersMenuButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                usersMenuButtonActionPerformed(evt);
            }
        });
        mainToolbar.add(usersMenuButton);

        rolesMenuButton.setIcon(Icon.getIcon(FontAwesome.KEY, 14, rolesMenuButton.getForeground()));
        rolesMenuButton.setText("Control de Acceso");
        rolesMenuButton.setDoubleBuffered(true);
        rolesMenuButton.setEnabled(false);
        rolesMenuButton.setFocusable(false);
        rolesMenuButton.setHorizontalAlignment(javax.swing.SwingConstants.LEADING);
        rolesMenuButton.setIconTextGap(10);
        rolesMenuButton.setMargin(new java.awt.Insets(10, 10, 10, 10));
        rolesMenuButton.setMaximumSize(new java.awt.Dimension(200, 30));
        rolesMenuButton.setMinimumSize(new java.awt.Dimension(200, 30));
        rolesMenuButton.setOpaque(true);
        rolesMenuButton.setPreferredSize(new java.awt.Dimension(200, 30));
        rolesMenuButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rolesMenuButtonActionPerformed(evt);
            }
        });
        mainToolbar.add(rolesMenuButton);

        reportsMenuButton.setIcon(Icon.getIcon(FontAwesome.BAR_CHART, 14, reportsMenuButton.getForeground()));
        reportsMenuButton.setText("Reportes");
        reportsMenuButton.setDoubleBuffered(true);
        reportsMenuButton.setEnabled(false);
        reportsMenuButton.setFocusable(false);
        reportsMenuButton.setHorizontalAlignment(javax.swing.SwingConstants.LEADING);
        reportsMenuButton.setIconTextGap(10);
        reportsMenuButton.setMargin(new java.awt.Insets(10, 10, 10, 10));
        reportsMenuButton.setMaximumSize(new java.awt.Dimension(200, 30));
        reportsMenuButton.setMinimumSize(new java.awt.Dimension(200, 30));
        reportsMenuButton.setOpaque(true);
        reportsMenuButton.setPreferredSize(new java.awt.Dimension(200, 30));
        reportsMenuButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reportsMenuButtonActionPerformed(evt);
            }
        });
        mainToolbar.add(reportsMenuButton);
        mainToolbar.add(logoutGlue);

        logoutButton.setBackground(new java.awt.Color(0, 153, 0));
        logoutButton.setForeground(new java.awt.Color(255, 255, 255));
        logoutButton.setIcon(Icon.getIcon(FontAwesome.LOCK, 14, new Color(255, 255, 255)));
        logoutButton.setText("Iniciar sesión");
        logoutButton.setDoubleBuffered(true);
        logoutButton.setFocusable(false);
        logoutButton.setHorizontalAlignment(javax.swing.SwingConstants.LEADING);
        logoutButton.setMargin(new java.awt.Insets(10, 10, 10, 10));
        logoutButton.setMaximumSize(new java.awt.Dimension(200, 30));
        logoutButton.setMinimumSize(new java.awt.Dimension(200, 30));
        logoutButton.setOpaque(true);
        logoutButton.setPreferredSize(new java.awt.Dimension(200, 30));
        logoutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logoutButtonActionPerformed(evt);
            }
        });
        mainToolbar.add(logoutButton);

        mainPane.setLeftComponent(mainToolbar);
        mainToolbar.getAccessibleContext().setAccessibleParent(this);

        mainTabbedPane.setAutoscrolls(true);
        mainTabbedPane.setOpaque(true);
        mainPane.setRightComponent(mainTabbedPane);

        getContentPane().add(mainPane, java.awt.BorderLayout.CENTER);

        statusbarPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        statusbarPanel.setMaximumSize(new java.awt.Dimension(2147483647, 24));
        statusbarPanel.setMinimumSize(new java.awt.Dimension(104, 24));
        statusbarPanel.setPreferredSize(new java.awt.Dimension(561, 24));
        statusbarPanel.setLayout(new java.awt.BorderLayout());

        connectionLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        connectionLabel.setText("Sin conexión");
        connectionLabel.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
        connectionLabel.setMaximumSize(new java.awt.Dimension(100, 24));
        connectionLabel.setMinimumSize(new java.awt.Dimension(100, 24));
        connectionLabel.setPreferredSize(new java.awt.Dimension(100, 24));
        statusbarPanel.add(connectionLabel, java.awt.BorderLayout.EAST);

        getContentPane().add(statusbarPanel, java.awt.BorderLayout.PAGE_END);

        setSize(new java.awt.Dimension(674, 442));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void usersMenuButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_usersMenuButtonActionPerformed
        this.menuActionButtonPerformed(evt, UsersTabPane.class);
    }//GEN-LAST:event_usersMenuButtonActionPerformed

    private void ownersMenuButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ownersMenuButtonActionPerformed
        this.menuActionButtonPerformed(evt, OwnersTabPane.class);
    }//GEN-LAST:event_ownersMenuButtonActionPerformed

    private void vehiclesMenuButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_vehiclesMenuButtonActionPerformed
        this.menuActionButtonPerformed(evt, VehiclesTabPane.class);
    }//GEN-LAST:event_vehiclesMenuButtonActionPerformed

    private void ticketsMenuButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ticketsMenuButtonActionPerformed
        this.menuActionButtonPerformed(evt, TicketsTabPane.class);
    }//GEN-LAST:event_ticketsMenuButtonActionPerformed

    private void parklotMenuButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_parklotMenuButtonActionPerformed
        this.menuActionButtonPerformed(evt, ParkingLotsTabPane.class);
    }//GEN-LAST:event_parklotMenuButtonActionPerformed

    private void logoutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_logoutButtonActionPerformed
        boolean hasToLogin = false;
        if (this.session != null && this.session.isValid()) {
            if (this.originSession != null && this.originSession.isValid()) {
                this.session = this.originSession;
                this.originSession = null;
                this.mainTabbedPane.removeAll();
            } else {
                hasToLogin = true;
                this.session = null;
            }
        } else {
            hasToLogin = true;
        }
        this.handleSessionUI();
        if (hasToLogin) {
            LoginDialog loginDialog = new LoginDialog(this, true);
            loginDialog.setVisible(true);
        }
    }//GEN-LAST:event_logoutButtonActionPerformed

    private void pricesMenuButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pricesMenuButtonActionPerformed
        this.menuActionButtonPerformed(evt, PricesTabPane.class);
    }//GEN-LAST:event_pricesMenuButtonActionPerformed

    private void rolesMenuButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rolesMenuButtonActionPerformed
        this.menuActionButtonPerformed(evt, RolesTabPane.class);
    }//GEN-LAST:event_rolesMenuButtonActionPerformed

    private void reportsMenuButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reportsMenuButtonActionPerformed
        this.menuActionButtonPerformed(evt, ReportsTabPane.class);
    }//GEN-LAST:event_reportsMenuButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel connectionLabel;
    private javax.swing.JLabel fullnameLabel;
    private javax.swing.JLabel iconLabel;
    private javax.swing.JButton logoutButton;
    private javax.swing.Box.Filler logoutGlue;
    private javax.swing.JSplitPane mainPane;
    private javax.swing.JTabbedPane mainTabbedPane;
    private javax.swing.JToolBar mainToolbar;
    private javax.swing.JToggleButton ownersMenuButton;
    private javax.swing.JToggleButton parklotMenuButton;
    private javax.swing.JToggleButton pricesMenuButton;
    private javax.swing.JToggleButton reportsMenuButton;
    private javax.swing.JToggleButton rolesMenuButton;
    private javax.swing.JPanel sessionCard;
    private javax.swing.JPanel statusbarPanel;
    private javax.swing.JToggleButton ticketsMenuButton;
    private javax.swing.JLabel usernameLabel;
    private javax.swing.JToggleButton usersMenuButton;
    private javax.swing.JToggleButton vehiclesMenuButton;
    // End of variables declaration//GEN-END:variables

    public void impersonate(Entity entity) {
        this.originSession = this.session;
        this.session = new Session(entity, this.originSession.getLoggedIn());
        this.mainTabbedPane.removeAll();
        this.handleSessionUI();
    }
}
