/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.view.component;

import java.util.function.Function;
import java.util.stream.Collectors;
import javax.swing.JList;
import javax.swing.ListModel;
import parqueadero.util.AbstractModel;

/**
 * List class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 * @param <T>
 */
public class List<T extends AbstractModel> extends JList<T> {

    private Function<T, Integer> getIdCallable;
    private Function<T, String> getLabelCallable;

    public List(Function<T, String> getLabelCallable, Function<T, Integer> getIdCallable, ListModel<T> dataModel) {
        super(dataModel);
        initComponents(getLabelCallable, getIdCallable);
    }

    public List(Function<T, String> getLabelCallable, Function<T, Integer> getIdCallable, T[] listData) {
        super(listData);
        initComponents(getLabelCallable, getIdCallable);
    }

    public List(Function<T, String> getLabelCallable, Function<T, Integer> getIdCallable) {
        super();
        initComponents(getLabelCallable, getIdCallable);
    }

    public List(Function<T, String> getLabelCallable) {
        this(getLabelCallable, (T el) -> el.getId() == null ? null : el.getId().intValue());
    }

    public List() {
        this((el) -> el.getId() == null ? "" : el.getId().toPlainString());
    }

    public Function<T, Integer> getGetIdCallable() {
        return getIdCallable;
    }

    public Function<T, String> getGetLabelCallable() {
        return getLabelCallable;
    }

    public Integer getSelectedId() {
        return this.getIdCallable.apply(super.getSelectedValue());
    }

    public java.util.Collection<Integer> getSelectedIdsList() {
        return getSelectedValuesList().stream()
                .map(this.getIdCallable)
                .collect(Collectors.toList());
    }

    public java.util.Collection<Integer> getIdsList() {
        java.util.Collection<Integer> result = new java.util.ArrayList();
        for (T e : getValuesList()) {
            result.add(this.getIdCallable.apply(e));
        }
        return result;
    }

    public String getSelectedLabel() {
        return this.getLabelCallable.apply(super.getSelectedValue());
    }

    public java.util.Collection<String> getSelectedLabelsList() {
        return getSelectedValuesList().stream()
                .map(this.getLabelCallable)
                .collect(Collectors.toList());
    }

    public java.util.Collection<String> getLabelsList() {
        java.util.Collection<String> result = new java.util.ArrayList();
        for (T e : getValuesList()) {
            result.add(this.getLabelCallable.apply(e));
        }
        return result;
    }
    @Override
    public T getSelectedValue() {
        return super.getSelectedValue();
    }

    @Override
    public T[] getSelectedValues() {
        return (T[]) super.getSelectedValues();
    }

    @Override
    public java.util.List<T> getSelectedValuesList() {
        return super.getSelectedValuesList();
    }

    public java.util.Collection<T> getValuesList() {
        java.util.Collection<T> result = new java.util.ArrayList();
        for (int i = 0; i < getModel().getSize(); i++) {
            result.add(getModel().getElementAt(i));
        }
        return result;
    }

    private void initComponents(Function<T, String> getLabelCallable, Function<T, Integer> getIdCallable) {
        this.getLabelCallable = getLabelCallable;
        this.getIdCallable = getIdCallable;
        this.setModel(new SortedListModel<T>(this.getLabelCallable));
        this.setCellRenderer(new ListCellRenderer<T>());
    }
}
