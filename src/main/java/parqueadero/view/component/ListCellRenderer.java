/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.view.component;

import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import parqueadero.util.AbstractModel;

/**
 * ListCellRenderer class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class ListCellRenderer<T extends AbstractModel> extends JLabel implements javax.swing.ListCellRenderer<T> {

    public ListCellRenderer() {
        setOpaque(true);
    }

    @Override
    public Component getListCellRendererComponent(JList<? extends T> list, T model, int index, boolean isSelected, boolean hasFocus) {
        setText((String) ((List) list).getGetLabelCallable().apply(model));
        
        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }

        return this;
    }
}
