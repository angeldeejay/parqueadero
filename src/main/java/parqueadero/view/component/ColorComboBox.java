/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.view.component;

import java.awt.Color;
import java.awt.Component;
import javax.swing.*;
import jiconfont.icons.font_awesome.FontAwesome;
import parqueadero.model.Vehicle;
import parqueadero.util.Icon;

/**
 * ColorComboBox class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class ColorComboBox extends JComboBox {

    public ColorComboBox() {
        super();
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        Vehicle.AVAILABLE_COLORS.forEach((String k, Color v) -> {
            model.addElement(k);
        });
        setModel(model);
        setRenderer(new ColorRenderer());
        this.setOpaque(true);
        this.setSelectedIndex(0);
    }

    public Color getSelectedColor() {
        return Vehicle.AVAILABLE_COLORS.get(getSelectedItem());
    }

    class ColorRenderer extends javax.swing.DefaultListCellRenderer {

        public ColorRenderer() {
            this.setOpaque(true);
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object key, int index,
                boolean isSelected, boolean cellHasFocus) {
            JLabel c = (JLabel) super.getListCellRendererComponent(list, key, index, isSelected, cellHasFocus);
            Color color = key == null ? Color.WHITE : Vehicle.AVAILABLE_COLORS.get(key);
            c.setText(String.valueOf(key));
            c.setIcon(Icon.getIcon(FontAwesome.CIRCLE, 16, color));
            c.setBorder(BorderFactory.createEmptyBorder(3, 5, 3, 5));
            return c;
        }
    }
}
