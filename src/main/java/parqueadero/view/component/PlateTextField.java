/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.view.component;

import java.awt.Color;
import java.awt.event.KeyEvent;
import static java.lang.Character.isDigit;
import static java.lang.Character.isLetter;
import static java.lang.Long.valueOf;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.text.MaskFormatter;
import parqueadero.view.form.VehicleForm;

/**
 * NumberTextField class
 *
 * some code comes from https://gist.github.com/gysel/4074617
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class PlateTextField extends javax.swing.JTextField {

    public PlateTextField() {
        super();
    }

    @Override
    protected void processKeyEvent(KeyEvent e) {
        if (this.getText().length() <= 6 && isDigit(e.getKeyChar()) && isLetter(e.getKeyChar())) {
            super.processKeyEvent(e);
        }
        e.consume();
    }

    /**
     * As the user is not even able to enter a dot ("."), only integers (whole
     * numbers) may be entered.
     */
    public Long getNumber() {
        Long result = null;
        String text = getText();
        if (text != null && !"".equals(text)) {
            result = valueOf(text);
        }
        return result;
    }

    class PlateInputVerifier extends InputVerifier {

        private final Color INVALID_COLOR = Color.RED;
        private final Color VALID_COLOR = Color.BLACK;
        private MaskFormatter mf = null;

        public PlateInputVerifier() {
        }

        /**
         * Mask setter
         *
         * @param mask
         */
        public void setMask(String mask) {
            try {
                mf = new MaskFormatter(mask);
            } catch (ParseException ex) {
                Logger.getLogger(VehicleForm.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        @Override
        public boolean verify(JComponent c) {
            try {
                JFormattedTextField ftf = (JFormattedTextField) c;
                String value = (String) mf.stringToValue(ftf.getText());
                c.setForeground(VALID_COLOR);
                return true;
            } catch (Exception e) {
                c.setForeground(INVALID_COLOR);
                return false;
            }

        }
    }
}
