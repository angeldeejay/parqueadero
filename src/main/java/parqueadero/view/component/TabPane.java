/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.view.component;

import java.awt.Component;
import java.awt.LayoutManager;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JToggleButton;

/**
 * TabPanel class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class TabPane extends JPanel {

    protected final JFrame rootParent;
    protected final JToggleButton linkedButton;


    public <T extends JFrame> TabPane(final T rootParent, final JToggleButton linkedButton, LayoutManager layout, boolean isDoubleBuffered) {
        super(layout, isDoubleBuffered);
        this.rootParent = rootParent;
        this.linkedButton = linkedButton;
    }

    public <T extends JFrame> TabPane(final T rootParent, final JToggleButton linkedButton, LayoutManager layout) {
        super(layout);
        this.rootParent = rootParent;
        this.linkedButton = linkedButton;
    }

    public <T extends JFrame> TabPane(final T rootParent, final JToggleButton linkedButton, boolean isDoubleBuffered) {
        super(isDoubleBuffered);
        this.rootParent = rootParent;
        this.linkedButton = linkedButton;
    }

    public <T extends JFrame> TabPane(final T rootParent, final JToggleButton linkedButton) {
        this.rootParent = rootParent;
        this.linkedButton = linkedButton;
    }
    public <T extends Component> T getComponentByName(Class<T> type, String name) {
        for (java.lang.reflect.Field field : this.getClass().getDeclaredFields()) {
            try {
                field.setAccessible(true);
                if (name.equals(field.getName())) {
                    final Object potentialMatch = field.get(this);
                    return (T) potentialMatch;
                }
            } catch (SecurityException | IllegalArgumentException | IllegalAccessException ex) {
            }
        }
        return null;
    }

    public JToggleButton getLinkedButton() {
        return linkedButton;
    }

    public <T extends JFrame> T getRootParent() {
        return (T) rootParent;
    }
}
