/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.view.component;

import java.awt.Component;
import java.util.Collections;
import java.util.Enumeration;
import javax.swing.*;
import jiconfont.icons.font_awesome.FontAwesome;
import parqueadero.model.Vehicle;
import parqueadero.util.Icon;

/**
 * ColorComboBox class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class VehicleTypeComboBox extends JComboBox {

    private boolean nullable;

    public VehicleTypeComboBox(boolean nullable) {
        super();
        this.nullable = nullable;
        DefaultComboBoxModel<String> model = new DefaultComboBoxModel();
        if (this.nullable) {
            model.addElement("Cualquiera");
        }
        for (String k : Collections.list((Enumeration<String>) Vehicle.AVAILABLE_TYPES.keys())) {
            model.addElement(k);
        };
        setModel(model);
        setRenderer(new VehicleRenderer());
        this.setOpaque(true);
        this.setSelectedIndex(-1);
    }

    public VehicleTypeComboBox() {
        this(false);
    }

    public void setSelectedType(String type) {
        for (String k : Collections.list((Enumeration<String>) Vehicle.AVAILABLE_TYPES.keys())) {
            String v = Vehicle.AVAILABLE_TYPES.get(k);
            if (v.equalsIgnoreCase(type)) {
                this.setSelectedItem(k);
                return;
            }
        }
        this.setSelectedItem(null);
    }

    public String getSelectedType() {
        if (super.getSelectedIndex() >= 0) {
            return Vehicle.AVAILABLE_TYPES.get(super.getSelectedItem());
        }
        return null;
    }

    class VehicleRenderer extends javax.swing.DefaultListCellRenderer {

        public VehicleRenderer() {
            this.setOpaque(true);
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object key, int index,
                boolean isSelected, boolean cellHasFocus) {
            JLabel c = (JLabel) super.getListCellRendererComponent(list, key, index, isSelected, cellHasFocus);
            if (key != null) {
                c.setText(key.toString());
                String type = Vehicle.AVAILABLE_TYPES.get(key);
                if (type != null) {
                    switch (type) {
                        case Vehicle.BIKE:
                            c.setIcon(Icon.getIcon(FontAwesome.BICYCLE, 16));
                            break;
                        case Vehicle.CAR:
                            c.setIcon(Icon.getIcon(FontAwesome.CAR, 16));
                            break;
                        case Vehicle.MOTORCYCLE:
                            c.setIcon(Icon.getIcon(FontAwesome.MOTORCYCLE, 16));
                            break;
                    }
                } else {
                    c.setIcon(Icon.getIcon(FontAwesome.ASTERISK, 16));
                }
            }
            c.setBorder(BorderFactory.createEmptyBorder(3, 5, 3, 5));
            return c;
        }
    }
}
