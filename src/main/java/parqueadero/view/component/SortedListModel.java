/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.view.component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.swing.AbstractListModel;

/**
 * SortedListModel class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class SortedListModel<E> extends AbstractListModel<E> {

    private Function<E, String> comparator;
    private Collection<E> list;

    public SortedListModel(Function<E, String> comparator) {
        list = new ArrayList<>();
        this.comparator = comparator;
    }

    public void add(E element) {
        if (list.add(element)) {
            sortElements();
            fireContentsChanged(this, 0, getSize());
        }
    }

    public void addAll(java.util.Collection<E> elements) {
        list.addAll(elements);
        sortElements();
        fireContentsChanged(this, 0, getSize());
    }

    public void clear() {
        list.clear();
        fireContentsChanged(this, 0, getSize());
    }

    public boolean contains(E element) {
        return list.contains(element);
    }

    public E firstElement() {
        return getElementAt(0);
    }

    public Collection<E> getAll() {
        return this.list;
    }

    @Override
    public E getElementAt(int index) {
        return ((java.util.List<E>) list).get(index);
    }

    @Override
    public int getSize() {
        return list.size();
    }

    public Iterator iterator() {
        return list.iterator();
    }

    public E lastElement() {
        int size = getSize();
        return size > 0 ? getElementAt(size - 1) : null;
    }

    public boolean removeElement(E element) {
        boolean removed = list.remove(element);
        if (removed) {
            sortElements();
            fireContentsChanged(this, 0, getSize());
        }
        return removed;
    }

    private void sortElements() {
        Collections.sort((java.util.List) list, (E o1, E o2) -> comparator.apply(o1).compareToIgnoreCase(comparator.apply(o2)));
    }
}
