/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.view.component;

import java.awt.event.KeyEvent;
import static java.lang.Character.isDigit;
import static java.lang.Long.valueOf;

/**
 * NumberTextField class
 *
 * some code comes from https://gist.github.com/gysel/4074617
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class NumberTextField extends javax.swing.JTextField {

    public NumberTextField() {
        super();
    }

    @Override
    protected void processKeyEvent(KeyEvent e) {
        if (isDigit(e.getKeyChar())
                || e.getKeyCode() == KeyEvent.VK_ENTER
                || e.getKeyCode() == KeyEvent.VK_BACK_SPACE
                || e.getKeyCode() == KeyEvent.VK_DELETE
                || e.getKeyCode() == KeyEvent.VK_LEFT
                || e.getKeyCode() == KeyEvent.VK_RIGHT) {
            super.processKeyEvent(e);
        }
        e.consume();
    }

    /**
     * As the user is not even able to enter a dot ("."), only integers (whole
     * numbers) may be entered.
     */
    public Long getNumber() {
        Long result = null;
        String text = getText();
        if (text != null && !"".equals(text)) {
            result = valueOf(text);
        }
        return result;
    }
}
