/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.view.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.function.BiFunction;
import static javax.swing.BorderFactory.createEmptyBorder;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JViewport;
import static javax.swing.ListSelectionModel.SINGLE_SELECTION;
import javax.swing.table.TableColumnModel;

/**
 * Table class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class Table extends JTable {

    private final static Color LOCKED_BACKGROUND = new Color(200, 128, 128);
    private final static Color LOCKED_SELECTION_BACKGROUND = new Color(255, 50, 50);
    private final static Color HIGHLIGHTED_BACKGROUND = new Color(128, 200, 128);
    private final static Color HIGHLIGHTED_SELECTION_BACKGROUND = new Color(50, 200, 50);
    private BiFunction<Integer, Integer, Boolean> disabledValidator;
    private BiFunction<Integer, Integer, Boolean> hightlightedValidator;
    private final Color fg = getForeground();
    private final Color bg = getBackground();
    private final Color sfg = getSelectionForeground();
    private final Color sbg = getSelectionBackground();

    public Table() {
        super();
        this.disabledValidator = (Integer _c, Integer _r) -> false;
        this.hightlightedValidator = (Integer _c, Integer _r) -> false;
    }

    public void setDisabledValidator(BiFunction<Integer, Integer, Boolean> lv) {
        this.disabledValidator = lv;
    }

    public void setHightlightedValidator(BiFunction<Integer, Integer, Boolean> hightlightedValidator) {
        this.hightlightedValidator = hightlightedValidator;
    }

    @Override
    public Component prepareRenderer(javax.swing.table.TableCellRenderer renderer, int row, int column) {
        Component c = super.prepareRenderer(renderer, row, column);
        boolean disabled = this.disabledValidator.apply(row, column);
        boolean highlighted = this.hightlightedValidator.apply(row, column);
        if (row == this.getSelectedRow()) {
            c.setBackground(disabled ? LOCKED_SELECTION_BACKGROUND : (highlighted ? HIGHLIGHTED_SELECTION_BACKGROUND : sbg));
            c.setForeground(disabled || highlighted ? Color.WHITE : sfg);
        } else {
            c.setBackground(disabled ? LOCKED_BACKGROUND : (highlighted ? HIGHLIGHTED_BACKGROUND : bg));
            c.setForeground(disabled || highlighted ? Color.WHITE : fg);
        }
        return c;
    }

    public void setupDefaults() {
        setAutoCreateRowSorter(false);
        setShowGrid(true);
        setBorder(createEmptyBorder(5, 5, 5, 5));
        setGridColor(new java.awt.Color(204, 204, 204));
        setRowSelectionAllowed(true);
        setSelectionMode(SINGLE_SELECTION);
        getTableHeader().setReorderingAllowed(false);
        setUpdateSelectionOnSort(false);
        TableColumnModel cModel = getColumnModel();
        cModel.getSelectionModel().setSelectionMode(SINGLE_SELECTION);
        for (int i = 0; i < getColumnCount(); i++) {
            int w = i == 0 ? 50 : 120;
            cModel.getColumn(i).setPreferredWidth(w);
            cModel.getColumn(i).setMinWidth(w);
            cModel.getColumn(i).setWidth(w);
            cModel.getColumn(i).setResizable(false);
        }
        // enable scrolling
        ((JViewport) getParent()).addComponentListener(new TableComponentAdapter(this));
    }

    public void proxyRowDoubleClickTo(JButton c) {
        this.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    c.doClick();
                }
            }
        });
    }

    class TableComponentAdapter extends ComponentAdapter {

        private final JTable table;

        public TableComponentAdapter(JTable table) {
            super();
            this.table = table;
        }

        @Override
        public void componentResized(final ComponentEvent e) {
            if (table.getPreferredSize().width < table.getParent().getWidth()) {
                table.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
            } else {
                table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            }
        }
    }
}
