/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.view.component;

import java.awt.*;
import static java.awt.Color.BLACK;
import static java.awt.Cursor.HAND_CURSOR;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.*;
import static javax.swing.BorderFactory.createEmptyBorder;
import static jiconfont.icons.font_awesome.FontAwesome.TIMES;
import static parqueadero.util.Icon.getIcon;

/**
 * UsersTabPane class
 *
 * Some code comes from https://gist.github.com/6dc/0c8926f85d701a869bb2
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class ClosableTabbedPane extends JTabbedPane {
    
    public ClosableTabbedPane() {
        super();
    }

    /**
     * Override Addtab in order to add the close Button everytime
     */
    @Override
    public void addTab(String title, Icon icon, Component component, String tip) {
        super.addTab(title, icon, component, tip);
        int count = this.getTabCount() - 1;
        setTabComponentAt(count, new CloseButtonTab(component, title, icon));
    }

    @Override
    public void addTab(String title, Icon icon, Component component) {
        addTab(title, icon, component, null);
    }

    @Override
    public void addTab(String title, Component component) {
        addTab(title, null, component);
    }

    /* addTabNoExit */
    public void addTabNoExit(String title, Icon icon, Component component, String tip) {
        super.addTab(title, icon, component, tip);
    }

    public void addTabNoExit(String title, Icon icon, Component component) {
        addTabNoExit(title, icon, component, null);
    }

    public void addTabNoExit(String title, Component component) {
        addTabNoExit(title, null, component);
    }

    /**
     * Close button class
     */
    class CloseButtonTab extends JPanel {

        private final Component tab;

        public CloseButtonTab(final Component tab, String title, Icon icon) {
            this.tab = tab;
            setOpaque(false);
            // Label
            JLabel tabTitleLabel = new JLabel(title, icon, TRAILING);
            tabTitleLabel.setBorder(createEmptyBorder(3, 0, 0, 10));
            // Close button
            JButton closeButton = new JButton();
            closeButton.setIcon(getIcon(TIMES, 14, BLACK));
            closeButton.setMargin(new Insets(0, 0, 0, 0));
            closeButton.setBorder(createEmptyBorder(3, 0, 0, 0));
            closeButton.setContentAreaFilled(false);
            closeButton.setBorderPainted(false);
            closeButton.setFocusPainted(false);
            closeButton.addMouseListener(new CloseListener(tab));
            closeButton.setCursor(new Cursor(HAND_CURSOR));
            // Layout
            FlowLayout layout = new FlowLayout(TRAILING, 3, 3);
            setLayout(layout);
            add(tabTitleLabel);
            add(closeButton);
        }
    }

    /**
     * Close button listener class
     */
    class CloseListener implements MouseListener {

        private final Component tab;

        public CloseListener(final Component tab) {
            this.tab = tab;
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if (e.getSource() instanceof JButton) {
                JButton closeButton = (JButton) e.getSource();
                JTabbedPane tabbedPane = (JTabbedPane) closeButton.getParent().getParent().getParent();
                tabbedPane.remove(tab);
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {
        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            if (e.getSource() instanceof JButton) {
                JButton closeButton = (JButton) e.getSource();
                closeButton.setIcon(getIcon(TIMES, 14, new Color(200, 51, 51)));
            }
        }

        @Override
        public void mouseExited(MouseEvent e) {
            if (e.getSource() instanceof JButton) {
                JButton closeButton = (JButton) e.getSource();
                closeButton.setIcon(getIcon(TIMES, 14, BLACK));
            }
        }
    }
}
