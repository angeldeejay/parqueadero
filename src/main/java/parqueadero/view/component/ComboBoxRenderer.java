/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parqueadero.view.component;

import java.awt.Component;
import java.util.function.Function;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import parqueadero.util.AbstractModel;

/**
 *
 * @author avanegas
 * @param <T>
 */
public class ComboBoxRenderer<T extends AbstractModel> extends BasicComboBoxRenderer {

    private Function<T, String> itemRendererFunction;

    public ComboBoxRenderer(Function<T, String> itemRendererFunction) {
        this.itemRendererFunction = itemRendererFunction;
    }

    public ComboBoxRenderer() {
        this.itemRendererFunction = (T item) -> item.toString();
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        JLabel c = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        c.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        if (value != null) {
            T item = (T) value;
            c.setText(itemRendererFunction.apply(item));
        }
        return c;
    }
}
