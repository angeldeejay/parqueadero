/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.view.component;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Vector;
import javax.swing.JComboBox;
import parqueadero.util.AbstractModel;

/**
 * ComboBox class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class ComboBox<T extends AbstractModel> extends JComboBox {

    public ComboBox(Collection<T> items) {
        this();
        addItems(items);
    }

    public ComboBox(T[] items) {
        super(items);
        setModel(new ComboBoxModel<T>());
    }

    public ComboBox(Vector<T> items) {
        super(items);
        setModel(new ComboBoxModel<T>());
    }

    public ComboBox() {
        super();
        setModel(new ComboBoxModel<T>());
    }

    public void addItem(T item) {
        super.addItem(item);
    }

    public void addItems(Collection<T> items) {
        for (T item : items) {
            ((ComboBoxModel<T>) getModel()).addElement(item);
        }
    }

    public T getSelectedEntity() {
        return (T) super.getSelectedItem();
    }

    /**
     *
     * @return
     */
    public BigDecimal getSelectedId() {
        if (super.getSelectedIndex() >= 0) {
            return ((T) super.getSelectedItem()).getId();
        }
        return null;
    }
}
