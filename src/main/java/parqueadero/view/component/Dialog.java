/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */

package parqueadero.view.component;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import static java.awt.event.KeyEvent.VK_ENTER;
import static java.awt.event.KeyEvent.VK_ESCAPE;
import static javax.swing.JComponent.WHEN_IN_FOCUSED_WINDOW;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;
import static javax.swing.KeyStroke.getKeyStroke;

/**
 * Dialog class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public abstract class Dialog extends javax.swing.JDialog {

    public Dialog() {
    }

    public Dialog(Frame owner) {
        super(owner);
    }

    public Dialog(Frame owner, boolean modal) {
        super(owner, modal);
    }
    

    @Override
    protected JRootPane createRootPane() {
        ActionListener exitOnEscapeKeyListener = (ActionEvent actionEvent) -> {
            this.cancelAction();
        };
        ActionListener loginOnEnterKeyListener = (ActionEvent actionEvent) -> {
            this.defaultAction();
        };
        this.rootPane = new JRootPane();
        KeyStroke escapeStroke = getKeyStroke(VK_ESCAPE, 0);
        KeyStroke enterStroke = getKeyStroke(VK_ENTER, 0);
        this.rootPane.registerKeyboardAction(exitOnEscapeKeyListener, escapeStroke, WHEN_IN_FOCUSED_WINDOW);
        this.rootPane.registerKeyboardAction(loginOnEnterKeyListener, enterStroke, WHEN_IN_FOCUSED_WINDOW);
        return this.rootPane;
    }

    protected abstract void cancelAction();
    protected abstract void defaultAction();
}
