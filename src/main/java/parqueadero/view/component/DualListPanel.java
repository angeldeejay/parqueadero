/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.view.component;

import java.awt.Color;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import javax.swing.ListModel;
import jiconfont.icons.font_awesome.FontAwesome;
import parqueadero.util.AbstractModel;
import parqueadero.util.Icon;

/**
 * DualListPanel component class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class DualListPanel<T extends AbstractModel> extends javax.swing.JPanel {

    private Function<T, String> getLabelCallable;
    private Function<T, Integer> getIdCallable;
    private boolean enabled = true;

    /**
     * Creates new form DualList
     */
    public DualListPanel() {
        this((T el) -> el.getId() == null ? "" : el.getId().toPlainString());
    }

    public DualListPanel(Function<T, String> getLabelCallable) {
        this(getLabelCallable, (T el) -> el.getId() == null ? null : el.getId().intValue());
    }

    public DualListPanel(Function<T, String> getLabelCallable, Function<T, Integer> getIdCallable) {
        this.getLabelCallable = getLabelCallable;
        this.getIdCallable = getIdCallable;
        initComponents();
    }

    protected void addElements(parqueadero.view.component.List list, ListModel newValue) {
        fillListModel((SortedListModel) list.getModel(), newValue);
    }

    protected void addElements(parqueadero.view.component.List list, Collection<T> newValues) {
        fillListModel((SortedListModel) list.getModel(), newValues);
    }

    protected void addElements(parqueadero.view.component.List list, T[] arrayValues) {
        Collection<T> newValues = Arrays.asList(arrayValues);
        addElements(list, newValues);
    }

    protected void addElement(parqueadero.view.component.List list, T newValue) {
        ((SortedListModel) list.getModel()).add(newValue);
    }

    protected void setElements(parqueadero.view.component.List list, Collection<T> newValues) {
        clearListModel(list);
        addElements(list, newValues);
    }

    protected void setElements(parqueadero.view.component.List list, T[] arrayValues) {
        Collection<T> newValues = Arrays.asList(arrayValues);
        setElements(list, newValues);
    }

    protected void setElements(parqueadero.view.component.List list, ListModel newValue) {
        clearListModel(list);
        addElements(list, newValue);
    }

    protected void setElement(parqueadero.view.component.List list, T newValue) {
        clearListModel(list);
        addElement(list, newValue);
    }

    public void addDestinationElements(ListModel newValue) {
        addElements(sourceList, newValue);
    }

    public void addDestinationElements(Collection<T> newValues) {
        addElements(sourceList, newValues);
    }

    public void addDestinationElements(T[] newValues) {
        addElements(sourceList, newValues);
    }

    public void addDestinationElement(T newValue) {
        addElement(sourceList, newValue);
    }

    public void addSourceElements(ListModel newValue) {
        addElements(sourceList, newValue);
    }

    public void addSourceElements(Collection<T> newValues) {
        addElements(sourceList, newValues);
    }

    public void addSourceElements(T[] newValues) {
        addElements(sourceList, newValues);
    }

    public void addSourceElement(T newValue) {
        addElement(sourceList, newValue);
    }

    protected void clearListModel(parqueadero.view.component.List list) {
        ((SortedListModel) list.getModel()).clear();
    }

    public Collection<T> getSelectedValuesList() {
        return destList.getValuesList();
    }

    public Collection<Integer> getSelectedIdsList() {
        return destList.getIdsList();
    }

    public Collection<String> getSelectedLabelsList() {
        return destList.getLabelsList();
    }

    public void setValues(Collection<T> values) {
        setValues(values, new java.util.ArrayList<T>());
    }

    public void setValues(Collection<T> values, Collection<T> selected) {
        values.removeAll(selected);
        setElements(sourceList, values);
        setElements(destList, selected);
    }

    public Collection<T> getUnselectedValuesList() {
        return sourceList.getSelectedValuesList();
    }

    public Collection<Integer> getUnselectedIdsList() {
        return sourceList.getSelectedIdsList();
    }

    public Collection<String> getUnselectedLabelsList() {
        return sourceList.getSelectedLabelsList();
    }

    private void clearSelected(parqueadero.view.component.List list) {
        Collection<T> selected = list.getSelectedValuesList();
        for (T item : selected) {
            ((SortedListModel<T>) list.getModel()).removeElement(item);
        }
        list.getSelectionModel().clearSelection();
    }

    private void fillListModel(SortedListModel model, ListModel newValues) {
        int size = newValues.getSize();
        for (int i = 0; i < size; i++) {
            model.add((T) newValues.getElementAt(i));
        }
    }

    private void fillListModel(SortedListModel model, Collection<T> newValues) {
        model.addAll((List) newValues);
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
        this.sourceList.setEnabled(this.enabled);
        this.destList.setEnabled(this.enabled);
        this.addButton.setEnabled(this.enabled);
        this.removeButton.setEnabled(this.enabled);
    }

    @Override
    public boolean isEnabled() {
        return this.enabled; //To change body of generated methods, choose Tools | Templates.
    }

    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.JPanel sourcePanel = new javax.swing.JPanel();
        javax.swing.JScrollPane sourceScrollPane = new javax.swing.JScrollPane();
        sourceList = new parqueadero.view.component.List<T>(this.getLabelCallable, this.getIdCallable);
        addButton = new javax.swing.JButton();
        javax.swing.JPanel destPanel = new javax.swing.JPanel();
        javax.swing.JScrollPane destScrollPane = new javax.swing.JScrollPane();
        destList = new parqueadero.view.component.List<T>(this.getLabelCallable, this.getIdCallable);
        removeButton = new javax.swing.JButton();

        setLayout(new java.awt.GridLayout(1, 2, 5, 0));

        sourcePanel.setLayout(new java.awt.BorderLayout());

        sourceScrollPane.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1), "Opciones disponibles", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.ABOVE_TOP));
        sourceScrollPane.setViewportView(sourceList);

        sourcePanel.add(sourceScrollPane, java.awt.BorderLayout.CENTER);

        addButton.setIcon(Icon.getIcon(FontAwesome.PLUS_CIRCLE, new Color(0, 128, 0)));
        addButton.setText("Agregar");
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });
        sourcePanel.add(addButton, java.awt.BorderLayout.PAGE_END);

        add(sourcePanel);

        destPanel.setLayout(new java.awt.BorderLayout());

        destScrollPane.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1), "Opciones seleccionadas", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.ABOVE_TOP));
        destScrollPane.setViewportView(destList);

        destPanel.add(destScrollPane, java.awt.BorderLayout.CENTER);

        removeButton.setIcon(Icon.getIcon(FontAwesome.MINUS_CIRCLE, new Color(128, 0, 0)));
        removeButton.setText("Remover");
        removeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeButtonActionPerformed(evt);
            }
        });
        destPanel.add(removeButton, java.awt.BorderLayout.PAGE_END);

        add(destPanel);
    }// </editor-fold>//GEN-END:initComponents

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
        addElements(destList, sourceList.getSelectedValuesList());
        clearSelected(sourceList);
    }//GEN-LAST:event_addButtonActionPerformed

    private void removeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeButtonActionPerformed
        addElements(sourceList, destList.getSelectedValuesList());
        clearSelected(destList);
    }//GEN-LAST:event_removeButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private parqueadero.view.component.List<T> destList;
    private javax.swing.JButton removeButton;
    private parqueadero.view.component.List<T> sourceList;
    // End of variables declaration//GEN-END:variables
}
