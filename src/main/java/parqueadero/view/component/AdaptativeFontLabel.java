/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.view.component;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

/**
 * AdaptativeFontLabel class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class AdaptativeFontLabel extends javax.swing.JLabel {

    private static int MAX_FONT_SIZE = 512;
    private static int MIN_FONT_SIZE = 1;
    private int referenceFontSize = MAX_FONT_SIZE;
    Graphics g;

    public AdaptativeFontLabel(String text) {
        super(text == null ? "" : text);
        addComponentListener(new ComponentListener() {
            @Override
            public void componentHidden(ComponentEvent ce) {
                doResize();
            }

            @Override
            public void componentMoved(ComponentEvent ce) {
                doResize();
            }

            @Override
            public void componentResized(ComponentEvent ce) {
                doResize();
            }

            @Override
            public void componentShown(ComponentEvent ce) {
                doResize();
            }
        });
    }

    public AdaptativeFontLabel() {
        this(null);
    }

    public int getReferenceFontSize() {
        return referenceFontSize;
    }

    public void setReferenceFontSize(int referenceFontSize) {
        this.referenceFontSize = referenceFontSize;
    }

    public void adaptLabelFont() {
        if (g == null) {
            return;
        }

        int fontSize = MIN_FONT_SIZE;
        Font f = this.getFont();
        Rectangle b = getBounds();

        Rectangle r = new Rectangle(b.width - (b.x * 2), b.height - (b.y * 2));
        Rectangle r1 = new Rectangle();
        Rectangle r2 = new Rectangle();
        while (fontSize < referenceFontSize) {
            r1.setSize(getTextSize(f.deriveFont(f.getStyle(), fontSize)));
            r2.setSize(getTextSize(f.deriveFont(f.getStyle(), fontSize + 1)));
            if (r.contains(r1) && !r.contains(r2)) {
                break;
            }
            fontSize++;
        }

        setFont(f.deriveFont(f.getStyle(), fontSize));
        repaint();
    }

    public void doResize() {
        adaptLabelFont();
    }

    private Dimension getTextSize(Font f) {
        Dimension size = new Dimension();
        g.setFont(f);
        FontMetrics fm = g.getFontMetrics(f);
        size.width = fm.stringWidth("XXX-888");
        size.height = fm.getHeight();

        return size;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.g = g;
    }
}
