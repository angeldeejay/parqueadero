/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.view.component.report;

import com.github.lgooddatepicker.optionalusertools.DateChangeListener;
import com.github.lgooddatepicker.optionalusertools.PickerUtilities;
import com.github.lgooddatepicker.zinternaltools.DateChangeEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import org.apache.ibatis.jdbc.SQL;
import org.knowm.xchart.CategoryChart;
import org.knowm.xchart.CategoryChartBuilder;
import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.style.Styler.LegendPosition;
import parqueadero.model.Vehicle;
import static parqueadero.util.DataSource.executeQuery;

/**
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class StatsReport extends javax.swing.JPanel {

    private DefaultTableModel tableModel;

    private SQL getVehicleCountQuery() {
        return new SQL() {
            {
                SELECT("COUNT(c.id) AS C");
                SELECT("COUNT(m.id) AS M");
                SELECT("COUNT(b.id) AS B");
                FROM("tickets t");
                LEFT_OUTER_JOIN("vehicles v ON v.id = t.vehicle_id");
                LEFT_OUTER_JOIN("cars c ON c.id = v.id");
                LEFT_OUTER_JOIN("motorcycles m ON m.id = v.id");
                LEFT_OUTER_JOIN("bikes b ON b.id = v.id");
                String dateF = PickerUtilities.localDateToString(dateFrom.getDate(), null);
                String dateT = PickerUtilities.localDateToString(dateTo.getDate(), null);
                boolean withDates = dateF != null && dateT != null;
                if (withDates) {
                    if (dateF.equalsIgnoreCase(dateT)) {
                        WHERE(String.format("TRUNC(t.checkin) >= TO_DATE('%s', 'yyyy-mm-dd')", dateF));
                    } else {
                        WHERE(String.format("TRUNC(t.checkin) BETWEEN TO_DATE('%s', 'yyyy-mm-dd') "
                                + "AND TO_DATE('%s', 'yyyy-mm-dd')", dateF, dateT));
                    }
                }
            }
        };
    }

    private SQL getVehicleTypesCountQuery() {
        return new SQL() {
            {
                SELECT("COUNT(c.id) AS automoviles");
                SELECT("COUNT(m.id) AS motocicletas");
                SELECT("COUNT(b.id) AS bicicletas");
                FROM("vehicles v");
                LEFT_OUTER_JOIN("cars c ON c.id = v.id");
                LEFT_OUTER_JOIN("motorcycles m ON m.id = v.id");
                LEFT_OUTER_JOIN("bikes b ON b.id = v.id");
            }
        };
    }

    /**
     * Creates new form ResumeReport
     */
    public StatsReport() {
        initComponents();
        setupUIDefaults();
    }

    private void setupUIDefaults() {
        this.tableModel = new DefaultTableModel(
                new Object[][]{},
                new String[]{"Estadistica", "Valor"}
        ) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        this.table.setFocusable(false);
        this.table.setRowSelectionAllowed(false);
        this.table.setModel(tableModel);
        DateChangeListener dateChangeListener = (DateChangeEvent dce) -> {
            setupCharts();
        };
        this.dateFrom.addDateChangeListener(dateChangeListener);
        this.dateFrom.getSettings().setAllowEmptyDates(false);
        this.dateFrom.setDateToToday();
        this.dateTo.addDateChangeListener(dateChangeListener);
        this.dateTo.getSettings().setAllowEmptyDates(false);
        this.dateTo.setDateToToday();
    }

    private List<Integer> getVehicleCountSerie() {
        SQL query = getVehicleCountQuery()
                .WHERE("t.checkin IS NOT NULL")
                .WHERE("t.checkout IS NOT NULL");
        Integer[] serie = new Integer[]{0, 0, 0};
        for (HashMap<String, Object> row : executeQuery(query.toString())) {
            for (Entry<String, Object> e : row.entrySet()) {
                switch (e.getKey().toUpperCase()) {
                    case Vehicle.CAR:
                        serie[0] = ((BigDecimal) e.getValue()).intValueExact();
                        break;
                    case Vehicle.MOTORCYCLE:
                        serie[1] = ((BigDecimal) e.getValue()).intValueExact();
                        break;
                    case Vehicle.BIKE:
                        serie[2] = ((BigDecimal) e.getValue()).intValueExact();
                        break;
                }
            }
            break;
        }
        return Arrays.asList(serie);
    }

    private void addVehiclesTypesCount() {
        for (HashMap<String, Object> row : executeQuery(getVehicleTypesCountQuery().toString())) {
            row.forEach((k, v) -> {
                tableModel.addRow(new Object[]{"Total de " + k, ((BigDecimal) v).intValueExact()});
            });
        }
    }

    private void addEntitiesCount() {
        String query = new SQL() {
            {
                SELECT("COUNT(u.id) AS usuarios");
                SELECT("COUNT(o.id) AS clientes");
                FROM("entities e");
                LEFT_OUTER_JOIN("users u ON u.id = e.id AND u.enabled = '1'");
                LEFT_OUTER_JOIN("owners o ON o.id = e.id AND o.enabled = '1'");
                WHERE("COALESCE(o.id, u.id) IS NOT NULL");
            }
        }.toString();
        for (HashMap<String, Object> row : executeQuery(query)) {
            row.forEach((k, v) -> {
                tableModel.addRow(new Object[]{"Total de " + k, ((BigDecimal) v).intValueExact()});
            });
        }
    }

    private void addParkingLotCount() {
        String query;
        query = new SQL() {
            {
                SELECT("COUNT(z.id) AS zonas");
                FROM("zones z");
                WHERE("z.enabled = '1'");
            }
        }.toString();
        for (HashMap<String, Object> row : executeQuery(query)) {
            row.forEach((k, v) -> {
                tableModel.addRow(new Object[]{"Total de " + k, ((BigDecimal) v).intValueExact()});
            });
        }
        query = new SQL() {
            {
                SELECT("COUNT(l.id)");
                FROM("lots l");
                WHERE("l.enabled = '1'");
                WHERE("l.empty = '1'");
            }
        }.toString();
        for (HashMap<String, Object> row : executeQuery(query)) {
            row.forEach((k, v) -> {
                tableModel.addRow(new Object[]{"Total de bahías disponibles", ((BigDecimal) v).intValueExact()});
            });
        }
        query = new SQL() {
            {
                SELECT("COUNT(l.id)");
                FROM("lots l");
                WHERE("l.enabled = '1'");
                WHERE("l.empty = '0'");
            }
        }.toString();
        for (HashMap<String, Object> row : executeQuery(query)) {
            row.forEach((k, v) -> {
                tableModel.addRow(new Object[]{"Total de bahías ocupadas", ((BigDecimal) v).intValueExact()});
            });
        }
        query = new SQL() {
            {
                SELECT("COUNT(l.id) AS bahias");
                FROM("lots l");
                WHERE("l.enabled = '1'");
            }
        }.toString();
        for (HashMap<String, Object> row : executeQuery(query)) {
            row.forEach((k, v) -> {
                tableModel.addRow(new Object[]{"Total de " + k, ((BigDecimal) v).intValueExact()});
            });
        }
    }

    private List<Integer> getVehicleInsideSerie() {
        SQL query = getVehicleCountQuery()
                .WHERE("t.checkin IS NOT NULL")
                .WHERE("t.checkout IS NULL");
        Integer[] serie = new Integer[]{0, 0, 0};
        for (HashMap<String, Object> row : executeQuery(query.toString())) {
            for (Entry<String, Object> e : row.entrySet()) {
                switch (e.getKey().toUpperCase()) {
                    case Vehicle.CAR:
                        serie[0] = ((BigDecimal) e.getValue()).intValueExact();
                        break;
                    case Vehicle.MOTORCYCLE:
                        serie[1] = ((BigDecimal) e.getValue()).intValueExact();
                        break;
                    case Vehicle.BIKE:
                        serie[2] = ((BigDecimal) e.getValue()).intValueExact();
                        break;
                }
            }
            break;
        }
        return Arrays.asList(serie);
    }

    private void setupCharts() {
        this.tableModel.setRowCount(0);
        addVehiclesTypesCount();
        addEntitiesCount();
        addParkingLotCount();

        List<String> labels = Arrays.asList(new String[]{"Automóvil", "Motocicleta", "Bicicleta"});
        List<Integer> vehicleCountSerie = getVehicleCountSerie();
        List<Integer> vehicleInsideSerie = getVehicleInsideSerie();
        this.graphsContainer.removeAll();

        CategoryChart vehicleOutCount = new CategoryChartBuilder().width(480).build();
        vehicleOutCount.setTitle("Han salido");
        vehicleOutCount.getStyler().setLegendVisible(false);
        vehicleOutCount.getStyler().setHasAnnotations(true);
        vehicleOutCount.getStyler().setAxisTickLabelsFont(getFont());
        vehicleOutCount.getStyler().setYAxisMin(0.0);
        vehicleOutCount.getStyler().setYAxisMax(new Double(findMax(vehicleCountSerie) + 5));
        vehicleOutCount.getStyler().setLegendPosition(LegendPosition.OutsideE);
        vehicleOutCount.addSeries("Han salido", labels, vehicleCountSerie);
        JPanel vehicleOutCountPanel = new XChartPanel<>(vehicleOutCount);
        vehicleOutCountPanel.setDoubleBuffered(true);
        this.graphsContainer.add(vehicleOutCountPanel);

        CategoryChart vehicleInCount = new CategoryChartBuilder().width(480).build();
        vehicleInCount.setTitle("Estacionados");
        vehicleInCount.getStyler().setLegendVisible(false);
        vehicleInCount.getStyler().setHasAnnotations(true);
        vehicleInCount.getStyler().setAxisTickLabelsFont(getFont());
        vehicleInCount.getStyler().setYAxisMin(0.0);
        vehicleInCount.getStyler().setYAxisMax(new Double(findMax(vehicleCountSerie) + 5));
        vehicleInCount.getStyler().setLegendPosition(LegendPosition.OutsideE);
        vehicleInCount.addSeries("Estacionados", labels, vehicleInsideSerie);
        JPanel vehicleInCountPanel = new XChartPanel<>(vehicleInCount);
        vehicleInCountPanel.setDoubleBuffered(true);
        this.graphsContainer.add(vehicleInCountPanel);

        this.graphsContainer.revalidate();
        this.graphsContainer.repaint();
        this.revalidate();
        this.repaint();
    }

    public static Integer findMin(List<Integer> list) {
        if (list == null || list.size() == 0) {
            return Integer.MAX_VALUE;
        }
        List<Integer> sortedlist = new ArrayList<>(list);
        Collections.sort(sortedlist);
        return sortedlist.get(0);
    }

    public static Integer findMax(List<Integer> list) {
        if (list == null || list.size() == 0) {
            return Integer.MIN_VALUE;
        }
        List<Integer> sortedlist = new ArrayList<>(list);
        Collections.sort(sortedlist);
        return sortedlist.get(sortedlist.size() - 1);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        formPanel = new javax.swing.JPanel();
        javax.swing.JLabel jLabel2 = new javax.swing.JLabel();
        dateFrom = new com.github.lgooddatepicker.components.DatePicker();
        javax.swing.JLabel jLabel1 = new javax.swing.JLabel();
        dateTo = new com.github.lgooddatepicker.components.DatePicker();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        jButton1 = new javax.swing.JButton();
        graphsContainer = new javax.swing.JPanel();
        javax.swing.JScrollPane tableScrollPane = new javax.swing.JScrollPane();
        table = new parqueadero.view.component.Table();

        setBorder(javax.swing.BorderFactory.createTitledBorder("Estadísticas generales"));
        setLayout(new java.awt.BorderLayout());

        formPanel.setBorder(javax.swing.BorderFactory.createEmptyBorder(3, 3, 3, 3));
        formPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel2.setText("Desde");
        formPanel.add(jLabel2);
        formPanel.add(dateFrom);

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel1.setText("Hasta");
        formPanel.add(jLabel1);
        formPanel.add(dateTo);
        formPanel.add(filler1);

        jButton1.setText("Actualizar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        formPanel.add(jButton1);

        add(formPanel, java.awt.BorderLayout.PAGE_START);

        graphsContainer.setMinimumSize(new java.awt.Dimension(100, 0));
        graphsContainer.setLayout(new java.awt.GridLayout(2, 1, 5, 5));
        add(graphsContainer, java.awt.BorderLayout.LINE_END);

        tableScrollPane.setViewportView(table);

        add(tableScrollPane, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        setupCharts();
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.github.lgooddatepicker.components.DatePicker dateFrom;
    private com.github.lgooddatepicker.components.DatePicker dateTo;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JPanel formPanel;
    private javax.swing.JPanel graphsContainer;
    private javax.swing.JButton jButton1;
    private parqueadero.view.component.Table table;
    // End of variables declaration//GEN-END:variables
}
