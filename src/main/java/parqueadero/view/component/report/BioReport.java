/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.view.component.report;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import javax.swing.table.DefaultTableModel;
import org.apache.ibatis.jdbc.SQL;
import static parqueadero.util.DataSource.executeQuery;
import static parqueadero.util.Logger.getLogger;

/**
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class BioReport extends javax.swing.JPanel {

    private DefaultTableModel csvTableModel;

    private SQL getParkingLotDataQuery() {
        return new SQL() {
            {
                SELECT("z.code AS Z");
                SELECT("(z.code || '-' || l.code) AS L");
                SELECT("l.empty AS V");
                SELECT("l.enabled AS H");
                SELECT("COUNT(c.id) AS A");
                SELECT("COUNT(m.id) AS M");
                SELECT("COUNT(b.id) AS B");
                FROM("zones z");
                INNER_JOIN("lots l ON l.zone_id = z.id");
                LEFT_OUTER_JOIN("tickets t ON t.lot_id = l.id AND t.checkin IS NOT NULL AND t.checkout IS NULL");
                LEFT_OUTER_JOIN("vehicles v ON v.id = t.vehicle_id");
                LEFT_OUTER_JOIN("cars c ON c.id = v.id");
                LEFT_OUTER_JOIN("motorcycles m ON m.id = v.id");
                LEFT_OUTER_JOIN("bikes b ON b.id = v.id");
                GROUP_BY("z.code, l.code, l.empty, l.enabled");
                ORDER_BY("z.code, l.code");
            }
        };
    }

    private SQL getParkingLotGroupedDataQuery() {
        return new SQL() {
            {
                SELECT("z.code AS Z");
                SELECT("COUNT(c.id) AS A");
                SELECT("COUNT(m.id) AS M");
                SELECT("COUNT(b.id) AS B");
                SELECT("SUM(l.empty) AS V");
                SELECT("COUNT(l.id) - SUM(l.empty) AS O");
                SELECT("SUM(l.enabled) AS H");
                SELECT("COUNT(l.id) - SUM(l.enabled) AS D");
                FROM("zones z");
                INNER_JOIN("lots l ON l.zone_id = z.id");
                LEFT_OUTER_JOIN("tickets t ON t.lot_id = l.id AND t.checkin IS NOT NULL AND t.checkout IS NULL");
                LEFT_OUTER_JOIN("vehicles v ON v.id = t.vehicle_id");
                LEFT_OUTER_JOIN("cars c ON c.id = v.id");
                LEFT_OUTER_JOIN("motorcycles m ON m.id = v.id");
                LEFT_OUTER_JOIN("bikes b ON b.id = v.id");
                GROUP_BY("z.code");
                ORDER_BY("z.code");
            }
        };
    }

    /**
     * Creates new form ResumeReport
     */
    public BioReport() {
        initComponents();
        setupUIDefaults();
    }

    private void setupUIDefaults() {
        this.csvTableModel = new DefaultTableModel(
                new Object[][]{},
                new String[]{"Nombre", "Operación", "Fecha Bio", "Tiempo Bio", "Fecha Parq", "Tiempo Parq", "Diferencia"}
        ) {
            Class[] types = new Class[]{
                String.class, String.class, String.class, String.class, String.class, String.class, Long.class
            };

            @Override
            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        this.heatTable.setFocusable(false);
        this.heatTable.setRowSelectionAllowed(false);
        this.heatTable.setModel(csvTableModel);
        heatTable.setDisabledValidator((Integer r, Integer c) -> {
            if (r >= 0) {
                Long cellValue = (Long) heatTable.getModel().getValueAt(r, 6);
                String opValue = (String) heatTable.getModel().getValueAt(r, 1);
                return opValue.equalsIgnoreCase("Entrada") && cellValue != null && cellValue > 0L;
            }
            return false;
        });
        heatTable.setHightlightedValidator((Integer r, Integer c) -> {
            if (r >= 0) {
                Long cellValue = (Long) heatTable.getModel().getValueAt(r, 6);
                String opValue = (String) heatTable.getModel().getValueAt(r, 1);
                return opValue.equalsIgnoreCase("Salida") && cellValue != null && cellValue > 0L;
            }
            return false;
        });

        addParkingLotCount();
    }

    private void addParkingLotCount() {
        this.csvTableModel.setRowCount(0);
        try (BufferedReader br = new BufferedReader(new InputStreamReader(
                this.getClass().getResourceAsStream("/ZTEK_export.csv")))) {
            String line;
            boolean first = true;
            while ((line = br.readLine()) != null) {
                if (first) {
                    first = false;
                    continue;
                }
                String[] values = line.split(",");
                Timestamp tsBio = Timestamp.valueOf(values[2]
                        .replaceAll("/", "-")
                        .replaceAll("^(\\d+)-(\\d+)-(\\d+)(.+)", "$3-$2-$1$4"));
                String query = new SQL() {
                    {
                        String f = (values[3].equalsIgnoreCase("Entrada")) ? "checkin" : "checkout";
                        String op = (values[3].equalsIgnoreCase("Entrada")) ? " < " : " > ";
                        String rp = (values[3].equalsIgnoreCase("Entrada")) ? " >= " : " < ";
                        String rt = (values[3].equalsIgnoreCase("Entrada")) ? "" : " + 1";
                        String or = (values[3].equalsIgnoreCase("Entrada")) ? " DESC" : " ASC";
                        SELECT("TO_CHAR(t." + f + ", 'YYYY-MM-DD HH24:MI:SS') AS ts");
                        FROM("tickets t");
                        WHERE("t." + f + op + " ?");
                        WHERE("t." + f + rp + " (TRUNC(?)" + rt + ")");
                        WHERE("ROWNUM = 1");
                        ORDER_BY("t." + f + or);
                    }
                }.toString();
                Timestamp tsParq = null;
                for (HashMap<String, Object> row : executeQuery(query, new Timestamp[]{tsBio, tsBio})) {
                    tsParq = Timestamp.valueOf((String) row.get("ts"));
                    break;
                }

                Timestamp tsDiff = tsParq == null ? null : diff(tsBio, tsParq);
                Long timeDiff = tsDiff == null ? null : tsDiff.getTime();
                this.csvTableModel.addRow(new Object[]{
                    values[1],
                    values[3],
                    tsBio != null ? new SimpleDateFormat("dd/MM/yyyy").format(tsBio) : "Sin registro",
                    tsBio != null ? new SimpleDateFormat("hh:mm:ss a").format(tsBio) : "Sin registro",
                    tsParq != null ? new SimpleDateFormat("dd/MM/yyyy").format(tsParq) : "Sin registro",
                    tsParq != null ? new SimpleDateFormat("hh:mm:ss a").format(tsParq) : "Sin registro",
                    (tsParq == null ? 0 : timeDiff / 60000L) - 7L
                });

            }
        } catch (Exception ex) {
            getLogger(this.getClass()).warn(ex.getMessage(), ex);
        }
    }

    public static Timestamp diff(java.util.Date t1, java.util.Date t2) {
        if (t1.compareTo(t2) < 0) {
            java.util.Date tmp = t1;
            t1 = t2;
            t2 = tmp;
        }

        long diffSeconds = (t1.getTime() / 1000) - (t2.getTime() / 1000);
        int nano1 = ((int) t1.getTime() % 1000) * 1000000;
        if (t1 instanceof Timestamp) {
            nano1 = ((Timestamp) t1).getNanos();
        }
        int nano2 = ((int) t2.getTime() % 1000) * 1000000;
        if (t2 instanceof Timestamp) {
            nano2 = ((Timestamp) t2).getNanos();
        }

        int diffNanos = nano1 - nano2;
        if (diffNanos < 0) {
            diffSeconds--;
            diffNanos += 1000000000;
        }

        Timestamp result = new Timestamp((diffSeconds * 1000) + (diffNanos / 1000000));
        result.setNanos(diffNanos);
        return result;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.JPanel toolbarPane = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        javax.swing.JPanel tablesContainer = new javax.swing.JPanel();
        javax.swing.JPanel heatMapWrapper = new javax.swing.JPanel();
        javax.swing.JScrollPane heatTableScrollPane = new javax.swing.JScrollPane();
        heatTable = new parqueadero.view.component.Table();

        setLayout(new java.awt.BorderLayout());

        toolbarPane.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        jButton1.setText("Actualizar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        toolbarPane.add(jButton1);

        add(toolbarPane, java.awt.BorderLayout.PAGE_START);

        tablesContainer.setLayout(new java.awt.GridLayout(1, 2, 5, 5));

        heatMapWrapper.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 5, 5, 5));
        heatMapWrapper.setLayout(new java.awt.BorderLayout());

        heatTableScrollPane.setViewportView(heatTable);

        heatMapWrapper.add(heatTableScrollPane, java.awt.BorderLayout.CENTER);

        tablesContainer.add(heatMapWrapper);

        add(tablesContainer, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        addParkingLotCount();
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private parqueadero.view.component.Table heatTable;
    private javax.swing.JButton jButton1;
    // End of variables declaration//GEN-END:variables
}
