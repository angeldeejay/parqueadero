/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.view.component.report;

import java.math.BigDecimal;
import java.util.HashMap;
import javax.swing.table.DefaultTableModel;
import org.apache.ibatis.jdbc.SQL;
import static parqueadero.util.DataSource.executeQuery;

/**
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class HeatMapReport extends javax.swing.JPanel {

    private DefaultTableModel heatTableModel;
    private DefaultTableModel groupedTableModel;

    private SQL getParkingLotDataQuery() {
        return new SQL() {
            {
                SELECT("z.code AS Z");
                SELECT("(z.code || '-' || l.code) AS L");
                SELECT("l.empty AS V");
                SELECT("l.enabled AS H");
                SELECT("COUNT(c.id) AS A");
                SELECT("COUNT(m.id) AS M");
                SELECT("COUNT(b.id) AS B");
                FROM("zones z");
                INNER_JOIN("lots l ON l.zone_id = z.id");
                LEFT_OUTER_JOIN("tickets t ON t.lot_id = l.id AND t.checkin IS NOT NULL AND t.checkout IS NULL");
                LEFT_OUTER_JOIN("vehicles v ON v.id = t.vehicle_id");
                LEFT_OUTER_JOIN("cars c ON c.id = v.id");
                LEFT_OUTER_JOIN("motorcycles m ON m.id = v.id");
                LEFT_OUTER_JOIN("bikes b ON b.id = v.id");
                GROUP_BY("z.code, l.code, l.empty, l.enabled");
                ORDER_BY("z.code, l.code");
            }
        };
    }

    private SQL getParkingLotGroupedDataQuery() {
        return new SQL() {
            {
                SELECT("z.code AS Z");
                SELECT("COUNT(c.id) AS A");
                SELECT("COUNT(m.id) AS M");
                SELECT("COUNT(b.id) AS B");
                SELECT("SUM(l.empty) AS V");
                SELECT("COUNT(l.id) - SUM(l.empty) AS O");
                SELECT("SUM(l.enabled) AS H");
                SELECT("COUNT(l.id) - SUM(l.enabled) AS D");
                FROM("zones z");
                INNER_JOIN("lots l ON l.zone_id = z.id");
                LEFT_OUTER_JOIN("tickets t ON t.lot_id = l.id AND t.checkin IS NOT NULL AND t.checkout IS NULL");
                LEFT_OUTER_JOIN("vehicles v ON v.id = t.vehicle_id");
                LEFT_OUTER_JOIN("cars c ON c.id = v.id");
                LEFT_OUTER_JOIN("motorcycles m ON m.id = v.id");
                LEFT_OUTER_JOIN("bikes b ON b.id = v.id");
                GROUP_BY("z.code");
                ORDER_BY("z.code");
            }
        };
    }

    /**
     * Creates new form ResumeReport
     */
    public HeatMapReport() {
        initComponents();
        setupUIDefaults();
    }

    private void setupUIDefaults() {
        this.heatTableModel = new DefaultTableModel(
                new Object[][]{},
                new String[]{"Zona", "Ubicación", "Automóvil", "Motocicleta", "Bicicleta", "Disponible", "Habilitado"}
        ) {
            Class[] types = new Class[]{
                String.class, String.class, Boolean.class, Boolean.class, Boolean.class, Boolean.class, Boolean.class
            };

            @Override
            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        this.heatTable.setFocusable(false);
        this.heatTable.setRowSelectionAllowed(false);
        this.heatTable.setModel(heatTableModel);
        heatTable.setDisabledValidator((Integer r, Integer c) -> (r >= 0) ? ((Boolean) heatTable.getModel().getValueAt(r, 6)) != true : false);
        heatTable.setHightlightedValidator((Integer r, Integer c) -> (r >= 0) ? ((Boolean) heatTable.getModel().getValueAt(r, 5)) != true : false);
        
        this.groupedTableModel = new DefaultTableModel(
                new Object[][]{},
                new String[]{"Zona", "Automóviles", "Motocicletas", "Bicicletas", "Disponibles", "Ocupados", "Habilitados", "Deshabilitados"}
        ) {
            Class[] types = new Class[]{
                String.class, Integer.class, Integer.class, Integer.class, Integer.class, Integer.class, Integer.class, Integer.class
            };

            @Override
            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        this.groupedTable.setFocusable(false);
        this.groupedTable.setRowSelectionAllowed(false);
        this.groupedTable.setModel(groupedTableModel);
        groupedTable.setDisabledValidator((Integer r, Integer c) -> (r >= 0) ? ((Integer) groupedTable.getModel().getValueAt(r, 6)) <= 0 : false);
        groupedTable.setHightlightedValidator((Integer r, Integer c) -> (r >= 0) ? ((Integer) groupedTable.getModel().getValueAt(r, 5)) > 0 : false);
        addParkingLotCount();
    }

    private void addParkingLotCount() {
        this.heatTableModel.setRowCount(0);
        this.groupedTableModel.setRowCount(0);
        for (HashMap<String, Object> row : executeQuery(getParkingLotDataQuery().toString())) {
            this.heatTableModel.addRow(new Object[]{
                row.getOrDefault("z", "-"),
                row.getOrDefault("l", "-"),
                row.getOrDefault("a", "0").equals("1"),
                row.getOrDefault("m", "0").equals("1"),
                row.getOrDefault("b", "0").equals("1"),
                row.getOrDefault("v", "1").equals("1"),
                row.getOrDefault("h", "1").equals("1")
            });
        }
        for (HashMap<String, Object> row : executeQuery(getParkingLotGroupedDataQuery().toString())) {
            this.groupedTableModel.addRow(new Object[]{
                row.getOrDefault("z", "-"),
                ((BigDecimal) row.getOrDefault("a", "0")).intValueExact(),
                ((BigDecimal) row.getOrDefault("m", "0")).intValueExact(),
                ((BigDecimal) row.getOrDefault("b", "0")).intValueExact(),
                ((BigDecimal) row.getOrDefault("v", "1")).intValueExact(),
                ((BigDecimal) row.getOrDefault("o", "1")).intValueExact(),
                ((BigDecimal) row.getOrDefault("h", "1")).intValueExact(),
                ((BigDecimal) row.getOrDefault("d", "1"))
            });
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.JPanel toolbarPane = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        javax.swing.JPanel tablesContainer = new javax.swing.JPanel();
        javax.swing.JPanel heatMapWrapper = new javax.swing.JPanel();
        javax.swing.JScrollPane heatTableScrollPane = new javax.swing.JScrollPane();
        heatTable = new parqueadero.view.component.Table();
        javax.swing.JPanel zoneGroupedWrapper = new javax.swing.JPanel();
        javax.swing.JScrollPane groupedTableScrollPane = new javax.swing.JScrollPane();
        groupedTable = new parqueadero.view.component.Table();

        setLayout(new java.awt.BorderLayout());

        toolbarPane.setBorder(javax.swing.BorderFactory.createEmptyBorder(3, 3, 3, 3));
        toolbarPane.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        jButton1.setText("Actualizar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        toolbarPane.add(jButton1);

        add(toolbarPane, java.awt.BorderLayout.PAGE_START);

        tablesContainer.setLayout(new java.awt.GridLayout(1, 2, 5, 5));

        heatMapWrapper.setBorder(javax.swing.BorderFactory.createTitledBorder("Mapa de calor"));
        heatMapWrapper.setLayout(new java.awt.BorderLayout());

        heatTableScrollPane.setViewportView(heatTable);

        heatMapWrapper.add(heatTableScrollPane, java.awt.BorderLayout.CENTER);

        tablesContainer.add(heatMapWrapper);

        zoneGroupedWrapper.setBorder(javax.swing.BorderFactory.createTitledBorder("Resumen por zonas"));
        zoneGroupedWrapper.setLayout(new java.awt.BorderLayout());

        groupedTableScrollPane.setViewportView(groupedTable);

        zoneGroupedWrapper.add(groupedTableScrollPane, java.awt.BorderLayout.CENTER);

        tablesContainer.add(zoneGroupedWrapper);

        add(tablesContainer, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        addParkingLotCount();
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private parqueadero.view.component.Table groupedTable;
    private parqueadero.view.component.Table heatTable;
    private javax.swing.JButton jButton1;
    // End of variables declaration//GEN-END:variables
}
