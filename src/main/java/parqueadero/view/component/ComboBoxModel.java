/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.view.component;

import javax.swing.DefaultComboBoxModel;
import parqueadero.util.AbstractModel;

/**
 * ComboBoxModel class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 * @param <T>
 */
public class ComboBoxModel<T extends AbstractModel> extends DefaultComboBoxModel {

    /**
     *
     * @param model
     */
    public void setSelectedItem(T model) {
        super.setSelectedItem(model); 
    }

    /**
     *
     * @param model
     */
    public void removeElement(T model) {
        super.removeElement(model);
    }

    /**
     *
     * @param model
     * @param index
     */
    public void insertElementAt(T model, int index) {
        super.insertElementAt(model, index);
    }

    /**
     *
     * @param model
     */
    public void addElement(T model) {
        super.addElement(model);
    }

    @Override
    public T getElementAt(int index) {
        return (T) super.getElementAt(index);
    }

    @Override
    public T getSelectedItem() {
        return (T) super.getSelectedItem();
    }
}
