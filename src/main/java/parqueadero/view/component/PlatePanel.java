/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.view.component;

import java.awt.Dimension;
import static javax.swing.SwingUtilities.invokeLater;

/**
 * PlatePanel class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class PlatePanel extends javax.swing.JPanel {

    private String city;
    private String plate;

    /**
     * Creates new form PlatePanel
     */
    public PlatePanel() {
        this(null);
    }

    public PlatePanel(String plate) {
        this(plate, null);
    }

    public PlatePanel(String plate, String city) {
        super();
        this.city = city != null ? city : "";
        this.plate = plate != null ? plate : "";
        initComponents();
    }

    public void setCity(String city) {
        this.city = city != null ? city : "";
        this.updateLabel(this.cityLabel);
    }

    public void setPlate(String plate) {
        this.plate = plate != null ? plate : "";
        this.updateLabel(this.plateLabel);
    }

    public String getCity() {
        return city;
    }

    public String getPlate() {
        return plate;
    }

    private void updateLabel(AdaptativeFontLabel l) {
        l.setText(l.equals(this.plateLabel) ? this.plate : this.city);
        l.doResize();
    }

    @Override
    public String toString() {
        return "PlatePanel{" + "city=" + city + ", plate=" + plate + '}';
    }

    private void updatePanelLabels() {
        invokeLater(() -> {
            int padSize = (int) Math.round(getHeight() * 0.05);
            PlatePanel.this.setBorder(javax.swing.BorderFactory.createCompoundBorder(
                    javax.swing.BorderFactory.createEmptyBorder(padSize, padSize, padSize, padSize),
                    javax.swing.BorderFactory.createLineBorder(PlatePanel.this.plateLabel.getForeground(), padSize)));
            Dimension plateD = new Dimension((int) plateContainer.getWidth(), Math.round((float) (getHeight() * 0.75)));
            Dimension cityD = new Dimension((int) cityContainer.getWidth(), Math.round((float) (getHeight() * 0.25)));
            PlatePanel.this.plateLabel.setMaximumSize(plateD);
            PlatePanel.this.plateLabel.setMinimumSize(plateD);
            PlatePanel.this.plateLabel.setPreferredSize(plateD);
            PlatePanel.this.cityLabel.setReferenceFontSize(PlatePanel.this.plateLabel.getFont().getSize());
            PlatePanel.this.cityLabel.setMaximumSize(cityD);
            PlatePanel.this.cityLabel.setMinimumSize(cityD);
            PlatePanel.this.cityLabel.setPreferredSize(cityD);
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        plateContainer = new javax.swing.JPanel();
        plateLabel = new parqueadero.view.component.AdaptativeFontLabel();
        cityContainer = new javax.swing.JPanel();
        cityLabel = new parqueadero.view.component.AdaptativeFontLabel();

        setBackground(new java.awt.Color(193, 161, 16));
        setMaximumSize(new java.awt.Dimension(200, 100));
        setMinimumSize(new java.awt.Dimension(100, 50));
        setPreferredSize(new java.awt.Dimension(200, 100));
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                panelComponentResized(evt);
            }
            public void componentShown(java.awt.event.ComponentEvent evt) {
                panelShown(evt);
            }
        });
        addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                panelChanged(evt);
            }
        });
        setLayout(new java.awt.GridBagLayout());

        plateContainer.setBackground(new java.awt.Color(193, 161, 16));
        plateContainer.setLayout(new java.awt.BorderLayout());

        plateLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        plateLabel.setAlignmentX(0.5F);
        plateLabel.setFont(plateLabel.getFont().deriveFont(plateLabel.getFont().getStyle() | java.awt.Font.BOLD));
        plateLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        plateContainer.add(plateLabel, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.7;
        add(plateContainer, gridBagConstraints);

        cityContainer.setBackground(new java.awt.Color(193, 161, 16));
        cityContainer.setLayout(new java.awt.BorderLayout());

        cityLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cityLabel.setAlignmentX(0.5F);
        cityLabel.setFont(cityLabel.getFont().deriveFont(cityLabel.getFont().getStyle() | java.awt.Font.BOLD));
        cityLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        cityContainer.add(cityLabel, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 0.3;
        add(cityContainer, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void panelComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_panelComponentResized
        updatePanelLabels();
    }//GEN-LAST:event_panelComponentResized

    private void panelShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_panelShown
        updatePanelLabels();
    }//GEN-LAST:event_panelShown

    private void panelChanged(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_panelChanged
        updatePanelLabels();
    }//GEN-LAST:event_panelChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel cityContainer;
    private parqueadero.view.component.AdaptativeFontLabel cityLabel;
    private javax.swing.JPanel plateContainer;
    private parqueadero.view.component.AdaptativeFontLabel plateLabel;
    // End of variables declaration//GEN-END:variables
}
