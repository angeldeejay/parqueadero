/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.view.component;

import java.awt.Component;
import java.util.Collections;
import java.util.Enumeration;
import javax.swing.*;
import parqueadero.model.Entity;

/**
 * ColorComboBox class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class IdentTypeComboBox extends JComboBox {

    public IdentTypeComboBox() {
        super();
        DefaultComboBoxModel<String> model = new DefaultComboBoxModel();
        Entity.AVAILABLE_IDENT_TYPES.forEach((String k, String v) -> {
            model.addElement(k);
        });
        setModel(model);
        setRenderer(new VehicleRenderer());
        this.setOpaque(true);
        this.setSelectedIndex(0);
    }

    public void setSelectedType(String type) {
        for (String k : Collections.list((Enumeration<String>) Entity.AVAILABLE_IDENT_TYPES.keys())) {
            String v = Entity.AVAILABLE_IDENT_TYPES.get(k);
            if (v.equalsIgnoreCase(type)) {
                this.setSelectedItem(k);
                return;
            }
        }
        this.setSelectedItem(null);
    }

    public String getSelectedType() {
        if (super.getSelectedIndex() >= 0) {
            return Entity.AVAILABLE_IDENT_TYPES.get(super.getSelectedItem());
        }
        return null;
    }

    class VehicleRenderer extends javax.swing.DefaultListCellRenderer {

        public VehicleRenderer() {
            this.setOpaque(true);
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object key, int index,
                boolean isSelected, boolean cellHasFocus) {
            JLabel c = (JLabel) super.getListCellRendererComponent(list, key, index, isSelected, cellHasFocus);
            if (key != null) {
                c.setText(key.toString());
                String type = Entity.AVAILABLE_IDENT_TYPES.get(key);
            }
            c.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
            return c;
        }
    }
}
