/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.model;

import java.util.HashMap;
import parqueadero.util.AbstractModel;

/**
 * Motorcycle class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class Motorcycle extends AbstractModel {

    protected String plate;
    protected String city;
    protected String model;
    protected String serial;
    protected String color;

    public Motorcycle() {
        super();
    }

    public Motorcycle(HashMap data) {
        super(data);
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
