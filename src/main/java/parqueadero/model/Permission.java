/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.model;

import java.text.Normalizer;
import java.util.HashMap;
import parqueadero.util.AbstractModel;

/**
 * Permission class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class Permission extends AbstractModel {

    protected String code;
    protected String enabled;
    protected String name;

    public Permission(HashMap data) {
        super(data);
    }

    public Permission(String name, boolean enabled) {
        this.setName(name);
        this.setEnabled(enabled);
    }

    public Permission(String name) {
        this(name, true);
    }

    public Permission() {
        super();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        generateCode();
    }

    public boolean isEnabled() {
        return enabled.equals("1");
    }

    public void setEnabled(boolean enable) {
        this.enabled = (enable ? "1" : "0");
    }

    private void generateCode() {
        String s = Normalizer.normalize(this.name, Normalizer.Form.NFD)
                .replaceAll("[\\p{InCombiningDiacriticalMarks}]", "")
                .toUpperCase()
                .replaceAll("[^A-Z0-9]+", "_");
        setCode(s);
    }
}
