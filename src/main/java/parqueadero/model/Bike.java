/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.model;

import java.util.HashMap;
import parqueadero.util.AbstractModel;

/**
 * Bike class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class Bike extends AbstractModel {

    public static final String REGULAR = "1";
    public static final String BMX = "2";
    public static final String MOUNTAIN = "3";
    public static final String ROAD = "4";
    public static final String TOURING = "5";
    public static final String OTHER = "6";

    protected String type;
    protected String color;
    protected String observations;

    public Bike() {
        super();
    }

    public Bike(HashMap data) {
        super(data);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }
}
