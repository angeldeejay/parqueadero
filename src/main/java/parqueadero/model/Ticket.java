/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import parqueadero.dao.LotDAO;
import parqueadero.dao.OwnerDAO;
import parqueadero.dao.UserDAO;
import parqueadero.dao.VehicleDAO;
import parqueadero.util.AbstractModel;

/**
 * Ticket class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class Ticket extends AbstractModel {

    protected static final long serialVersionUID = 1L;

    protected Timestamp checkin;
    protected Timestamp checkout;
    protected String observations;
    protected BigDecimal endPrice;
    protected BigDecimal lotId;
    protected BigDecimal ownerId;
    protected BigDecimal userId;
    protected BigDecimal vehicleId;
    protected Lot lot;
    protected Owner owner;
    protected Vehicle vehicle;
    protected User user;

    public Ticket(Map data) {
        super(data);
        if (data.get("code") != null) {
            Map<String, Object> l = new HashMap<>(data);
            l.put("id", data.get("lotId"));
            this.lot = new Lot(l);
        }
        if (data.get("handicapped") != null) {
            Map<String, Object> o = new HashMap<>(data);
            o.put("id", data.get("ownerId"));
            this.lot = new Lot(o);
        }
        if (data.get("vehicle_type") != null) {
            Map<String, Object> v = new HashMap<>(data);
            v.put("id", data.get("vehicleId"));
            this.lot = new Lot(v);
        }
        if (data.get("username") != null) {
            Map<String, Object> u = new HashMap<>(data);
            u.put("id", data.get("userId"));
            this.lot = new Lot(u);
        }
    }

    public Ticket() {
        super();
    }

    public BigDecimal getEndPrice() {
        return endPrice;
    }

    public void setEndPrice(BigDecimal endPrice) {
        this.endPrice = endPrice;
    }

    public BigDecimal getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(BigDecimal ownerId) {
        this.ownerId = ownerId;
    }

    public BigDecimal getUserId() {
        return userId;
    }

    public void setUserId(BigDecimal userId) {
        this.userId = userId;
    }

    public BigDecimal getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(BigDecimal vehicleId) {
        this.vehicleId = vehicleId;
    }

    public Timestamp getCheckin() {
        return checkin;
    }

    public void setCheckin(Timestamp checkin) {
        this.checkin = checkin;
    }

    public Timestamp getCheckout() {
        return checkout;
    }

    public void setCheckout(Timestamp checkout) {
        this.checkout = checkout;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public Lot getLot() {
        if (lot == null && lotId != null) {
            LotDAO lotDAO = new LotDAO();
            lot = lotDAO.getById(lotId.intValueExact());
        }
        return lot;
    }

    public void setLot(Lot lot) {
        this.lot = lot;
    }

    public Owner getOwner() {
        if (owner == null && ownerId != null) {
            OwnerDAO ownerDAO = new OwnerDAO();
            owner = ownerDAO.getById(ownerId.intValueExact());
        }
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public User getUser() {
        if (user == null && userId != null) {
            UserDAO userDAO = new UserDAO();
            user = userDAO.getById(userId.intValueExact());
        }
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Vehicle getVehicle() {
        if (vehicle == null && vehicleId != null) {
            VehicleDAO vehicleDAO = new VehicleDAO();
            vehicle = vehicleDAO.getById(vehicleId.intValueExact());
        }
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public BigDecimal getLotId() {
        return lotId;
    }

    public void setLotId(BigDecimal lotId) {
        this.lotId = lotId;
    }
}
