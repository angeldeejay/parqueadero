/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.model;

import java.math.BigDecimal;
import java.util.Map;
import parqueadero.dao.OwnerDAO;
import parqueadero.dao.VehicleDAO;
import static parqueadero.model.Vehicle.BIKE;
import static parqueadero.model.Vehicle.CAR;
import static parqueadero.model.Vehicle.MOTORCYCLE;
import parqueadero.util.AbstractModel;

/**
 * Price class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class Price extends AbstractModel {

    public static final String BIKE_TYPE = BIKE;
    public static final String CAR_TYPE = CAR;
    public static final String MOTORCYCLE_TYPE = MOTORCYCLE;
    public static final String ALL_TYPES = null;

    protected String vehicleType;
    protected BigDecimal value;
    protected BigDecimal ownerId;
    protected BigDecimal vehicleId;
    protected Owner owner;
    protected Vehicle vehicle;

    public Price(Map data) {
        super(data);
    }

    public Price() {
        super();
    }

    public BigDecimal getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(BigDecimal ownerId) {
        this.ownerId = ownerId;
    }

    public BigDecimal getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(BigDecimal vehicleId) {
        this.vehicleId = vehicleId;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public Owner getOwner() {
        if (owner == null && ownerId != null) {
            OwnerDAO ownerDAO = new OwnerDAO();
            owner = ownerDAO.getById(ownerId.intValueExact());
        }
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public Vehicle getVehicle() {
        if (vehicle == null && vehicleId != null) {
            VehicleDAO vehicleDAO = new VehicleDAO();
            vehicle = vehicleDAO.getById(vehicleId.intValueExact());
        }
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }
}
