/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashMap;
import parqueadero.util.AbstractModel;

/**
 * Zone class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class Zone extends AbstractModel {

    protected String code;
    protected String enabled;
    protected BigDecimal deletedBy;
    protected Timestamp deletedAt;
    protected Collection<Lot> lots;

    public Zone(HashMap data) {
        super(data);
    }

    public Zone() {
        super();
        this.enabled = "1";
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(BigDecimal deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Timestamp getDeletedAt() {
        return deletedAt;
    }

    public boolean isEnabled() {
        return enabled.equals("1");
    }

    public void setEnabled(boolean enable) {
        this.enabled = (enable ? "1" : "0");
    }

    public void setDeletedAt(Timestamp deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Collection<Lot> getLots() {
        return lots;
    }

    public void setLots(Collection<Lot> lots) {
        this.lots = lots;
    }
}
