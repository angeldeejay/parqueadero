/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.model;

import java.awt.Color;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import parqueadero.dao.BikeDAO;
import parqueadero.dao.CarDAO;
import parqueadero.dao.MotorcycleDAO;
import parqueadero.util.AbstractModel;

/**
 * Vehicle class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class Vehicle extends AbstractModel {

    public static final String CAR = "C";
    public static final String BIKE = "B";
    public static final String MOTORCYCLE = "M";

    public static final Hashtable<String, String> AVAILABLE_TYPES = new Hashtable() {
        {
            put("AUTOMÓVIL", CAR);
            put("BICICLETA", BIKE);
            put("MOTOCICLETA", MOTORCYCLE);
        }
    };
    public static final Hashtable<String, Color> AVAILABLE_COLORS = new Hashtable() {
        {
            put("VERDE", Color.GREEN);
            put("ROSA", Color.PINK);
            put("ROJO", Color.RED);
            put("PÚRPURA", Color.MAGENTA);
            put("NEGRO", Color.BLACK);
            put("NARANJA", Color.ORANGE);
            put("GRIS", Color.GRAY);
            put("GRIS OSCURO", Color.DARK_GRAY);
            put("CYAN", Color.CYAN);
            put("BLANCO", Color.WHITE);
            put("AZUL", Color.BLUE);
            put("AMARILLO", Color.YELLOW);
        }
    };

    protected String vehicleType;
    protected Bike bike;
    protected Car car;
    protected Motorcycle motorcycle;
    protected Collection<Price> prices;
    protected Collection<Ticket> tickets;

    public Vehicle() {
        super();
    }

    public Vehicle(HashMap data) {
        super(data);
        if (data.get("color") != null) {
            switch (this.vehicleType) {
                case BIKE:
                    this.setBike(new Bike(data));
                    break;
                case CAR:
                    this.setCar(new Car(data));
                    break;
                case MOTORCYCLE:
                    this.setMotorcycle(new Motorcycle(data));
                    break;
            }
        }
    }

    public Vehicle(BigDecimal id, String type) {
        super.id = id;
        this.vehicleType = type;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public Collection<Price> getPrices() {
        return prices;
    }

    public void setPrices(Collection<Price> prices) {
        this.prices = prices;
    }

    public Bike getBike() {
        if (vehicleType.equals(BIKE) && bike == null && super.id != null) {
            BikeDAO dao = new BikeDAO();
            bike = dao.getById(super.id.intValue());
        }
        return bike;
    }

    public Car getCar() {
        if (vehicleType.equals(CAR) && car == null && super.id != null) {
            CarDAO dao = new CarDAO();
            car = dao.getById(super.id.intValue());
        }
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Motorcycle getMotorcycle() {
        if (vehicleType.equals(MOTORCYCLE) && motorcycle == null && super.id != null) {
            MotorcycleDAO dao = new MotorcycleDAO();
            motorcycle = dao.getById(super.id.intValue());
        }
        return motorcycle;
    }

    public void setMotorcycle(Motorcycle motorcycle) {
        this.motorcycle = motorcycle;
    }

    public void setBike(Bike bike) {
        this.bike = bike;
    }

    public Collection<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(Collection<Ticket> tickets) {
        this.tickets = tickets;
    }
}
