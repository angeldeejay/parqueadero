/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Map;
import parqueadero.dao.ZoneDAO;
import static parqueadero.model.Vehicle.BIKE;
import static parqueadero.model.Vehicle.CAR;
import static parqueadero.model.Vehicle.MOTORCYCLE;
import parqueadero.util.AbstractModel;

/**
 * Lot class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class Lot extends AbstractModel {

    public static final String ONLY_BIKE_ALLOWED = BIKE;
    public static final String ONLY_CAR_ALLOWED = CAR;
    public static final String ONLY_MOTORCYCLE_ALLOWED = MOTORCYCLE;
    public static final String ALL_ALLOWED = null;

    protected String code;
    protected String empty;
    protected String allowedType;
    protected String handicapped;
    protected BigDecimal deletedBy;
    protected Timestamp deletedAt;
    protected String enabled;
    protected BigDecimal zoneId;
    protected Zone zone;
    protected Collection<Ticket> tickets;

    public Lot(final Map<String, Object> data) {
        super(data);
    }

    public Lot() {
        super();
        this.empty = "1";
        this.enabled = "1";
        this.handicapped = "0";
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isEmpty() {
        return empty.equalsIgnoreCase("1");
    }

    public void setEmpty(boolean empty) {
        this.empty = empty ? "1" : "0";
    }

    public String getAllowedType() {
        return allowedType;
    }

    public void setAllowedType(String allowedType) {
        this.allowedType = allowedType;
    }

    public boolean isHandicapped() {
        return handicapped.equalsIgnoreCase("1");
    }

    public void setHandicapped(boolean handicapped) {
        this.handicapped = handicapped ? "1" : "0";
    }

    public BigDecimal getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(BigDecimal deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Timestamp getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Timestamp deletedAt) {
        this.deletedAt = deletedAt;
    }

    public boolean isEnabled() {
        return enabled.equals("1");
    }

    public void setEnabled(boolean enable) {
        this.enabled = (enable ? "1" : "0");
    }

    public Zone getZone() {
        if (zone == null && zoneId != null && super.getId() != null) {
            ZoneDAO dao = new ZoneDAO();
            zone = dao.getById(zoneId.intValueExact());
        }
        return zone;
    }

    public void setZone(Zone zone) {
        this.zoneId = zone.getId();
    }

    public void setZoneId(BigDecimal id) {
        zoneId = id;
    }

    public BigDecimal getZoneId() {
        return zoneId;
    }

    public Collection<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(Collection<Ticket> tickets) {
        this.tickets = tickets;
    }
}
