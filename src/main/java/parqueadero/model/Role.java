/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import parqueadero.dao.PermissionDAO;
import parqueadero.util.AbstractModel;

/**
 * Role class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class Role extends AbstractModel {

    protected String enabled;
    protected String inmutable;
    protected String name;
    protected Collection<Permission> permissions;

    public Role() {
        super();
        this.inmutable = "0";
        this.enabled = "1";
        this.permissions = new ArrayList();
    }

    public Role(HashMap data) {
        super(data);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<Permission> getPermissions() {
        if (permissions == null && super.id != null) {
            PermissionDAO dao = new PermissionDAO();
            permissions = dao.getByRoleId(getId().intValue(), true);
        }
        return permissions;
    }

    public void setPermissions(Collection<Permission> permissions) {
        this.permissions = permissions;
    }

    public boolean isEnabled() {
        return enabled.equals("1");
    }

    public void setEnabled(boolean enable) {
        this.enabled = (enable ? "1" : "0");
    }

    public boolean isInmutable() {
        return inmutable.equalsIgnoreCase("1");
    }

    public void setInmutable(boolean inmutable) {
        this.inmutable = (inmutable ? "1" : "0");
    }
}
