/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashMap;
import parqueadero.util.AbstractModel;

/**
 * Owner class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class Owner extends AbstractModel {

    protected String handicapped;
    protected String enabled;
    protected BigDecimal deletedBy;
    protected Timestamp deletedAt;
    protected Collection<Price> prices;
    protected Collection<Ticket> tickets;

    public Owner(HashMap data) {
        super(data);
    }

    public Owner() {
        super();
        this.enabled = "1";
        this.handicapped = "0";
    }

    public boolean isHandicapped() {
        return handicapped.equalsIgnoreCase("1");
    }

    public void setHandicapped(boolean handicapped) {
        this.handicapped = handicapped ? "1" : "0";
    }

    public BigDecimal getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(BigDecimal deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Timestamp getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Timestamp deletedAt) {
        this.deletedAt = deletedAt;
    }

    public boolean isEnabled() {
        return enabled.equals("1");
    }

    public void setEnabled(boolean enable) {
        this.enabled = (enable ? "1" : "0");
    }

    public Collection<Price> getPrices() {
        return prices;
    }

    public void setPrices(Collection<Price> prices) {
        this.prices = prices;
    }

    public Collection<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(Collection<Ticket> tickets) {
        this.tickets = tickets;
    }
}
