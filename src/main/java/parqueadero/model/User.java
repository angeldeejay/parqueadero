/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashMap;
import parqueadero.dao.RoleDAO;
import parqueadero.util.AbstractModel;

/**
 * User class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class User extends AbstractModel {

    protected Timestamp deletedAt;
    protected BigDecimal deletedBy;
    protected String enabled;
    protected String password;
    protected Collection<Role> roles;
    protected Collection<Ticket> tickets;
    protected String username;

    public User(HashMap data) {
        super(data);
    }

    public User() {
        super();
    }

    public User(String enabled, String password, String username) {
        this.enabled = enabled;
        this.password = password;
        this.username = username;
    }

    public Timestamp getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Timestamp deletedAt) {
        this.deletedAt = deletedAt;
    }

    public BigDecimal getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(BigDecimal deletedBy) {
        this.deletedBy = deletedBy;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String passwordToHash) {
        this.password = passwordToHash;
    }

    public Collection<Role> getRoles() {
        if (roles == null && super.id != null) {
            RoleDAO dao = new RoleDAO();
            roles = dao.getByUserId(getId().intValue(), true);
        }
        return roles;
    }

    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }

    public Collection<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(Collection<Ticket> tickets) {
        this.tickets = tickets;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isEnabled() {
        return enabled.equals("1");
    }

    public void setEnabled(boolean enable) {
        this.enabled = (enable ? "1" : "0");
    }

}
