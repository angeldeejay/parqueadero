/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero.model;

import java.util.HashMap;
import java.util.Hashtable;
import parqueadero.dao.OwnerDAO;
import parqueadero.dao.UserDAO;
import parqueadero.util.AbstractModel;

/**
 * Entity class
 *
 * @author W. Andres Vanegas J. <wandres.vanegas@gmail.com>
 */
public class Entity extends AbstractModel {

    public static final String CC = "CC";
    public static final String CE = "CE";
    public static final String TI = "TI";

    public static final Hashtable<String, String> AVAILABLE_IDENT_TYPES = new Hashtable() {
        {
            put("C. DE CIUDADANÍA", CC);
            put("C. DE EXTRANJERÍA", CE);
            put("T. DE IDENTIDAD", TI);
        }
    };
    protected String firstName;
    protected String lastName;
    protected String identType;
    protected String identValue;
    protected User user;
    protected Owner owner;

    public Entity() {
        super();
    }

    public Entity(HashMap data) {
        super(data);
        if (data.get("username") != null) {
            this.setUser(new User(data));
        }
        if (data.get("handicapped") != null) {
            this.setOwner(new Owner(data));
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdentType() {
        return identType;
    }

    public void setIdentType(String identType) {
        this.identType = identType;
    }

    public String getIdentValue() {
        return identValue;
    }

    public void setIdentValue(String identValue) {
        this.identValue = identValue;
    }

    public User getUser() {
        if (user == null && super.id != null) {
            UserDAO dao = new UserDAO();
            user = dao.getById(super.id.intValue());
        }
        return user;
    }

    public Owner getOwner() {
        if (owner == null && super.id != null) {
            OwnerDAO dao = new OwnerDAO();
            owner = dao.getById(super.id.intValue());
        }
        return owner;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }
}
