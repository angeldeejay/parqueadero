/*
 * Institución Universitaria Antonio José Camacho
 * CC 1107051252
 * Grupo 411
 * Tecnología en Sistemas de Información (Nocturna)
 */
package parqueadero;

import static java.awt.Color.WHITE;
import java.awt.Image;
import static java.lang.System.exit;
import static javax.swing.SwingUtilities.invokeLater;
import javax.swing.UIManager;
import static javax.swing.UIManager.getInstalledLookAndFeels;
import static javax.swing.UIManager.setLookAndFeel;
import static jiconfont.icons.font_awesome.FontAwesome.CAR;
import static parqueadero.util.Icon.getIcon;
import static parqueadero.util.Icon.getImage;
import parqueadero.view.SplashScreenView;

/**
 * Splash screen class
 *
 * @author W. Andres Vanegas
 */
public class Main {
    
    public static final javax.swing.Icon APP_ICON = getIcon(CAR, WHITE);
    public static final Image APP_IMAGE_ICON = getImage(CAR, WHITE);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        initialize();
    }

    /**
     * Getting native cross-platform look and feel
     */
    public static void setLyF() {
        String[] lookAndFeels = new String[]{
            "GTK+",
            "Windows",
            "Windows Classic",
            "Nimbus"
        };
        boolean setted = false;
        for (String lyf : lookAndFeels) {
            try {
                for (UIManager.LookAndFeelInfo info : getInstalledLookAndFeels()) {
                    if (lyf.equals(info.getName()) && !setted) {
                        setLookAndFeel(info.getClassName());
                        setted = true;
                        break;
                    }
                }
            } catch (Exception ex) {
            }
        }
    }

    /**
     * Initialize system
     */
    public static void initialize() {
        setLyF();
        SplashScreenView s = new SplashScreenView();
        s.setVisible(true);
        boolean validConfig = s.checkSettings();
        invokeLater(() -> {
            if (validConfig) {
                // Opening the main application
                s.dispose();
                parqueadero.view.MainView mainView = new parqueadero.view.MainView();
            } else {
                // Exiting on error
                exit(3);
            }
        });
    }
}
